FROM node:12-alpine

COPY . ./app

WORKDIR /app

RUN apk add --update python3 make g++ && rm -rf /var/cache/apk/*

RUN npm install

EXPOSE 3000

CMD ["npm", "run","start:dev"]
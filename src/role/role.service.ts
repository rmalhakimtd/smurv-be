import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Device } from 'src/device/entity/device.entity';
import { PageService } from 'src/helper/service/page/page.service';
import { isEmpty } from 'src/helper/validator/empty-validator';
import { Permission } from 'src/permission/entities/permission.entity';
import { Repository } from 'typeorm';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';

@Injectable()
export class RoleService extends PageService {
  constructor(
    @InjectRepository(Role) private roleRepo: Repository<Role>,
    @InjectRepository(Device) private deviceRepo: Repository<Device>,
    @InjectRepository(Permission)
    private permissionRepo: Repository<Permission>,
  ) {
    super();
  }

  async queryBuilder(alias: string) {
    return this.roleRepo.createQueryBuilder(alias);
  }

  async create(createRoleDto: CreateRoleDto) {
    const permissions = await this.permissionRepo.findByIds(
      createRoleDto.permissions,
    );
    if (isEmpty(permissions))
      throw new HttpException('Permission is not found', 409);

    createRoleDto.permissions = permissions;
    return this.roleRepo.save(createRoleDto);
  }

  findAll(filter) {
    return this.generatePage(filter, this.roleRepo, {
      relations: ['permissions'],
    });
  }

  async getPermissionByRole(id: number) {
    const data = await this.roleRepo.find({
      select: ['id'],
      where: { id: id },
      relations: ['permissions'],
    });

    return this.responseAPI('success', data);
  }

  async getRoleByUserId(id: number) {
    const data = await this.roleRepo.find({
      select: ['id'],
      relations: ['users'],
      where: { users: { id: id } },
    });



    return this.responseAPI('sucess', data);
  }

  async findOne(id: number) {
    const data = await this.roleRepo.findOne(id, {
      relations: ['permissions'],
    });
    if (!data) throw new HttpException('Role is not found', 409);

    return data;
  }

  async assignDevice(id: number, deviceId: number[]) {
    const role = await this.roleRepo.findOne(id);
    if (!role) throw new HttpException('Role is not found', 409);

    const devices = await this.deviceRepo.findByIds(deviceId);
    if (!devices) throw new HttpException('Devices is not found', 409);

    role.devices = devices;
    return this.roleRepo.save(role);
  }

  async update(id: number, updateRoleDto: UpdateRoleDto) {
    updateRoleDto.id = id;

    if (updateRoleDto.permissions) {
      const permissions = await this.permissionRepo.findByIds(
        updateRoleDto.permissions,
      );
      if (isEmpty(permissions))
        throw new HttpException('Permission is not found', 409);
      updateRoleDto.permissions = permissions;
    }

    return this.roleRepo.save(updateRoleDto);
  }

  async remove(id: number) {
    const role = await this.findOne(id);
    if (!role) throw new HttpException('Role is not found', 409);

    return this.roleRepo.remove(role);
  }
}

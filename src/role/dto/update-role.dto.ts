import { ApiProperty, PartialType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNumber } from 'class-validator';
import { RoleDto } from './create-role.dto';

export class UpdateRoleDto extends PartialType(RoleDto) {}
export class AssignDeviceDto {
  @ApiProperty({ required: false })
  @IsNumber({}, { each: true })
  devices: number[];
}

import { ApiProperty, OmitType, PickType } from '@nestjs/swagger';
import { IsArray, IsOptional, IsString } from 'class-validator';
import { PageRequestDto, PageResponseDto } from 'src/helper/dto/page.dto';
import { IsExist } from 'src/helper/validator/exist-validator';
import { Permission } from 'src/permission/entities/permission.entity';
import { Role } from 'src/role/entities/role.entity';

export class RoleDto {
  @ApiProperty()
  @IsOptional()
  @IsExist([Role, 'id'])
  id?: number;

  @ApiProperty({ required: true })
  @IsString()
  role_name: string;

  @ApiProperty({ required: false })
  @IsArray()
  @IsOptional()
  permissions: Permission[];
}

export class FindRoleDto extends PageRequestDto {
  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  role_name: string;
}

export class ResponseRoleDto extends PageResponseDto {
  @ApiProperty({ type: [RoleDto] })
  data: RoleDto[];
}

export class CreateRoleDto extends OmitType(RoleDto, ['id']) {}
export class RoleIdDto extends PickType(RoleDto, ['id']) {}

import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  Req,
  HttpException,
} from '@nestjs/common';
import { RoleService } from './role.service';
import {
  CreateRoleDto,
  RoleIdDto,
  ResponseRoleDto,
  FindRoleDto,
} from './dto/create-role.dto';
import { AssignDeviceDto, UpdateRoleDto } from './dto/update-role.dto';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { JwtGuard } from 'src/auth/jwt.guard';
import {
  createNewRole,
  deleteRole,
  findByIdRole,
  getAllRoles,
  updateRole,
} from 'src/helper/constant';
@ApiTags('Role Management')
@ApiBearerAuth()
@UseGuards(JwtGuard)
@Controller('role')
export class RoleController {
  constructor(private readonly roleService: RoleService) {}

  @Post()
  create(@Req() req: any, @Body() createRoleDto: CreateRoleDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(createNewRole))
      throw new HttpException('Access Forbidden', 403);

    return this.roleService.create(createRoleDto);
  }

  @Get('permission/:id')
  findPermission(@Req() req: any, @Param() id: RoleIdDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(findByIdRole))
      throw new HttpException('Access Forbidden', 403);

    return this.roleService.getPermissionByRole(+id.id);
  }

  @Get()
  @ApiOkResponse({ type: ResponseRoleDto })
  findAll(@Req() req: any, @Query() filter: FindRoleDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(getAllRoles))
      throw new HttpException('Access Forbidden', 403);

    return this.roleService.findAll(filter);
  }

  @Get(':id')
  findOne(@Req() req: any, @Param() id: RoleIdDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(findByIdRole))
      throw new HttpException('Access Forbidden', 403);

    return this.roleService.findOne(+id.id);
  }

  @Post(':id/devices')
  assignDevices(
    @Req() req: any,
    @Param() id: RoleIdDto,
    @Body() body: AssignDeviceDto,
  ) {
    const perms = req.body.permission;
    if (!perms.includes(updateRole))
      throw new HttpException('Access Forbidden', 403);

    return this.roleService.assignDevice(+id.id, body.devices);
  }

  @Patch(':id')
  update(
    @Req() req: any,
    @Param() id: RoleIdDto,
    @Body() updateRoleDto: UpdateRoleDto,
  ) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(updateRole))
      throw new HttpException('Access Forbidden', 403);

    return this.roleService.update(+id.id, updateRoleDto);
  }

  @Delete(':id')
  remove(@Req() req: any, @Param() id: RoleIdDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(deleteRole))
      throw new HttpException('Access Forbidden', 403);

    return this.roleService.remove(+id.id);
  }
}

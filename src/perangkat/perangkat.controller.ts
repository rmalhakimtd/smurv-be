import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { JwtGuard } from 'src/auth/jwt.guard';
import {
  createNewPerangkat,
  deletePerangkat,
  findByIdPerangkat,
  getAllPerangkats,
  updatePerangkat,
} from 'src/helper/constant';
import { ResponseService } from 'src/helper/service/response/api.service';
import {
  CreatePerangkatDto,
  FindPerangkatDto,
  PerangkatIdDto,
  ResponsePerangkatDto,
  UpdatePerangkatDto,
} from './dto/perangkat.dto';
import { PerangkatService } from './perangkat.service';

@ApiTags('Perangkat')
@ApiBearerAuth()
@UseGuards(JwtGuard)
@Controller('perangkat')
export class PerangkatController extends ResponseService {
  constructor(private readonly perangkatService: PerangkatService) {
    super();
  }

  @Post()
  create(@Req() req: any, @Body() createPerangkatDto: CreatePerangkatDto) {
    const perms = req.body.permission;
    if (!perms.includes(createNewPerangkat))
      throw new HttpException('Access Forbidden', 403);

    return this.perangkatService.create(createPerangkatDto);
  }

  @Get()
  @ApiOkResponse({ type: ResponsePerangkatDto })
  findAll(@Req() req: any, @Query() filter: FindPerangkatDto) {
    const perms = req.body.permission;
    if (!perms.includes(getAllPerangkats))
      throw new HttpException('Access Forbidden', 403);

    return this.perangkatService.findAll(filter);
  }

  @Get('options')
  findOptions() {
    return this.perangkatService.findOptions();
  }

  @Get(':id')
  findOne(@Req() req: any, @Param() id: PerangkatIdDto) {
    const perms = req.body.permission;
    if (!perms.includes(findByIdPerangkat))
      throw new HttpException('Access Forbidden', 403);

    return this.perangkatService.findOne(+id.id);
  }

  @Patch(':id')
  update(
    @Req() req: any,
    @Param() id: PerangkatIdDto,
    @Body() updatePerangkatDto: UpdatePerangkatDto,
  ) {
    const perms = req.body.permission;
    if (!perms.includes(updatePerangkat))
      throw new HttpException('Access Forbidden', 403);

    return this.perangkatService.update(+id.id, updatePerangkatDto);
  }

  @Delete(':id')
  remove(@Req() req: any, @Param() id: PerangkatIdDto) {
    const perms = req.body.permission;
    if (!perms.includes(deletePerangkat))
      throw new HttpException('Access Forbidden', 403);

    return this.perangkatService.remove(+id.id);
  }
}

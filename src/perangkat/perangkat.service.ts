import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PageService } from 'src/helper/service/page/page.service';
import { Organization } from 'src/organization/entity/organization.entity';
import { Repository } from 'typeorm';
import { CreatePerangkatDto, UpdatePerangkatDto } from './dto/perangkat.dto';
import { Perangkat } from './entity/perangkat.entity';

@Injectable()
export class PerangkatService extends PageService {
  constructor(
    @InjectRepository(Perangkat) private perangkatRepo: Repository<Perangkat>,
    @InjectRepository(Organization)
    private organizationRepo: Repository<Organization>,
  ) {
    super();
  }
  async create(createPerangkatDto: CreatePerangkatDto) {
    const org = await this.organizationRepo.findOne(createPerangkatDto.owner);
    if (!org) throw new HttpException('Organization not found', 404);

    createPerangkatDto.organization = org;

    const data = await this.perangkatRepo.save(createPerangkatDto);

    return this.responseAPI('success create organization', data);
  }

  async findAll(filter) {
    return this.generatePage(filter, this.perangkatRepo, {
      relations: ['organization'],
    });
  }

  async findOne(id: number) {
    const data = await this.perangkatRepo.findOne(id);
    if (!data) throw new HttpException('organization not found', 409);

    return this.responseAPI('success', data);
  }

  async findOptions() {
    const data = await this.perangkatRepo.find({
      select: ['id', 'device_name'],
    });

    return this.responseAPI('success', data);
  }

  async update(id: number, updatePerangkatDto: UpdatePerangkatDto) {
    updatePerangkatDto.id = id;
    const data = await this.perangkatRepo.save(updatePerangkatDto);

    return this.responseAPI('success', data);
  }

  async remove(id: number) {
    const data = await this.perangkatRepo.findOne(id);
    if (!data) throw new HttpException('organization not found', 409);

    const del = await this.perangkatRepo.remove(data);

    return this.responseAPI('success delete organization', del);
  }
}

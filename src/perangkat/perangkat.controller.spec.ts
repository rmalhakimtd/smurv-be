import { Test, TestingModule } from '@nestjs/testing';
import { PerangkatController } from './perangkat.controller';

describe('PerangkatController', () => {
  let controller: PerangkatController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PerangkatController],
    }).compile();

    controller = module.get<PerangkatController>(PerangkatController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

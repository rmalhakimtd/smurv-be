import { Test, TestingModule } from '@nestjs/testing';
import { PerangkatService } from './perangkat.service';

describe('PerangkatService', () => {
  let service: PerangkatService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PerangkatService],
    }).compile();

    service = module.get<PerangkatService>(PerangkatService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

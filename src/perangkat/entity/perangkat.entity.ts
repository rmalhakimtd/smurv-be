import { Organization } from 'src/organization/entity/organization.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'm_perangkat' })
export class Perangkat {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  device_name: string;

  @Column()
  prefix: string;

  @Column()
  detail: string;

  @OneToOne(() => Organization, (org) => org.id)
  @JoinColumn({ name: 'owner' })
  organization: Organization;

  @Column()
  owner: number;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}

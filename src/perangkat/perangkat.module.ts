import { Module } from '@nestjs/common';
import { PerangkatService } from './perangkat.service';
import { PerangkatController } from './perangkat.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Perangkat } from './entity/perangkat.entity';
import { Organization } from 'src/organization/entity/organization.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Perangkat, Organization])],
  providers: [PerangkatService],
  controllers: [PerangkatController],
})
export class PerangkatModule {}

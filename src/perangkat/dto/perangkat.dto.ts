import { ApiProperty, OmitType, PartialType, PickType } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { PageRequestDto, PageResponseDto } from 'src/helper/dto/page.dto';
import { IsExist } from 'src/helper/validator/exist-validator';
import { Organization } from 'src/organization/entity/organization.entity';
import { Perangkat } from '../entity/perangkat.entity';

export class PerangkatDto {
  @ApiProperty()
  @IsOptional()
  @IsExist([Perangkat, 'id'])
  id: number;

  @ApiProperty()
  @IsOptional()
  device_name: string;

  @ApiProperty()
  @IsOptional()
  prefix: string;

  @ApiProperty()
  @IsOptional()
  detail: string;

  @ApiProperty()
  @IsOptional()
  owner: number;

  @ApiProperty()
  @IsOptional()
  organization: Organization;
}

export class FindPerangkatDto extends PageRequestDto {
  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  device_name: string;
}

export class ResponsePerangkatDto extends PageResponseDto {
  @ApiProperty({ type: PerangkatDto })
  data: PerangkatDto[];
}

export class CreatePerangkatDto extends OmitType(PerangkatDto, ['id']) {}
export class PerangkatIdDto extends PickType(PerangkatDto, ['id']) {}
export class UpdatePerangkatDto extends PartialType(PerangkatDto) {}

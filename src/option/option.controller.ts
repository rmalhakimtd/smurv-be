import { Controller, Get } from '@nestjs/common';
import { OptionService } from './option.service';

@Controller('option')
export class OptionController {
  constructor(private readonly optionService: OptionService) {}

  @Get('organisasi')
  getOrganisasi() {
    return this.optionService.getOrganisasi();
  }

  @Get('permission')
  getPermission() {
    return this.optionService.getPermission();
  }
}

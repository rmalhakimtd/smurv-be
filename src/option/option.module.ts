import { Module } from '@nestjs/common';
import { OptionService } from './option.service';
import { OptionController } from './option.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Organization } from 'src/organization/entity/organization.entity';
import { Permission } from 'src/permission/entities/permission.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Organization, Permission])],
  controllers: [OptionController],
  providers: [OptionService],
})
export class OptionModule {}

import { Inject, Injectable } from '@nestjs/common';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { Organization } from 'src/organization/entity/organization.entity';
import { Permission } from 'src/permission/entities/permission.entity';
import { Repository } from 'typeorm';

@Injectable()
export class OptionService {
  constructor(
    @InjectRepository(Organization) private orgRepo: Repository<Organization>,
    @InjectRepository(Permission)
    private permissionRepo: Repository<Permission>,
  ) {}

  async getOrganisasi() {
    const models = await this.orgRepo
      .createQueryBuilder('m_organization')
      .select(['m_organization.id AS id', 'm_organization.name AS text'])
      .getRawMany();
    return models;
  }

  async getPermission() {
    const models = await this.permissionRepo
      .createQueryBuilder('m_permissions')
      .select([
        'm_permissions.id AS id',
        'm_permissions.permission_name AS text',
      ])
      .getRawMany();
    return models;
  }
}

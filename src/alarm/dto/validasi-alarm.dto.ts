import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsString } from 'class-validator';

export class ValidasiRequest {
  uuid: string;
  kategori: string;
}

export class CreateValidasiDto {
  @ApiProperty({ required: true })
  @IsArray()
  data: ValidasiRequest[];

  @ApiProperty({ required: true })
  @IsString()
  catatan: string;

  @ApiProperty({ required: true })
  @IsString()
  status: string;
}

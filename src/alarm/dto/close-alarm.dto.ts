import { ApiProperty } from '@nestjs/swagger';
import { IsArray } from 'class-validator';

export class DataAlarm {
  uuid: string;
  kategori: string;
}

export class CloseAlarmDto {
  @ApiProperty({ required: true })
  @IsArray()
  data: DataAlarm[];
}

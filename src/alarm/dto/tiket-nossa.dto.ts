import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsString } from 'class-validator';

export class TiketNossaDto {
  uuid: string;
  kategori: string;
}

export class CreateTiketNossaDto {
  @ApiProperty({ required: true })
  @IsArray()
  data: TiketNossaDto[];

  @ApiProperty({ required: true })
  @IsString()
  tiket: string;
}

import { PartialType } from '@nestjs/swagger';
import { AlarmDto } from './create-alarm.dto';

export class UpdateAlarmDto extends PartialType(AlarmDto) {}
export class UpdateAcknowledgeAlarmDto extends PartialType(AlarmDto) {}

import { ApiProperty, OmitType, PickType } from '@nestjs/swagger';
import { IsArray, IsBoolean, IsOptional, IsString } from 'class-validator';
import { PageRequestDto, PageResponseDto } from 'src/helper/dto/page.dto';
import { IsExist } from 'src/helper/validator/exist-validator';
import { AlarmSummary } from '../entities/alarm.entity';

export class AlarmDto {
  @ApiProperty()
  @IsOptional()
  @IsExist([AlarmSummary, 'id'])
  id: number;

  @ApiProperty()
  @IsOptional()
  @IsString()
  alert_code: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  kategori: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  tipe: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  regional: string;

  @ApiProperty()
  @IsOptional()
  alert_time: Date;

  @ApiProperty()
  @IsOptional()
  is_valid: boolean;

  @ApiProperty()
  @IsOptional()
  acknowledge_by: number;

  @ApiProperty()
  @IsOptional()
  acknowledge_time: Date;

  @ApiProperty()
  @IsOptional()
  acknowledge_status: number;

  @ApiProperty()
  @IsOptional()
  status: string;

  @ApiProperty()
  @IsOptional()
  id_perangkat: number;

  @ApiProperty()
  @IsOptional()
  is_closed: boolean;
}

export class FindAlarmDto extends PageRequestDto {
  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  kategori: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  regional: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  tipe: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  status: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  search: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  date: string;
}

export class ResponseAlarmDto extends PageResponseDto {
  @ApiProperty({ type: [AlarmSummary] })
  data: AlarmSummary[];
}

export class CreateAlarmSummaryDto extends OmitType(AlarmDto, ['id']) {}
export class AlarmIdDto extends PickType(AlarmDto, ['id']) {}

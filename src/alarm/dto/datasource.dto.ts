import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
  UUIDVersion,
} from 'class-validator';
import { PageRequestDto } from 'src/helper/dto/page.dto';
import { IsExist } from 'src/helper/validator/exist-validator';
import { ViewAlarmSummary } from '../entities/view-alarm.entity';

export class ViewAlarmSummaryDto {
  @ApiProperty()
  @IsOptional()
  @IsExist([ViewAlarmSummary, 'id'])
  id: number;

  @ApiProperty()
  @IsOptional()
  uuid: string;

  @ApiProperty()
  @IsOptional()
  kategori: string;
}

export class FindViewAlarm extends PageRequestDto {
  @ApiProperty({ required: true })
  @IsUUID()
  uuid: UUIDVersion;

  @ApiProperty()
  @IsString()
  kategori: string;
}

export class FindDetailAlarm extends PageRequestDto {
  @ApiProperty({ required: false })
  uuid: number;

  @ApiProperty({ required: false })
  @IsString()
  kategori: string;
}

export class SetAcknowledge {
  @ApiProperty({ required: true })
  uuid: number;

  @ApiProperty({ required: true })
  type: string;
}

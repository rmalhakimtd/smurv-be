import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsString } from 'class-validator';

export class AcknowledgeDto {
  uuid: string;
  kategori: string;
}

export class CreateAcknowledgeDto {
  @ApiProperty({ required: true })
  @IsArray()
  data: AcknowledgeDto[];
}

import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'm_regional' })
export class Regional {
  @PrimaryColumn()
  kode: string;

  @Column()
  nama_regional: string;
}

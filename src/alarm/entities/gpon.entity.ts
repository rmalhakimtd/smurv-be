import { User } from 'src/user/entities/user.entity';
import {
  Entity,
  Column,
  Generated,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity({ name: 'gpon_alerts' })
export class GPONAlert {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  uuid: string;

  @Column()
  vendor: string;

  @Column()
  node_ip: string;

  @Column()
  node_id: string;

  @Column()
  node_status: string;

  @Column()
  jumlah_ont: number;

  @Column()
  check_telegram: string;

  @Column()
  cek_ping: string;

  @Column({ nullable: true })
  regional: string;

  @Column({ type: 'timestamp', nullable: true })
  alert_time: Date;

  @Column({ nullable: true })
  is_valid: boolean;

  @Column({ nullable: true })
  acknowledge_by: number;

  @Column({ type: 'timestamp', nullable: true })
  acknowledge_time: Date;

  @Column({ nullable: true })
  acknowledge_status: number;

  @Column({ nullable: true })
  status: string;

  @Column({ nullable: true })
  is_closed: boolean;

  @Column()
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @Column()
  @UpdateDateColumn({ name: 'updated_at', onUpdate: 'CURRENT_TIMESTAMP(6)' })
  updatedAt: Date;

  @Column()
  tiket_nossa: string;

  @Column()
  id_perangkat: number;

  @Column()
  validated_time: Date;

  @ManyToOne(() => User, (user) => user.acknowledges)
  @JoinColumn({ name: 'validated_by' })
  validated_user: User;

  @Column({ nullable: true })
  validated_by: number;

  @Column()
  dismantle_id: number;
}

import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'trunk_alert' })
export class TrunkAlert {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  uuid: string;

  @Column()
  server_id: string;

  @Column()
  tglfile: string;

  @Column()
  ruas: string;

  @Column()
  node: string;

  @Column()
  node_reach: string;

  @Column()
  tm_node_reach: string;

  @Column()
  interface: string;

  @Column()
  port_memb: string;

  @Column()
  jm_memb: string;

  @Column()
  tm_st_ehealth: string;

  @Column()
  tm_dt_ehealth: string;

  @Column()
  ehealth_stat: string;

  @Column()
  trf_in: string;

  @Column()
  trf_out: string;

  @Column()
  nbr: string;

  @Column()
  nbr_tglbacon: string;

  @Column()
  nbr_reach: string;

  @Column()
  tm_nbr_reach: string;

  @Column()
  ip_p2p: string;

  @Column()
  mode: string;

  @Column()
  utz_in: string;

  @Column()
  utz_ot: string;

  @Column()
  crc_err: string;

  @Column()
  tm_dt_splk: string;

  @Column()
  spl_stat: string;

  @Column()
  spl_flap: string;

  @Column()
  spl_lvpwr: string;

  @Column()
  spl_crc: string;

  @Column()
  cpu: string;

  @Column()
  mem: string;

  @Column()
  temp: string;

  @Column()
  alert: string;

  @Column()
  fungsi: string;

  @Column()
  tiket: string;

  @Column()
  ket: string;

  @Column()
  pdescp: string;

  @Column()
  eheatlh_adm: string;

  @Column()
  merk_olt: string;

  @Column()
  ospf: string;

  @Column()
  lok_radioip: string;

  @Column()
  speed_in: string;

  @Column()
  speed_ot: string;

  @Column()
  utz_in_int: string;

  @Column()
  utz_ot_int: string;

  @Column()
  BitsPerSecondInavg: string;

  @Column()
  BitsPerSecondOutAvg: string;

  @Column()
  UtilizationInavg: string;

  @Column()
  UtilizationOutavg: string;

  @Column()
  ospf_time: string;

  @Column()
  ospff_st: string;

  @Column()
  ospf_flap: string;

  @Column()
  tm_spl_flap: string;

  @Column()
  inp_man: string;

  @Column()
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @Column()
  @UpdateDateColumn({ name: 'updated_at', onUpdate: 'CURRENT_TIMESTAMP(6)' })
  updatedAt: Date;

  @Column()
  alert_time: Date;

  @Column()
  regional: string;

  @Column()
  is_valid: boolean;

  @Column()
  validated_by: number;

  @Column()
  acknowledge_by: number;

  @Column()
  acknowledge_time: Date;

  @Column()
  acknowledge_status: string;

  @Column()
  status: string;

  @Column()
  is_closed: boolean;

  @Column()
  tiket_nossa: string;

  @Column()
  id_perangkat: number;

  @Column()
  validated_time: Date;

  @ManyToOne(() => User, (user) => user.acknowledges)
  @JoinColumn({ name: 'validated_by' })
  validated_user: User;
}

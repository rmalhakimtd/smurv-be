import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'o_voice_data' })
export class VoiceData {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  uuid: string;

  @Column()
  server_id: string;

  @Column()
  tglfile: string;

  @Column()
  ruas: string;

  @Column()
  node: string;

  @Column()
  node_reach: string;

  @Column()
  tm_node_reach: string;

  @Column()
  interface: string;

  @Column()
  port_memb: string;

  @Column()
  jm_memb: string;

  @Column()
  tm_st_ehealth: string;

  @Column()
  eheath_stat: number;

  @Column()
  tm_dt_ehelath: string;

  @Column()
  trf_in: number;

  @Column()
  trf_ot: number;

  @Column()
  nbr: string;

  @Column()
  nbr_tglbacon: string;

  @Column()
  nbr_reach: string;

  @Column()
  tm_nbr_reach: string;

  @Column()
  ip_p2p: string;

  @Column()
  mode: string;

  @Column()
  ut_in: number;

  @Column()
  ut_ot: number;

  @Column()
  crc_err: number;

  @Column()
  eht_err: number;

  @Column()
  tm_dt_splk: string;

  @Column()
  spl_stat: string;

  @Column()
  spl_flap: number;

  @Column()
  spl_lvpwr: number;

  @Column()
  spl_crc: number;

  @Column()
  cpu: number;

  @Column()
  mem: number;

  @Column()
  temp: number;

  @Column()
  alert: string;

  @Column()
  fungsi: string;

  @Column()
  ket: string;

  @Column()
  pdescp: string;
  @Column()
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @Column()
  @UpdateDateColumn({ name: 'updated_at', onUpdate: 'CURRENT_TIMESTAMP(6)' })
  updatedAt: Date;

  @Column()
  alert_time: Date;

  @Column()
  regional: string;

  @Column()
  is_valid: boolean;

  @Column()
  validated_by: number;

  @Column()
  acknowledge_by: number;

  @Column()
  acknowledge_time: Date;

  @Column()
  acknowledge_status: string;

  @Column()
  status: string;

  @Column()
  is_closed: boolean;

  @Column()
  tiket_nossa: string;

  @Column()
  validated_time: Date;

  @ManyToOne(() => User, (user) => user.acknowledges)
  @JoinColumn({ name: 'validated_by' })
  validated_user: User;
}

import { DismantleRequest } from 'src/dismantle/entity/dismantle.entity';
import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ValidatedResponse } from './validated-response.entity';

@Entity({ name: 'v_alarm_summary' })
export class AlarmSummary {
  @PrimaryGeneratedColumn('uuid')
  uuid: string;

  @Column({ nullable: true })
  alert_code: string;

  @Column({ nullable: true })
  kategori: string;

  @Column({ nullable: true })
  tipe: string;

  @Column({ nullable: true })
  regional: string;

  @Column({ type: 'timestamp', nullable: true })
  alert_time: Date;

  @Column({ nullable: true })
  is_valid: boolean;

  @Column({ nullable: true })
  acknowledge_by: number;

  @Column({ type: 'timestamp', nullable: true })
  acknowledge_time: Date;

  @Column({ nullable: true })
  acknowledge_status: number;

  @Column({ nullable: true })
  status: string;

  @Column()
  status_order: number;

  @Column({ nullable: true })
  is_closed: boolean;

  @ManyToOne(() => User, (user) => user.acknowledges)
  @JoinColumn({ name: 'acknowledge_by' })
  acknowledge_user: User;

  @ManyToOne(() => User, (user) => user.acknowledges)
  @JoinColumn({ name: 'validated_by' })
  validated_user: User;

  @Column({ nullable: true })
  validated_by: number;

  @Column()
  validated_time: string;

  @OneToOne(() => ValidatedResponse, (validated) => validated.uuid)
  @JoinColumn({ name: 'uuid', referencedColumnName: 'uuid' })
  validated_response: ValidatedResponse;

  @Column({ nullable: true })
  tiket_nossa: string;
}

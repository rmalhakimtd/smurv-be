import { ViewColumn, ViewEntity } from 'typeorm';

@ViewEntity({ name: 'v_alarm_summary' })
export class ViewAlarmSummary {
  @ViewColumn()
  id: number;

  @ViewColumn()
  uuid: string;

  @ViewColumn()
  kategori: string;
}

import {
  Controller,
  Get,
  Post,
  Param,
  UseGuards,
  Body,
  Req,
  HttpException,
  Query,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtGuard } from 'src/auth/jwt.guard';
import { ResponseService } from 'src/helper/service/response/api.service';
import { AlarmSummaryService } from './alarm.service';
import { AlarmIdDto } from './dto/create-alarm.dto';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { Request } from 'express';
import {
  findByIdAlarmSummaries,
  getAllAlarmSummaries,
  setAcknowledgeAlarm,
  setTiketNossa,
  setValidasiAlarm,
} from '../helper/constant';
import { FindViewAlarm } from './dto/datasource.dto';
import { MailService } from 'src/mail/mail.service';
import { CreateValidasiDto } from './dto/validasi-alarm.dto';
import { CreateTiketNossaDto } from './dto/tiket-nossa.dto';
import { CreateAcknowledgeDto } from './dto/acknowledge.dto';
import { CloseAlarmDto } from './dto/close-alarm.dto';
import { OrganizationService } from 'src/organization/organization.service';
import { UserService } from 'src/user/user.service';

@ApiTags('Alarm Summary')
@ApiBearerAuth()
@UseGuards(JwtGuard)
@Controller('alarm-summary')
export class AlarmSummaryController extends ResponseService {
  constructor(
    @InjectQueue('alarm') private readonly alarmQueue: Queue,
    private readonly alarmService: AlarmSummaryService,
    private readonly mailService: MailService,
    private readonly organizationService: OrganizationService,
    private readonly userService: UserService,
  ) {
    super();
  }

  @Post('close-alarm')
  setCloseAlarm(@Body() body: CloseAlarmDto, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(setValidasiAlarm))
      throw new HttpException('Access Forbidden', 403);

    const userId = req.user.id;
    return this.alarmService.setCloseAlarm(body, userId);
  }

  @Post('validasi-alarm')
  setValidasiAlarm(@Body() body: CreateValidasiDto, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(setValidasiAlarm))
      throw new HttpException('Access Forbidden', 403);

    const userId = req.user.id;
    return this.alarmService.SetValidasiAlarm(body, userId);
  }

  @Post('tiket-nossa')
  setTiketNossa(@Body() body: CreateTiketNossaDto, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(setTiketNossa))
      throw new HttpException('Access Forbidden', 403);

    return this.alarmService.setTiketNossa(body);
  }

  @Post('acknowledge')
  async setAcknowledge(@Body() body: CreateAcknowledgeDto, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(setAcknowledgeAlarm))
      throw new HttpException('Access Forbidden', 403);

    const idUser = req.user.id;
    try {
      for (const item of body.data) {
        const job = await this.alarmQueue.add('acknowledge', {
          uuid: item.uuid,
          kategori: item.kategori,
        });
      }

      return this.alarmService.setStatusAck(body, idUser);
    } catch (e) {
      console.log(e);
      return {
        status: 400,
        message: 'Terjadi kesalahan',
        error: e.message,
      };
    }
  }

  @Get('detail')
  getDetailAlarm(@Req() req: any, @Query() query: FindViewAlarm) {
    const perms = req.body.permission;
    if (!perms.includes(findByIdAlarmSummaries))
      throw new HttpException('Access Forbidden', 403);

    const uuid = query.uuid;
    const type = query.kategori as string;
    return this.alarmService.getDetailAlarm(uuid, type);
  }

  @Get()
  async getAll(@Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(getAllAlarmSummaries))
      throw new HttpException('Access Forbidden', 403);

    const user = await this.userService.findById(req.user.id);
    const regional = await this.organizationService.findRegional(
      user.organization.id,
    );

    // eslint-disable-next-line prefer-const
    let models = await this.alarmService.queryBuilder('v_alarm_summary');

    models.leftJoinAndSelect(
      'v_alarm_summary.acknowledge_user',
      'acknowledge_user',
    );

    models.leftJoinAndSelect(
      'v_alarm_summary.validated_user',
      'validated_user',
    );

    models.leftJoinAndSelect(
      'v_alarm_summary.validated_response',
      'validated_response',
    );

    const area: string = req.query.area as string;
    if (area && area != '') {
      models.andWhere('regional ilike :q4', { q4: `'${area}'` });
    } else {
      models.andWhere('v_alarm_summary.regional IN (:...regional)', {
        regional: regional,
      });
    }

    const search = req.query.search as string;
    if (search && search != '') {
      models.andWhere(
        'v_alarm_summary.alert_code ilike :s or kategori ilike :s or status ilike :s ',
        { s: `%${search}%` },
      );
    }

    const sort: any = req.query.sortBy;
    const sortDirection: any = req.query.sortDirection;

    if (sort && sortDirection) {
      models.orderBy(`v_alarm_summary.${sort}`, sortDirection.toUpperCase());
    } else {
      models.orderBy('v_alarm_summary.status_order', 'ASC');
      models.addOrderBy('v_alarm_summary.alert_time', 'DESC');
    }

    const uuid: string = req.query.uuid as string;
    if (uuid && uuid != '') {
      models.andWhere('v_alarm_summary.uuid = :uuid', { uuid: uuid });
    }

    const tanggal: string = req.query.tanggal as string;
    if (tanggal) {
      models.andWhere('alert_time = :q1', { q1: `'${tanggal}'` });
    }

    const start_date: string = req.query.start_date as string;
    if (start_date) {
      models.andWhere('"alert_time" >= :start_date', {
        start_date: `${start_date}`,
      });
    }

    const end_date: string = req.query.end_date as string;
    if (end_date) {
      models.andWhere('"alert_time" <= :end_date', {
        end_date: `${end_date}`,
      });
    }

    const kategori: string = req.query.kategori as string;
    if (kategori && kategori != '') {
      models.andWhere('kategori ilike :q2', { q2: kategori });
    }

    const status: string = req.query.status as string;
    if (status && status != '') {
      if (status != 'Closed') {
        models.andWhere("status <> 'Closed'");
      }
      models.andWhere('status ilike :q3', { q3: `${status}` });
    } else {
      models.andWhere("status <> 'Closed'");
    }

    const tipe_alert: string = req.query.tipe_alert as string;
    if (tipe_alert && tipe_alert != '') {
      models.andWhere('tipe ilike :q5', { q5: tipe_alert });
    }

    const page: number = parseInt(req.query.page as any) || 1;
    const perpage: number = parseInt(req.query.perpage as any) || 20;
    const total = await models.getCount();

    models.offset((page - 1) * perpage).limit(perpage);

    return {
      total,
      page,
      pages: Math.ceil(total / perpage),
      data: await models.getMany(),
    };
  }

  @Get(':id')
  findOne(@Req() req: any, @Param() id: AlarmIdDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(findByIdAlarmSummaries))
      throw new HttpException('Access Forbidden', 403);

    return this.alarmService.findOne(+id.id);
  }
}

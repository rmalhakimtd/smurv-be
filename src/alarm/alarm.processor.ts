import { Process, Processor } from '@nestjs/bull';
import { Job } from 'bull';
import { AlarmSummaryService } from './alarm.service';

@Processor('alarm')
export class AlarmProcessor {
  constructor(private readonly alarmService: AlarmSummaryService) {}

  @Process('acknowledge')
  async acknowledgeOpen(job: Job) {
    const uuid = job.data.uuid;
    const kategori = job.data.kategori;
    const timeout = 60000; // 10 menit
    // const timeout = 1000; // 5 Seconds
    console.log(`running job : ${kategori} : ${uuid}`);

    return new Promise<void>((resolve, reject) => {
      setTimeout(async () => {
        await this.alarmService.openAcknowledge(uuid, kategori);
        resolve();
      }, timeout);
    });
  }
}

import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UUIDVersion } from 'class-validator';
import * as moment from 'moment';
import { BlackoutAlert } from 'src/dashboard/entities/blackout.entity';
import { PageService } from 'src/helper/service/page/page.service';
import { Any, In, Not, Repository } from 'typeorm';
import { CreateAcknowledgeDto } from './dto/acknowledge.dto';
import { CloseAlarmDto } from './dto/close-alarm.dto';
import { CreateTiketNossaDto } from './dto/tiket-nossa.dto';
import { CreateValidasiDto } from './dto/validasi-alarm.dto';
import { AlarmSummary } from './entities/alarm.entity';
import { BboneAlert } from './entities/bbone.entity';
import { DatinData } from './entities/datin.entity';
import { GPONAlert } from './entities/gpon.entity';
import { MetroAlert } from './entities/metro.entity';
import { TrunkAlert } from './entities/trunk.entity';
import { ValidatedResponse } from './entities/validated-response.entity';
import { VoiceData } from './entities/voice.entity';

@Injectable()
export class AlarmSummaryService extends PageService {
  constructor(
    @InjectRepository(TrunkAlert)
    private trunkAlertRepo: Repository<TrunkAlert>,
    @InjectRepository(GPONAlert) private gponAlertRepo: Repository<GPONAlert>,
    @InjectRepository(AlarmSummary) private alarmRepo: Repository<AlarmSummary>,
    @InjectRepository(BboneAlert) private bboneRepo: Repository<BboneAlert>,
    @InjectRepository(MetroAlert) private metroRepo: Repository<MetroAlert>,
    @InjectRepository(DatinData) private datinRepo: Repository<DatinData>,
    @InjectRepository(VoiceData) private voiceRepo: Repository<VoiceData>,
    @InjectRepository(BlackoutAlert)
    private blackOutRepo: Repository<BlackoutAlert>,
    @InjectRepository(ValidatedResponse)
    private validResponseRepo: Repository<ValidatedResponse>,
  ) {
    super();
  }

  async setCloseAlarm(request: CloseAlarmDto, userId: number) {
    for (const item of request.data) {
      let models = null;
      switch (item.kategori.toLocaleUpperCase()) {
        case 'GPON':
          models = this.gponAlertRepo;
          break;
        case 'METRO':
          models = this.metroRepo;
          break;
        case 'BBONE':
          models = this.bboneRepo;
          break;
        case 'DATIN':
          models = this.datinRepo;
          break;
        case 'VOICE':
          models = this.voiceRepo;
          break;
        case 'BLACKOUT':
          models = this.blackOutRepo;
        case 'BLACKOUT':
          models = this.blackOutRepo;
        default:
          new HttpException('Kategori tidak valid', 400);
          break;
      }
      const data = await models.findOne({
        where: {
          uuid: item.uuid,
          status: Not(Any(['NEW', 'Acknowledge'])),
        },
      });
      if (!data) continue;
      data.is_closed = true;
      data.status = 'Closed';
      const update = await models.save(data);
    }

    return this.responseAPI(`Success set validation alarm`, []);
  }

  async setDismantleFlag(request: CreateValidasiDto, userId: string) {
    for (const item of request.data) {
      let models = null;
      switch (item.kategori.toLocaleUpperCase()) {
        case 'GPON':
          models = this.gponAlertRepo;
          break;
        case 'METRO':
          models = this.metroRepo;
          break;
        case 'BBONE':
          models = this.bboneRepo;
          break;
        case 'DATIN':
          models = this.datinRepo;
          break;
        case 'VOICE':
          models = this.voiceRepo;
          break;
        case 'BLACKOUT':
          models = this.blackOutRepo;
        default:
          new HttpException('Kategori tidak valid', 400);
          break;
      }
      const data = await models.findOne({
        where: {
          uuid: item.uuid,
          status: 'Acknowledge',
        },
      });
      if (!data) continue;
      data.validated_by = userId;
      data.is_valid = request.status.toLocaleLowerCase() == 'valid';
      data.status = request.status;
      data.validated_time = moment().toDate();
      const update = await models.save(data);
    }

    return this.responseAPI(`Success set validation alarm`, []);
  }

  async SetValidasiAlarm(request: CreateValidasiDto, userId: number) {
    for (const item of request.data) {
      let models = null;
      switch (item.kategori.toLocaleUpperCase()) {
        case 'GPON':
          models = this.gponAlertRepo;
          break;
        case 'METRO':
          models = this.metroRepo;
          break;
        case 'BBONE':
          models = this.bboneRepo;
          break;
        case 'DATIN':
          models = this.datinRepo;
          break;
        case 'VOICE':
          models = this.voiceRepo;
          break;
        case 'BLACKOUT':
          models = this.blackOutRepo;
        default:
          new HttpException('Kategori tidak valid', 400);
          break;
      }
      const data = await models.findOne({
        where: {
          uuid: item.uuid,
          status: 'Acknowledge',
        },
      });
      const validResponse = new ValidatedResponse();
      validResponse.uuid = item.uuid;
      validResponse.kategori = item.kategori;
      validResponse.validated_note = request.catatan;

      await this.validResponseRepo.save(validResponse);

      if (!data) continue;
      data.validated_by = userId;
      data.is_valid = request.status.toLocaleLowerCase() == 'valid';
      data.status = request.status;
      data.validated_time = moment().toDate();

      const update = await models.save(data);
    }

    return this.responseAPI(`Success set validation alarm`, []);
  }

  async setTiketNossa(request: CreateTiketNossaDto) {
    for (const item of request.data) {
      let models = null;

      switch (item.kategori.toLocaleUpperCase()) {
        case 'GPON':
          models = this.gponAlertRepo;
          break;
        case 'METRO':
          models = this.metroRepo;
          break;
        case 'BBONE':
          models = this.bboneRepo;
          break;
        case 'DATIN':
          models = this.datinRepo;
          break;
        case 'VOICE':
          models = this.voiceRepo;
          break;
        case 'BLACKOUT':
          models = this.blackOutRepo;
        default:
          new HttpException('Kategori tidak valid', 400);
          break;
      }

      const data = await models.findOne({
        where: {
          uuid: item.uuid,
          status: 'Valid',
        },
      });

      if (!data) continue;

      data.tiket_nossa = request.tiket;
      const update = await models.save(data);
    }
    return this.responseAPI('Tiket nossa is updated', []);
  }

  async setStatusAck(request: CreateAcknowledgeDto, userId: any) {
    for (const item of request.data) {
      let models = null;

      switch (item.kategori.toLocaleUpperCase()) {
        case 'GPON':
          models = this.gponAlertRepo;
          break;
        case 'METRO':
          models = this.metroRepo;
          break;
        case 'BBONE':
          models = this.bboneRepo;
          break;
        case 'DATIN':
          models = this.datinRepo;
          break;
        case 'VOICE':
          models = this.voiceRepo;
          break;
        case 'BLACKOUT':
          models = this.blackOutRepo;
        default:
          new HttpException('Kategori tidak valid', 400);
          break;
      }

      const data = await models.findOne({
        where: {
          uuid: item.uuid,
          status: 'NEW',
        },
      });

      if (!data) continue;

      data.acknowledge_by = userId;
      data.acknowledge_status = 1;
      data.acknowledge_time = moment().toDate();
      data.status = 'Acknowledge';
      const update = await models.save(data);
    }

    return this.responseAPI('Alarm is set Acknowledge', []);
  }

  async openAcknowledge(uuid: string, kategori: string) {
    try {
      let models = null;
      switch (kategori.toLocaleUpperCase()) {
        case 'GPON':
          models = this.gponAlertRepo;
          break;
        case 'METRO':
          models = this.metroRepo;
          break;
        case 'BBONE':
          models = this.bboneRepo;
          break;
        case 'DATIN':
          models = this.datinRepo;
          break;
        case 'VOICE':
          models = this.voiceRepo;
          break;
        case 'BLACKOUT':
          models = this.blackOutRepo;
        default:
          new HttpException('Kategori tidak valid', 400);
          break;
      }

      const data = await models.findOne({
        where: {
          uuid: uuid,
        },
      });

      if (!data) console.log('not found');

      data.acknowledge_status = 2;
      return await models.save(data);
    } catch (e) {
      console.log(e.message);
    }
  }

  async getDetailAlarm(uuid: UUIDVersion, type: string) {
    let models = null;
    switch (type.toLocaleUpperCase()) {
      case 'GPON':
        models = this.gponAlertRepo;
        break;
      case 'METRO':
        models = this.metroRepo;
        break;
      case 'BBONE':
        models = this.bboneRepo;
        break;
      case 'DATIN':
        models = this.datinRepo;
        break;
      case 'VOICE':
        models = this.voiceRepo;
        break;
      case 'BLACKOUT':
        models = this.blackOutRepo;
      default:
        new HttpException('Kategori tidak valid', 400);
        break;
    }

    const data = await models.findOne({
      where: {
        uuid: uuid,
      },
    });
    if (!data) throw new HttpException('data not found', 404);

    const validResp = await this.validResponseRepo.findOne({
      where: {
        uuid: uuid,
      },
    });
    data.validated_note = validResp.validated_note;

    return this.responseAPI('success', data);
  }

  findAll(filter) {
    return this.generatePage(filter, this.alarmRepo);
  }

  async all(): Promise<AlarmSummary[]> {
    return this.alarmRepo.find();
  }

  async queryBuilder(alias: string) {
    return this.alarmRepo.createQueryBuilder(alias);
  }

  async findOne(id: any) {
    const data = await this.alarmRepo.findOne(id);
    if (!data) throw new HttpException('Alarm not found', 409);
    return this.responseAPI('alarm is found', data);
  }
}

import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlackoutAlert } from 'src/dashboard/entities/blackout.entity';
import { MailService } from 'src/mail/mail.service';
import { Organization } from 'src/organization/entity/organization.entity';
import { OrganizationService } from 'src/organization/organization.service';
import { Role } from 'src/role/entities/role.entity';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/user.service';
import { AlarmSummaryController } from './alarm.controller';
import { AlarmProcessor } from './alarm.processor';
import { AlarmSummaryService } from './alarm.service';
import { AlarmSummary } from './entities/alarm.entity';
import { BboneAlert } from './entities/bbone.entity';
import { DatinData } from './entities/datin.entity';
import { GPONAlert } from './entities/gpon.entity';
import { MetroAlert } from './entities/metro.entity';
import { Regional } from './entities/regional.entity';
import { TrunkAlert } from './entities/trunk.entity';
import { ValidatedResponse } from './entities/validated-response.entity';
import { VoiceData } from './entities/voice.entity';
@Module({
  imports: [
    TypeOrmModule.forFeature([
      AlarmSummary,
      TrunkAlert,
      GPONAlert,
      DatinData,
      MetroAlert,
      BboneAlert,
      VoiceData,
      BlackoutAlert,
      ValidatedResponse,
      Organization,
      User,
      Role,
      Regional,
    ]),
    BullModule.registerQueue({
      name: 'alarm',
    }),
  ],
  controllers: [AlarmSummaryController],
  providers: [
    AlarmSummaryService,
    AlarmProcessor,
    MailService,
    OrganizationService,
    UserService,
  ],
  exports: [AlarmSummaryService],
})
export class AlarmSummaryModule {}

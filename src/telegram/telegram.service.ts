import { Get, HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../user/entities/user.entity';

@Injectable()
export class TelegramService {
  constructor(@InjectRepository(User) private userRepo: Repository<User>) {}

  async generateToken(nik: string) {
    this.userRepo.findOne();

    let models = await this.userRepo.findOne({
      where: {
        username: nik,
      },
    });
    if (!models) {
      throw new HttpException(
        'Mohon maaf NIK yang anda masukkan tidak dapat kami temukan',
        400,
      );
    }
    models.telegram_verification = (Math.random() + 1)
      .toString(36)
      .substring(6);
    await this.userRepo.save(models);
    return models;
  }

  async validasiToken(
    nik: string,
    token: string,
    user_id: string,
    username: string,
  ) {
    let models = await this.userRepo.findOne({
      username: nik,
    });

    if (!models) {
      throw new HttpException('Mohon maaf data pengguna tidak valid', 400);
    }

    if (models.telegram_verification == token.toLowerCase()) {
      models.telegram_id = user_id;
      models.telegram_username = username;
      await this.userRepo.save(models);

      return {
        nik: models.username,
        nama: models.nama,
        telegram_id: user_id,
        telegram_username: username,
      };
    } else {
      throw new HttpException(
        'Mohon maaf token yang anda berikan tidak sesuai!!',
        400,
      );
    }
  }
}

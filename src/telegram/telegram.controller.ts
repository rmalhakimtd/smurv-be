import { Body, Controller, Get, Post, Req } from '@nestjs/common';
import { TelegramService } from './telegram.service';

@Controller('telegram-bot')
export class TelegramController {
  constructor(private readonly telegramService: TelegramService) {}

  @Post('generate')
  async generateToken(@Body() body) {
    const { nik } = body;
    try {
      let resp = await this.telegramService.generateToken(nik);
      return {
        status: 200,
        message: 'Token Berhasil Digenerate',
        result: resp,
      };
    } catch (e) {
      return {
        status: 400,
        message: 'Terjadi kesalahan server',
        result: e.message,
      };
    }
  }

  @Post('verifikasi')
  async verifikasiToken(@Body() body) {
    const { nik, token, user_id, username } = body;
    try {
      let resp = await this.telegramService.validasiToken(
        nik,
        token,
        user_id,
        username,
      );
      return {
        status: 200,
        message: 'Proses verifikasi berhasil dilakukan',
        result: resp,
      };
    } catch (e) {
      console.log(e);
      return {
        status: 400,
        message: 'Terjadi kesalahan server',
        result: e.message,
      };
    }
  }
}

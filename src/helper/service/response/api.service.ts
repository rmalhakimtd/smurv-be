import { Injectable } from '@nestjs/common';

@Injectable()
export class ResponseService {
  responseAPI(message, data) {
    const resp = {
      status: 200,
      message: message,
      data: data,
    };

    return resp;
  }
}

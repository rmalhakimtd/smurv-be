import { Injectable } from '@nestjs/common';
import { FindManyOptions, ILike } from 'typeorm';

@Injectable()
export class PageService {
  async generatePage(data, repo, opt: FindManyOptions = {}) {
    try {
      const { page, limit } = data;

      if (Object.keys(data).length > 2) {
        const where = Object.keys(data)
          .slice(2)
          .reduce((result, key) => {
            result[key] = data[key];

            return result;
          }, {});

        const filter = {};
        Object.keys(where).forEach((f) => {
          if (f == 'uuid') {
            filter[f] = where[f];
          } else {
            filter[f] = ILike(`%${where[f]}%`);
          }
        });
        opt.where = filter;
      }

      const total = await repo.count(opt);
      opt.skip = (page - 1) * limit;
      opt.take = limit;

      const result = await repo.find(opt);
      const pages = Math.ceil(total / limit);
      const finalData = {
        total: total,
        page: data.page,
        pages: pages,
        data: result,
      };
      return finalData;
    } catch (err) {
      console.log(err);
    }
  }

  responseAPI(message, data) {
    const resp = {
      status: 200,
      message: message,
      data: data,
    };

    return resp;
  }
}

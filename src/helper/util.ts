import { Between } from 'typeorm';
import moment from 'moment';

export const BetweenDates = (from: Date | string, to: Date | string) =>
  Between(
    moment(typeof from === 'string' ? new Date(from) : from).format(
      'YYYY-MM-DD HH:MM:SS',
    ),
    moment(typeof to === 'string' ? new Date(to) : to).format(
      'YYYY-MM-DD HH:MM:SS',
    ),
  );

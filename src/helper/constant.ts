// dashboard
export const dashboard = 'dashboard';

// Alarm Summary
export const getAllAlarmSummaries = 'getAllAlarmSummaries';
export const findByIdAlarmSummaries = 'findByIdAlarmSummaries';
export const setAcknowledgeAlarm = 'setAcknowledgeAlarm';
export const setTiketNossa = 'setTiketNossa';
export const setValidasiAlarm = 'setValidasiAlarm';

// Roles
export const getAllRoles = 'getAllRoles';
export const createNewRole = 'createNewRole';
export const updateRole = 'updateRole';
export const deleteRole = 'deleteRole';
export const findByIdRole = 'findByIdRole';

// Permission
export const getAllPermissions = 'getAllPermissions';
export const createNewPermission = 'createNewPermission';
export const updatePermission = 'updatePermission';
export const deletePermission = 'deletePermission';
export const findByIdPermission = 'findByIdPermission';

// Organization
export const getAllOrganizations = 'getAllOrganizations';
export const createNewOrganization = 'createNewOrganization';
export const updateOrganization = 'updateOrganization';
export const deleteOrganization = 'deleteOrganization';
export const findByIdOrganization = 'findByIdOrganization';

// User Permission
export const createNewUser = 'createNewUser';
export const getAllUsers = 'getAllUsers';
export const findByIdUser = 'findByIdUser';
export const updateUser = 'updateUser';
export const deleteUser = 'deleteUser';

// Device
export const createNewDevice = 'createNewDevice';
export const getAllDevices = 'getAllDevices';
export const findByIdDevice = 'findByIdDevice';
export const updateDevice = 'updateDevice';
export const deleteDevice = 'deleteDevice';

// Perangkat
export const createNewPerangkat = 'createNewPerangkat';
export const getAllPerangkats = 'getAllPerangkats';
export const findByIdPerangkat = 'findByIdPerangkat';
export const updatePerangkat = 'updatePerangkat';
export const deletePerangkat = 'deletePerangkat';

// Exclude Request
export const sendExcludeRequest = 'sendExcludeRequest';
export const getAllExcludeData = 'getAllExcludeData';

// Dismantle hasil pengecekan
export const sendDismantleRequest = 'sendDismantleRequest';
export const sendDismantleResponse = 'sendDismantleResponse';
export const getAllDismantleRequest = 'getAllDismantleRequest';
export const getAllDismantleResponse = 'getAllDismantleResponse';
export const findByIdDismantle = 'findByIdDismantle';
export const findResponseOM = 'findResponseOM';
export const findResponseSPM = 'findResponseSPM';
export const sendResponseOM = 'sendDismantleResponseOM';
export const sendResponseSPM = 'sendDismantleResponseSPM';
export const sendDismantleList = 'sendDismantleList';
export const includeDismantleList = 'includeDismantleList';
export const dismantleResponseOM = [
  'Perangkat sudah di dismantle',
  'Perangkat masih digunakan, tidak ada dismantle',
  'Konfigurasi perangkat masih digunakan, tolong dilakukan Exclude saja',
];
export const dismantleResponseSPM = [
  'Perangkat Dismantle, Alert sudah tidak muncul',
  'Perangkat masih digunakan, tidak ada dismantle',
  'Alert masih muncul, mohon dismantle ulang',
  'Tidak jadi Dismantle, akan dilakukan Exclude alert',
];

export const setNormalAlert = 'setNormalAlert';
export const setExcludeAlert = 'setExcludeAlert';

export const typeDismanteResposne = ['OM', 'SPM'];

export const excludeResponse = ['Approve Exclude', 'Reject Exclude'];
export const getAllExcludeRequest = 'getAllExcludeRequest';
export const sendExcludeResponse = 'sendExcludeResponse';

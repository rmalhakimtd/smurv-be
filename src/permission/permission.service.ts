import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PageService } from 'src/helper/service/page/page.service';
import { In, Repository } from 'typeorm';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { Permission } from './entities/permission.entity';

@Injectable()
export class PermissionService extends PageService {
  constructor(
    @InjectRepository(Permission)
    private permissionRepo: Repository<Permission>,
  ) {
    super();
  }

  async create(createPermissionDto: CreatePermissionDto) {
    const data = await this.permissionRepo.save(createPermissionDto);
    return this.responseAPI('success create permission', data);
  }

  async findOptions() {
    const data = await this.permissionRepo.find({
      select: ['id', 'permission_name'],
    });

    return this.responseAPI('success', data);
  }

  async getPermissionByRole(id: number) {
    const data = await this.permissionRepo.find({
      where: {
        id: id,
      },
      relations: ['roles'],
    });

    return this.responseAPI('success', data);
  }

  async getPermissionByRoles(id: number[]) {
    const data = await this.permissionRepo.find({
      where: {
        id: In(id),
      },
      relations: ['roles'],
    });

    return this.responseAPI('success', data);
  }

  async findAll(filter) {
    const data = await this.generatePage(filter, this.permissionRepo);
    return this.responseAPI('success', data);
  }

  async findOne(id: number) {
    const data = await this.permissionRepo.findOne(id);
    if (!data) throw new HttpException('permission not found', 409);

    return data;
  }

  async update(id: number, updatePermissionDto: UpdatePermissionDto) {
    const data = await this.permissionRepo.save(updatePermissionDto);

    return this.responseAPI('success update permission', data);
  }

  async remove(id: number) {
    const permission = await this.permissionRepo.findOne(id);
    if (!permission) throw new HttpException('permission not found', 409);

    const data = await this.permissionRepo.remove(permission);
    return this.responseAPI('success delete permission', data);
  }
}

import { PartialType } from '@nestjs/swagger';
import { PermissionDto } from './create-permission.dto';

export class UpdatePermissionDto extends PartialType(PermissionDto) {}

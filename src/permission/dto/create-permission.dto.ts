import { ApiProperty, OmitType, PickType } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { PageRequestDto, PageResponseDto } from 'src/helper/dto/page.dto';
import { IsExist } from 'src/helper/validator/exist-validator';
import { Permission } from '../entities/permission.entity';

export class PermissionDto {
  @ApiProperty()
  @IsOptional()
  @IsExist([Permission, 'id'])
  id?: number;

  @ApiProperty({ required: true })
  @IsString()
  permission_name: string;

  @ApiProperty({ required: true })
  @IsString()
  group: string;
}

export class FindPermissionDto extends PageRequestDto {
  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  permission_name: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  group: string;
}

export class ResponsePermissionDto extends PageResponseDto {
  @ApiProperty({ type: [PermissionDto] })
  data: PermissionDto[];
}

export class CreatePermissionDto extends OmitType(PermissionDto, ['id']) {}
export class PermissionIdDto extends PickType(PermissionDto, ['id']) {}

import { Role } from 'src/role/entities/role.entity';
import {
  PrimaryGeneratedColumn,
  Column,
  Generated,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToMany,
  Unique,
} from 'typeorm';

@Entity({ name: 'm_permissions' })
export class Permission {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  uuid: string;

  @Column()
  @Unique(['permission_name'])
  permission_name: string;

  @Column()
  group: string;

  @ManyToMany(() => Role, (role) => role.permissions)
  roles: Role[];

  @Column()
  @CreateDateColumn()
  created_at: Date;

  @Column()
  @UpdateDateColumn()
  updated_at: Date;

  @Column({ nullable: true })
  @DeleteDateColumn()
  deleted_at: Date;
}

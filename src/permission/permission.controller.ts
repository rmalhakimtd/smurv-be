import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
  UseFilters,
  Req,
  HttpException,
} from '@nestjs/common';
import { PermissionService } from './permission.service';
import {
  CreatePermissionDto,
  FindPermissionDto,
  PermissionIdDto,
  ResponsePermissionDto,
} from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { JwtGuard } from 'src/auth/jwt.guard';
import { RoleIdDto } from 'src/role/dto/create-role.dto';
import {
  createNewPermission,
  deletePermission,
  findByIdPermission,
  getAllPermissions,
  updatePermission,
} from 'src/helper/constant';
import { UserIdDto } from 'src/user/dto/create-user.dto';

@ApiTags('Permission Management')
@ApiBearerAuth()
@UseGuards(JwtGuard)
@Controller('permission')
export class PermissionController {
  constructor(private readonly permissionService: PermissionService) {}

  @Post()
  create(@Req() req: any, @Body() createPermissionDto: CreatePermissionDto) {
    const perms = req.body.permission;
    if (!perms.includes(createNewPermission))
      throw new HttpException('Access Forbidden', 403);

    return this.permissionService.create(createPermissionDto);
  }

  @Get()
  @ApiOkResponse({ type: ResponsePermissionDto })
  findAll(@Req() req: any, @Query() filter: FindPermissionDto) {
    const perms = req.body.permission;
    if (!perms.includes(getAllPermissions))
      throw new HttpException('Access Forbidden', 403);

    return this.permissionService.findAll(filter);
  }

  @Get('options')
  findOptions() {
    return this.permissionService.findOptions();
  }

  @Get('user/:id')
  findPermissionByUser(@Req() req: any, @Param() id: UserIdDto) {
    const perms = req.body.permission;
    if (!perms.includes(findByIdPermission))
      throw new HttpException('Access Forbidden', 403);
  }

  @Get('role/:id')
  findPermissionByRole(@Req() req: any, @Param() id: RoleIdDto) {
    const perms = req.body.permission;
    if (!perms.includes(findByIdPermission))
      throw new HttpException('Access Forbidden', 403);

    return this.permissionService.getPermissionByRole(+id.id);
  }

  @Get(':id')
  findOne(@Req() req: any, @Param('id') id: PermissionIdDto) {
    const perms = req.body.permission;
    if (!perms.includes(findByIdPermission))
      throw new HttpException('Access Forbidden', 403);

    return this.permissionService.findOne(+id.id);
  }

  @Patch(':id')
  update(
    @Req() req: any,
    @Param('id') id: PermissionIdDto,
    @Body() updatePermissionDto: UpdatePermissionDto,
  ) {
    const perms = req.body.permission;
    if (!perms.includes(updatePermission))
      throw new HttpException('Access Forbidden', 403);

    return this.permissionService.update(+id.id, updatePermissionDto);
  }

  @Delete(':id')
  remove(@Req() req: any, @Param('id') id: PermissionIdDto) {
    const perms = req.body.permission;
    if (!perms.includes(deletePermission))
      throw new HttpException('Access Forbidden', 403);

    return this.permissionService.remove(+id.id);
  }
}

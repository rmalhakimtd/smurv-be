import { IsNumber, IsString } from 'class-validator';

export class PushNotificationDto {
  @IsString()
  public token: string;

  @IsString()
  public title: string;

  @IsString()
  public body: string;

  @IsNumber()
  public sender: number;

  @IsNumber()
  public receiver: number;
}

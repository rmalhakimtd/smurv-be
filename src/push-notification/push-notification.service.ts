import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as admin from 'firebase-admin';
import { PageService } from 'src/helper/service/page/page.service';
import { User } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';
import { PushNotificationDto } from './dto/push-notification.dto';
import { Notification } from './entity/notification.entity';

@Injectable()
export class PushNotificationService extends PageService {
  constructor(
    @InjectRepository(Notification) private notifRepo: Repository<Notification>,
    @InjectRepository(User) private userRepo: Repository<User>,
  ) {
    super();
  }

  async send(pushNotificationDto: PushNotificationDto) {
    const { title, body, token, sender, receiver } = pushNotificationDto;

    const payload = {
      notification: {
        title,
        body,
      },
      data: {
        title,
        body,
      },
    };

    const notif = new Notification();
    notif.judul_pesan = title;
    notif.isi_pesan = body;
    notif.status = 0;
    notif.sender_id = sender;
    notif.receiver_id = receiver;
    notif.is_read = false;
    await this.notifRepo.save(notif);

    return admin.messaging().sendToDevice(token, payload);
  }

  async readNotif(req: any) {
    const user = await this.userRepo.findOne(req.user.id);
    const data = await this.notifRepo.find({
      where: {
        sender: user,
      },
    });

    for (const item of data) {
      item.is_read = true;
      await this.notifRepo.save(item);
    }

    return true;
  }

  async getAll(req: any) {
    const user = await this.userRepo.findOne(req.user.id);
    if (!user) throw new HttpException('User not found', 409);

    const data = await this.notifRepo.find({
      relations: ['sender', 'receiver'],
      where: {
        sender: user,
      },
    });
    return this.responseAPI('success', data);
  }
}

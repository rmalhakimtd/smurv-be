import {
  Body,
  Controller,
  Get,
  HttpException,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtGuard } from 'src/auth/jwt.guard';
import { dashboard } from 'src/helper/constant';
import { PushNotificationDto } from './dto/push-notification.dto';
import { PushNotificationService } from './push-notification.service';

@Controller('notifikasi')
@ApiTags('Notifikasi')
@ApiBearerAuth()
@UseGuards(JwtGuard)
export class PushNotificationController {
  constructor(
    private readonly pushNotificationService: PushNotificationService,
  ) {}

  @Post()
  async send(@Body() pushNotificationDto: PushNotificationDto) {
    return this.pushNotificationService.send(pushNotificationDto);
  }

  @Get()
  getAll(@Req() req: any) {
    try {
      const perms = req.body.permission;
      if (!perms.includes(dashboard))
        throw new HttpException('Access Forbidden', 403);

      return this.pushNotificationService.getAll(req);
    } catch (e) {
      return {
        status: 400,
        message: 'Terjadi kesalahan',
        error: e.message,
      };
    }
  }

  @Post('read')
  read(@Req() req: any) {
    return this.pushNotificationService.readNotif(req);
  }
}

import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 't_notification' })
export class Notification {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  judul_pesan: string;

  @Column()
  isi_pesan: string;

  @Column()
  status: number;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'sender_id' })
  sender: User;

  @Column()
  sender_id: number;

  @Column()
  receiver_id: number;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'receiver_id' })
  receiver: User;

  @Column({ default: false })
  is_read: boolean;

  @Column()
  @CreateDateColumn()
  createdAt: Date;
}

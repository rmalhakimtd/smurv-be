import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { request } from 'http';
import { BboneAlert } from 'src/alarm/entities/bbone.entity';
import { DatinData } from 'src/alarm/entities/datin.entity';
import { GPONAlert } from 'src/alarm/entities/gpon.entity';
import { MetroAlert } from 'src/alarm/entities/metro.entity';
import { TrunkAlert } from 'src/alarm/entities/trunk.entity';
import { DismantleRequest } from 'src/dismantle/entity/dismantle.entity';
import { excludeResponse } from 'src/helper/constant';
import { User } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateExcludeDto } from './dto/create-exclude.dto';
import { CreateExcludeResponse, SetInclude } from './dto/exclude-response.dto';
import { UpdateExcludeDto } from './dto/update-exclude.dto';
import { ExcludeData } from './entities/exclude-data.entity';
import { ExcludeResponse } from './entities/exclude-response.entity';
import { ExcludeEntity } from './entities/exclude.entity';

@Injectable()
export class ExcludesService {
  constructor(
    @InjectRepository(GPONAlert) private gponAlertRepo: Repository<GPONAlert>,
    @InjectRepository(TrunkAlert) private trunkRepo: Repository<TrunkAlert>,
    @InjectRepository(User) private userRepo: Repository<User>,
    @InjectRepository(ExcludeEntity)
    private excludeRepo: Repository<ExcludeEntity>,
    @InjectRepository(ExcludeData) private excludeData: Repository<ExcludeData>,
    @InjectRepository(ExcludeResponse)
    private exResponseRepo: Repository<ExcludeResponse>,
    @InjectRepository(BboneAlert) private bboneRepo: Repository<BboneAlert>,
    @InjectRepository(MetroAlert) private metroRepo: Repository<MetroAlert>,
    @InjectRepository(DatinData) private datinRepo: Repository<DatinData>,
    @InjectRepository(DismantleRequest)
    private dismantleRepo: Repository<DismantleRequest>,
  ) {}
  async postExcludeRequest(request: CreateExcludeDto, user: number) {
    try {
      for (let item of request.data) {
        let cek = await this.excludeRepo.findOne({
          where: {
            uuid: item.uuid,
            kategori: item.kategori,
          },
        });
        if (cek) {
          continue;
        }
        // Save into exclude request
        const userModel = await this.userRepo.findOne(user);
        await this.excludeRepo.save({
          uuid: item.uuid,
          kategori: item.kategori,
          sender_id: user,
          sender: userModel,
          catatan: request.catatan,
        });

        // Update masing-masing model
        let models = null;
        switch (item.kategori.toLocaleUpperCase()) {
          case 'GPON':
            models = this.gponAlertRepo;
            break;
          case 'METRO':
            models = this.metroRepo;
            break;
          case 'BBONE':
            models = this.bboneRepo;
            break;
          case 'DATIN':
            models = this.datinRepo;
            break;
          default:
            continue;
            break;
        }
        let data = await models.findOne({
          where: {
            uuid: item.uuid,
          },
        });
        if (!data) {
          throw new HttpException('Alarm not found', 409);
        }
        data.status = 'Exclude Request';
        await models.save(data);
      }
      return {
        status: 200,
        message: 'Berhasil Menambahkan Data Exclude',
        result: [],
      };
    } catch (e) {
      throw new HttpException('Terjadi kesalahan server', 400);
    }
  }

  async findAll(req: any) {
    // eslint-disable-next-line prefer-const
    let models = this.excludeData.createQueryBuilder('v_exclude_data');

    models.leftJoinAndSelect('v_exclude_data.response', 'response');

    const search = req.query.search as string;
    if (search && search != '') {
      models.andWhere(
        'v_exclude_data.alert_code ilike :s or kategori ilike :s or status ilike :s ',
        { s: `%${search}%` },
      );
    }

    const sort: any = req.query.sortBy;
    const sortDirection: any = req.query.sortDirection;

    if (sort && sortDirection) {
      models.orderBy(`v_exclude_data.${sort}`, sortDirection.toUpperCase());
    } else {
      models.orderBy('v_exclude_data.id', 'DESC');
    }

    const uuid: string = req.query.uuid as string;
    if (uuid && uuid != '') {
      models.andWhere('v_exclude_data.uuid = :uuid', { uuid: uuid });
    }

    const tanggal: string = req.query.tanggal as string;
    if (tanggal) {
      models.andWhere('date(alert_time) = :q1', { q1: `'${tanggal}'` });
    }

    const start_date: string = req.query.start_date as string;
    if (start_date) {
      models.andWhere('"createdAt" >= :start_date', {
        start_date: `${start_date}`,
      });
    }

    const end_date: string = req.query.end_date as string;
    if (end_date) {
      models.andWhere('"createdAt" <= :end_date', {
        end_date: `${end_date}`,
      });
    }

    const kategori: string = req.query.kategori as string;
    if (kategori && kategori != '') {
      models.andWhere('kategori ilike :q2', { q2: kategori });
    }

    const status: string = req.query.status as string;
    if (status && status != '') {
      models.andWhere('status = :q3', { q3: `${status}` });
    }

    const area: string = req.query.area as string;
    if (area && area != '') {
      models.andWhere('regional ilike :q4', { q4: `'${area}'` });
    }

    const tipe_alert: string = req.query.tipe_alert as string;
    if (tipe_alert && tipe_alert != '') {
      models.andWhere('tipe ilike :q5', { q5: tipe_alert });
    }

    const page: number = parseInt(req.query.page as any) || 1;
    const perpage: number = parseInt(req.query.perpage as any) || 20;
    const total = await models.getCount();

    // models.offset((page - 1) * perpage).limit(perpage);

    return {
      total,
      page,
      pages: Math.ceil(total / perpage),
      data: await models.getMany(),
    };
  }

  findOne(id: number) {
    return this.excludeRepo.findOne({
      where: {
        id: id,
      },
      relations: ['response', 'sender', 'sender.organization'],
    });
  }

  async createExcludeResponse(request: CreateExcludeResponse, userId: number) {
    try {
      for (let item of request.data) {
        const exclude = await this.excludeRepo.findOne({
          where: {
            id: item.id,
          },
        });
        if (!exclude) {
          throw new HttpException('Exclude data not found', 400);
        }

        const data = await this.exResponseRepo.findOne({
          where: {
            request: exclude,
          },
        });
        if (data)
          throw new HttpException(
            'Response has been created for that exclude',
            409,
          );

        // Save into exclude request
        const userModel = await this.userRepo.findOne(userId);
        if (!userModel) throw new HttpException('access forbiden', 403);

        await this.exResponseRepo.save({
          request: exclude,
          user: userModel,
          response: excludeResponse[request.response - 1],
          response_int: request.response,
          catatan: request.catatan,
        });

        exclude.status = request.response;
        await this.excludeRepo.save(exclude);
      }
      return {
        status: 200,
        message: 'Success exclude response',
        result: [],
      };
    } catch (e) {
      console.log(e);
      throw new HttpException(e.message, 400);
    }
  }

  // Set to Include : remove from exclude table
  async setToInclude(request: SetInclude) {
    try {
      for (let item of request.data) {
        let exclude = await this.excludeRepo.findOne({
          where: {
            id: item.id,
          },
        });
        if (!exclude) {
          continue;
        }
        let models = null;
        switch (exclude.kategori.toLocaleUpperCase()) {
          case 'GPON':
            models = this.gponAlertRepo;
            break;
          case 'METRO':
            models = this.metroRepo;
            break;
          case 'BBONE':
            models = this.bboneRepo;
            break;
          case 'DATIN':
            models = this.datinRepo;
            break;
          default:
            models = this.metroRepo;
            continue;
            break;
        }

        let data = await models.findOne({
          where: {
            uuid: exclude.uuid,
          },
        });
        if (!data) {
          continue;
        }
        data.status = 'Acknowledge';
        await models.save(data);
        let resp = await this.exResponseRepo.findOne({
          where: {
            request: exclude,
          },
        });
        if (resp) {
          await this.exResponseRepo.remove(resp);
        }
        await this.excludeRepo.remove(exclude);
      }
      return {
        status: 200,
        message: 'succes',
        result: [],
      };
    } catch (e) {
      throw new HttpException(e.message, 400);
    }
  }

  async setToDismantle(request: SetInclude) {
    try {
      for (let item of request.data) {
        let exclude = await this.excludeRepo.findOne({
          where: {
            id: item.id,
          },
        });
        if (!exclude) {
          continue;
        }
        let models = null;
        switch (exclude.kategori.toLocaleUpperCase()) {
          case 'GPON':
            models = this.gponAlertRepo;
            break;
          case 'METRO':
            models = this.metroRepo;
            break;
          case 'BBONE':
            models = this.bboneRepo;
            break;
          case 'DATIN':
            models = this.datinRepo;
            break;
          default:
            models = this.metroRepo;
            continue;
            break;
        }

        let data = await models.findOne({
          where: {
            uuid: exclude.uuid,
          },
        });
        if (!data) {
          continue;
        }

        // kirim notif dismantle request
        await this.dismantleRepo.save({
          catatan: exclude.catatan,
          sender: exclude.sender,
          uuid: exclude.uuid,
          kategori: exclude.kategori,
        });
        data.status = 'Set Dismantle';
        await models.save(data);
        let resp = await this.exResponseRepo.findOne({
          where: {
            request: exclude,
          },
        });
        if (resp) {
          await this.exResponseRepo.remove(resp);
        }
        await this.excludeRepo.remove(exclude);
      }
      return {
        status: 200,
        message: 'succes',
        result: [],
      };
    } catch (e) {
      throw new HttpException(e.message, 400);
    }
  }

  async findByUuid(uuid: string, kategori: string) {
    const data = await this.excludeData.findOne({
      relations: ['sender'],
      where: {
        uuid: uuid,
        kategori: kategori.toLocaleUpperCase(),
      },
    });
    if (!data) throw new HttpException('Data Dismantle not found', 404);
    return data;
  }
}

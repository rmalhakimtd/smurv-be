import { Test, TestingModule } from '@nestjs/testing';
import { ExcludesService } from './excludes.service';

describe('ExcludesService', () => {
  let service: ExcludesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExcludesService],
    }).compile();

    service = module.get<ExcludesService>(ExcludesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Req,
  HttpException,
  UseGuards,
} from '@nestjs/common';
import { ExcludesService } from './excludes.service';
import { CreateExcludeDto } from './dto/create-exclude.dto';
import { sendExcludeRequest, sendExcludeResponse } from '../helper/constant';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtGuard } from 'src/auth/jwt.guard';

import { getAllExcludeRequest } from 'src/helper/constant';
import { CreateExcludeResponse, SetInclude } from './dto/exclude-response.dto';
import { UserService } from 'src/user/user.service';
import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';

@Controller('exclude')
@ApiTags('Exclude')
@ApiBearerAuth()
@UseGuards(JwtGuard)
export class ExcludesController {
  constructor(
    @InjectQueue('notification') private readonly queue: Queue,
    private readonly excludesService: ExcludesService,
    private readonly userService: UserService,
  ) {}

  @Post('request')
  async create(@Body() body: CreateExcludeDto, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(sendExcludeRequest))
      throw new HttpException('Access Forbidden', 403);
    try {

      const resp = await this.excludesService.postExcludeRequest(
        body,
        req.user.id,
      );
      if (!resp) throw new HttpException('An Error occured', 409);
      const mailData = {
        data: [],
      };

      for (const item of body.data) {
        const exclude = await this.excludesService.findByUuid(
          item.uuid,
          item.kategori,
        );
        let users = await this.userService.getUserByRole('SPM');
        users = users.filter((val) => val.id != req.user.id);
        const payload = {
          users: users,
          title: 'exclude Request',
          message: `Telah dilakukan request exclude oleh : ${exclude.sender.nama}`,
          data: exclude,
        };

        await this.queue.add('redis', {
          payload: payload,
        });

        for (const user of users) {
          mailData.data.push({
            to: user.email,
            from: process.env.MAIL_USER,
            subject: 'Notif exclude ' + exclude.alert_code,
            html:
              'Kepada Yth <br/>' +
              'User ' +
              exclude.sender.nama +
              ' telah melakukan exclude request dengan detail sebagai berikut : <br/><br/>' +
              '<table>' +
              '<tr><td>Alert Code</td>' +
              exclude.alert_code +
              '<td></tr>' +
              '<tr><td>Alert Time</td>' +
              exclude.alert_time +
              '<td></tr>' +
              '<tr><td>Request User</td>' +
              exclude.sender.nama +
              '<td></tr>' +
              '<tr><td>Catatan</td>' +
              exclude.catatan +
              '<td></tr>' +
              '</table>',
          });
          this.queue.add('firebase', {
            sender: req.user.id,
            receiver: user.id,
            token: user.firebase_id,
            title: 'exclude Request',
            body: `Telah dilakukan request exclude oleh : ${exclude.sender.nama}`,
          });
        }
      }
      return resp;
    } catch (e) {
      console.log(e);
      return {
        status: 400,
        message: 'Terjadi kesalahan',
        error: e.message,
      };
    }
  }

  @Get()
  async findAll(@Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(getAllExcludeRequest))
      throw new HttpException('Access Forbidden', 403);

    return await this.excludesService.findAll(req);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.excludesService.findOne(+id);
  }

  @Post('response')
  async responseExclude(@Body() body: CreateExcludeResponse, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(sendExcludeResponse))
      throw new HttpException('Access Forbidden', 403);

    try {
      const resp = await this.excludesService.createExcludeResponse(
        body,
        req.user.id,
      );

      const sender = await this.userService.findById(req.user.id);

      const mailData = {
        data: [],
      };
      for (const item of body.data) {
        const excl = await this.excludesService.findOne(item.id);
        const exclude = await this.excludesService.findByUuid(
          excl.uuid,
          excl.kategori,
        );
        let users = await this.userService.getUserByRole('SPM');
        users = users.filter((val) => val.id != req.user.id);
        const payload = {
          users: users,
          title: 'exclude Request',
          message: `Telah dilakukan request exclude oleh : ${sender.nama}`,
          data: exclude,
        };

        await this.queue.add('redis', {
          payload: payload,
        });

        for (const user of users) {
          mailData.data.push({
            to: user.email,
            from: process.env.MAIL_USER,
            subject: 'Notif exclude ' + exclude.alert_code,
            html:
              'Kepada Yth <br/>' +
              'User ' +
              exclude.sender.nama +
              ' telah melakukan exclude request dengan detail sebagai berikut : <br/><br/>' +
              '<table>' +
              '<tr><td>Alert Code</td>' +
              exclude.alert_code +
              '<td></tr>' +
              '<tr><td>Alert Time</td>' +
              exclude.alert_time +
              '<td></tr>' +
              '<tr><td>Request User</td>' +
              exclude.sender.nama +
              '<td></tr>' +
              '<tr><td>Catatan</td>' +
              exclude.catatan +
              '<td></tr>' +
              '<tr><td>Response</td><td>' +
              (exclude.status == 1 ? 'Approved' : 'Rejected') +
              '</td></tr>' +
              '<tr><td>Catatan Response exclude</td>' +
              body.catatan +
              '<td></tr>' +
              '</table>',
          });
          await this.queue.add('firebase', {
            sender: req.user.id,
            receiver: user.id,
            token: user.firebase_id,
            title: 'exclude Response OM',
            body: `Telah dilakukan response exclude oleh : ${sender.nama}`,
          });
        }
      }
      await this.queue.add('email', {
        mail: mailData,
      });
      return resp;
    } catch (e) {
      return {
        status: 400,
        message: 'Terjadi kesalahan',
        error: e.message,
      };
    }
  }

  @Post('set-include')
  async setToInclude(@Body() body: SetInclude, @Req() req: any) {
    return await this.excludesService.setToInclude(body);
  }

  @Post('set-dismantle')
  async setToDismantle(@Body() body: SetInclude, @Req() req: any) {
    return await this.excludesService.setToDismantle(body);
  }

  @Get('summary')
  async getSummary(@Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(getAllExcludeRequest))
      throw new HttpException('Access Forbidden', 403);

    let resp = await this.excludesService.findAll(req);
    return {
      total: resp.data.length,
      backbone: resp.data.filter((x) => x.kategori.toLowerCase() == 'backbone')
        .length,
      metro: resp.data.filter((x) => x.kategori.toLowerCase() == 'metro'),
      gpon: resp.data.filter((x) => x.kategori.toLowerCase() == 'gpon'),
      service_node: resp.data.filter(
        (x) => x.kategori.toLowerCase() == 'service_node',
      ),
      blackout: resp.data.filter((x) => x.kategori.toLowerCase() == 'blackout'),
    };
  }
}

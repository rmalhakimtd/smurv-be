import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ExcludeResponse } from './exclude-response.entity';

@Entity('t_exclude_request')
export class ExcludeEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  uuid: string;

  @Column()
  kategori: string;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'sender_id', referencedColumnName: 'id' })
  sender: User;

  sender_id: number;

  @OneToOne(() => ExcludeResponse, (response) => response.request)
  response: ExcludeResponse;

  @Column({ default: 0 })
  status: number;

  @Column()
  catatan: string;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn({ onUpdate: 'CURRENT_TIMESTAMP(6)' })
  updatedAt: Date;
}

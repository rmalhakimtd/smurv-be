import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ExcludeResponse } from './exclude-response.entity';

@Entity('v_exclude_data')
export class ExcludeData {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  uuid: string;

  @Column()
  kategori: string;

  @Column()
  alert_code: string;

  @Column()
  alert_time: string;

  @Column()
  tipe: string;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'sender_id', referencedColumnName: 'id' })
  sender: User;

  sender_id: number;

  @Column({ default: 0 })
  status: number;

  @Column()
  catatan: string;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  regional: string;

  @Column()
  @UpdateDateColumn({ onUpdate: 'CURRENT_TIMESTAMP(6)' })
  updatedAt: Date;

  @OneToMany(() => ExcludeResponse, (exclude) => exclude.data)
  @JoinColumn({ name: 'id' })
  response: ExcludeResponse[];
}

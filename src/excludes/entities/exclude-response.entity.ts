import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ExcludeData } from './exclude-data.entity';
import { ExcludeEntity } from './exclude.entity';

@Entity('t_exclude_response')
export class ExcludeResponse {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => ExcludeEntity, (exclude) => exclude.id)
  @JoinColumn({ name: 'request_id', referencedColumnName: 'id' })
  request: ExcludeEntity;

  @ManyToOne(() => ExcludeData, (exclude) => exclude.response)
  @JoinColumn({ name: 'request_id' })
  data: ExcludeData;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;

  @Column()
  response: string;

  @Column()
  response_int: number;

  @Column()
  catatan: string;

  @Column()
  @CreateDateColumn()
  created_at: Date;

  @Column()
  @UpdateDateColumn({ onUpdate: 'CURRENT_TIMESTAMP(6)' })
  updated_at: Date;
}

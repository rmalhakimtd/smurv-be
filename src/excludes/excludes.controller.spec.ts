import { Test, TestingModule } from '@nestjs/testing';
import { ExcludesController } from './excludes.controller';
import { ExcludesService } from './excludes.service';

describe('ExcludesController', () => {
  let controller: ExcludesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExcludesController],
      providers: [ExcludesService],
    }).compile();

    controller = module.get<ExcludesController>(ExcludesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

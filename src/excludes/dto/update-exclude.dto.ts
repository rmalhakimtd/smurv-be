import { PartialType } from '@nestjs/swagger';
import { CreateExcludeDto } from './create-exclude.dto';

export class UpdateExcludeDto extends PartialType(CreateExcludeDto) {}

import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNumber, IsOptional, IsString } from 'class-validator';

export class ItemData {
  @IsOptional()
  id: any;
}

export class CreateExcludeResponse {
  @ApiProperty({ required: true })
  @IsArray()
  data: ItemData[];

  @ApiProperty({ required: true })
  @IsNumber()
  response: number;

  @ApiProperty({ required: true })
  @IsString()
  catatan: string;
}

export class SetInclude {
  @IsOptional()
  data: ItemData[];
}

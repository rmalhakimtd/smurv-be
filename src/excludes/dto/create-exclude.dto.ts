import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsString } from 'class-validator';

export class ExcludeRequestDto {
  uuid: string;
  kategori: string;
}

export class CreateExcludeDto {
  @ApiProperty({ required: true })
  @IsArray()
  data: ExcludeRequestDto[];

  @ApiProperty({ required: true })
  @IsString()
  catatan: string;
}

import { Module } from '@nestjs/common';
import { ExcludesService } from './excludes.service';
import { ExcludesController } from './excludes.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GPONAlert } from 'src/alarm/entities/gpon.entity';
import { ExcludeEntity } from './entities/exclude.entity';
import { ExcludeResponse } from './entities/exclude-response.entity';
import { TrunkAlert } from 'src/alarm/entities/trunk.entity';
import { User } from 'src/user/entities/user.entity';
import { ExcludeData } from './entities/exclude-data.entity';
import { DatinData } from 'src/alarm/entities/datin.entity';
import { MetroAlert } from 'src/alarm/entities/metro.entity';
import { BboneAlert } from 'src/alarm/entities/bbone.entity';
import { DismantleRequest } from 'src/dismantle/entity/dismantle.entity';
import { BullModule } from '@nestjs/bull';
import { DismantleProcessor } from 'src/dismantle/dismantle.processor';
import { PushNotificationService } from 'src/push-notification/push-notification.service';
import { DismantleModule } from 'src/dismantle/dismantle.module';
import { MailService } from 'src/mail/mail.service';
import { UserService } from 'src/user/user.service';
import { Role } from 'src/role/entities/role.entity';
import { Organization } from 'src/organization/entity/organization.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      GPONAlert,
      ExcludeEntity,
      ExcludeResponse,
      TrunkAlert,
      User,
      Role,
      Organization,
      ExcludeData,
      GPONAlert,
      DatinData,
      MetroAlert,
      BboneAlert,
      DismantleRequest,
    ]),
    BullModule.registerQueue({
      name: 'notification',
    }),
    DismantleModule,
  ],
  controllers: [ExcludesController],
  providers: [ExcludesService, UserService],
})
export class ExcludesModule {}

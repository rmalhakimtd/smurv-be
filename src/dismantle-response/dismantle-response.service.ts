import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BboneAlert } from 'src/alarm/entities/bbone.entity';
import { DatinData } from 'src/alarm/entities/datin.entity';
import { GPONAlert } from 'src/alarm/entities/gpon.entity';
import { MetroAlert } from 'src/alarm/entities/metro.entity';
import { VoiceData } from 'src/alarm/entities/voice.entity';
import { BlackoutAlert } from 'src/dashboard/entities/blackout.entity';
import {
  CreateDismantleResponseDto,
  NormalAlertDto,
} from 'src/dismantle/dto/dismantle.dto';
import { DismantleResponse } from 'src/dismantle/entity/dismantle-response.entity';
import { DismantleRequest } from 'src/dismantle/entity/dismantle.entity';
import { ExcludeEntity } from 'src/excludes/entities/exclude.entity';
import { dismantleResponseOM, typeDismanteResposne } from 'src/helper/constant';
import { PageService } from 'src/helper/service/page/page.service';
import { User } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';
@Injectable()
export class DismantleResponseService extends PageService {
  constructor(
    @InjectRepository(User) private userRepo: Repository<User>,
    @InjectRepository(DismantleRequest)
    private dismantleRepo: Repository<DismantleRequest>,
    @InjectRepository(DismantleResponse)
    private dismantleResponseRepo: Repository<DismantleResponse>,
    @InjectRepository(ExcludeEntity)
    private excludeRepo: Repository<ExcludeEntity>,
    @InjectRepository(GPONAlert) private gponAlertRepo: Repository<GPONAlert>,
    @InjectRepository(BboneAlert) private bboneRepo: Repository<BboneAlert>,
    @InjectRepository(MetroAlert) private metroRepo: Repository<MetroAlert>,
    @InjectRepository(DatinData) private datinRepo: Repository<DatinData>,
    @InjectRepository(VoiceData) private voiceRepo: Repository<VoiceData>,
    @InjectRepository(BlackoutAlert)
    private blackOutRepo: Repository<BlackoutAlert>,
  ) {
    super();
  }

  async getDataMessage(request_id: number) {
    try {
      // eslint-disable-next-line prefer-const
      let message = [];
      const data = await this.dismantleRepo.findOne(request_id, {
        relations: ['sender'],
      });

      message.push({
        id: data.id,
        user: data.sender.nama,
        tipe: 'REQUESTER',
        konfirmasi: null,
        catatan: data.catatan,
      });

      const response = await this.dismantleResponseRepo.find({
        relations: ['user'],
        where: {
          request: data,
        },
      });

      for (const item of response) {
        const indexRes = item.response_int + 1;
        message.push({
          id: item.id,
          user: item.user.nama,
          tipe: typeDismanteResposne[item.type],
          konfirmasi: dismantleResponseOM[indexRes],
          catatan: item.catatan,
        });
      }

      return this.responseAPI('success', message);
    } catch (e) {
      throw new HttpException(e.message, 400);
    }
  }

  async postResponseOMRequest(
    request: CreateDismantleResponseDto,
    user: number,
  ) {
    try {
      for (const item of request.data) {
        const cek = await this.dismantleResponseRepo.findOne({
          where: {
            id: item.request_id as number,
            type: 1,
          },
        });
        if (cek)
          throw new HttpException(
            'Response has been created for dismantle',
            409,
          );

        // Save into dismantle request
        const userModel = await this.userRepo.findOne(user);
        if (!userModel) throw new HttpException('access forbiden', 403);

        // eslint-disable-next-line prefer-const
        let dismantleData = await this.dismantleRepo.findOne({
          where: {
            id: item.request_id as number,
            status: 0,
          },
        });
        if (!dismantleData) {
          throw new HttpException('Dismantle Request tidak ditemukan', 409);
        }
        dismantleData.status = 1;
        await this.dismantleRepo.save(dismantleData);

        await this.dismantleResponseRepo.save({
          request: dismantleData,
          user: userModel,
          response: request.response,
          response_int: request.response_int,
          type: 0,
          catatan: request.catatan,
        });
      }
      return {
        status: 200,
        message: 'Berhasil Menambahkan Data Dismantle',
        result: [],
      };
    } catch (e) {
      throw new HttpException(e.message, 400);
    }
  }

  async postResponseSPMRequest(
    request: CreateDismantleResponseDto,
    user: number,
  ) {
    try {
      for (const item of request.data) {
        const cek = await this.dismantleResponseRepo.findOne({
          where: {
            id: item.request_id as number,
            type: 2,
          },
        });
        if (cek)
          throw new HttpException(
            'Response has been created for dismantle',
            409,
          );

        // Save into dismantle request
        const userModel = await this.userRepo.findOne(user);
        if (!userModel) throw new HttpException('access forbiden', 403);

        // eslint-disable-next-line prefer-const
        let data = await this.dismantleRepo.findOne({
          where: {
            id: item.request_id as number,
            status: 1,
          },
        });
        if (!data) {
          throw new HttpException('Belum ada response dari OM', 409);
        }

        await this.dismantleResponseRepo.save({
          request: data,
          user: userModel,
          response: request.response,
          response_int: request.response_int,
          type: 1,
          catatan: request.catatan,
        });

        switch (request.response_int) {
          case 1:
            let models = null;
            switch (data.kategori.toLocaleUpperCase()) {
              case 'GPON':
                models = this.gponAlertRepo;
                break;
              case 'METRO':
                models = this.metroRepo;
                break;
              case 'BBONE':
                models = this.bboneRepo;
                break;
              case 'DATIN':
                models = this.datinRepo;
                break;
              case 'VOICE':
                models = this.voiceRepo;
                break;
              case 'BLACKOUT':
                models = this.blackOutRepo;
              default:
                new HttpException('Kategori tidak valid', 400);
                break;
            }
            const dataAlarm = await models.findOne({
              where: {
                uuid: data.uuid,
              },
            });
            dataAlarm.is_closed = true;
            dataAlarm.status = 'Closed';
            await models.save(dataAlarm);
            data.is_dismantle = 1;
            data.is_exclude = 1;
            break;
          case 2:
            data.is_dismantle = 0;
            data.is_exclude = 0;
            break;
          case 3:
            await this.dismantleRepo.save({
              uuid: data.uuid,
              kategori: data.kategori,
              sender_id: data.sender_id,
              catatan: data.catatan,
              status: 0,
              parent_id: data.id,
            });
            break;
          case 4:
            await this.excludeRepo.save({
              uuid: data.uuid,
              kategori: data.kategori,
              sender_id: data.sender_id,
              catatan: data.catatan + 'dari dismantle',
              status: 1,
            });
            break;
          default:
            throw new HttpException('Failed to dismantle', 409);
            break;
        }

        data.status = request.response_int == 1 ? 3 : 2;
        await this.dismantleRepo.save(data);
      }
      return {
        status: 200,
        message: 'Berhasil Menambahkan Data Dismantle',
        result: [],
      };
    } catch (e) {
      throw new HttpException(e.message, 400);
    }
  }

  async postExcludeAlert(request: NormalAlertDto, userId: number) {
    try {
      for (const item of request.data) {
        const data = await this.dismantleRepo.findOne({
          where: {
            id: item.id,
            is_exclude: 1,
            is_dismantle: 1,
          },
        });
        if (!data)
          throw new HttpException('Data tidak ada pada table dismantle', 404);

        const userModel = await this.userRepo.findOne(userId);
        if (!userModel) throw new HttpException('access forbiden', 403);

        await this.excludeRepo.save({
          uuid: data.uuid,
          kategori: data.kategori,
          sender_id: data.sender_id,
          catatan: data.catatan + 'dari dismantle',
          status: 2,
        });
      }
      return this.responseAPI('success', []);
    } catch (e) {
      throw new HttpException(e.message, 400);
    }
  }

  findAll() {
    return `This action returns all excludes`;
  }

  findOne(id: number) {
    return `This action returns a #${id} exclude`;
  }
}

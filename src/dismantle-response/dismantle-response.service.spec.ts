import { Test, TestingModule } from '@nestjs/testing';
import { DismantleResponseService } from './dismantle-response.service';

describe('DismantleResponseService', () => {
  let service: DismantleResponseService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DismantleResponseService],
    }).compile();

    service = module.get<DismantleResponseService>(DismantleResponseService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

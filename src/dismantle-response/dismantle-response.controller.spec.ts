import { Test, TestingModule } from '@nestjs/testing';
import { DismantleResponseController } from './dismantle-response.controller';

describe('DismantleResponseController', () => {
  let controller: DismantleResponseController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DismantleResponseController],
    }).compile();

    controller = module.get<DismantleResponseController>(DismantleResponseController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

// eslint-disable-next-line prettier/prettier
import { HttpException, HttpStatus, Injectable, NestMiddleware, Request, UseGuards } from "@nestjs/common";
import { Response, NextFunction } from 'express';
import { JwtGuard } from 'src/auth/jwt.guard';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/user.service';

@Injectable()
export class RoleMiddleware implements NestMiddleware {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  @UseGuards(JwtGuard)
  async use(@Request() req: any, res: Response, next: NextFunction) {
    const authHeaders = req.headers.authorization;

    if (authHeaders && (authHeaders as string).split(' ')[1]) {
      const token = (authHeaders as string).split(' ')[1];
      try {
        const payload = this.jwtService.verify(token);
        const userId = payload.id;
        const resp = await this.userService.findOne(userId);
        const user = resp.data;
        const permission = [];
        new Promise<void>((resolve, rejects) => {
          user.roles.map((val) => {
            val.permissions.map((val) => {
              if (!permission.includes(val.permission_name))
                permission.push(val.permission_name);

              return true;
            });
            return true;
          });
          resolve();
        });

        if (!(permission.length > 0))
          throw new HttpException('Please contact admin, to assign role', 403);
        req.body.permission = permission;
      } catch (exception) {
        throw new HttpException(exception, 400);
      }
    }
    next();
  }
}

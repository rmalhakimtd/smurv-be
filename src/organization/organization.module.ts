import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Regional } from 'src/alarm/entities/regional.entity';
import { Role } from 'src/role/entities/role.entity';
import { User } from 'src/user/entities/user.entity';
import { UserService } from 'src/user/user.service';
import { Organization } from './entity/organization.entity';
import { OrganizationController } from './organization.controller';
import { OrganizationService } from './organization.service';

@Module({
  imports: [TypeOrmModule.forFeature([Organization, Role, User, Regional])],
  controllers: [OrganizationController],
  providers: [OrganizationService, UserService],
  exports: [OrganizationService],
})
export class OrganizationModule {}

import { IsNotEmpty } from 'class-validator';
import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'm_organization' })
export class Organization {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  uuid: string;

  @Column()
  @IsNotEmpty()
  name: string;

  @ManyToOne(() => Organization, (organization) => organization.id)
  @JoinColumn({
    name: 'parent_id',
    referencedColumnName: 'id',
  })
  parent: Organization;

  @OneToMany(() => Organization, (organization) => organization.parent)
  children: Organization[];

  @Column()
  level: number;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn({ onUpdate: 'CURRENT_TIMESTAMP(6)' })
  updatedAt: Date;
}

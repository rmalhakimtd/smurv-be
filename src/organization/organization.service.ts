import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Regional } from 'src/alarm/entities/regional.entity';
import { PageService } from 'src/helper/service/page/page.service';
import { ILike, Repository } from 'typeorm';
import {
  CreateOrganizationDto,
  UpdateOrganizationDto,
} from './dto/organization.dto';
import { Organization } from './entity/organization.entity';

@Injectable()
export class OrganizationService extends PageService {
  constructor(
    @InjectRepository(Organization)
    private organizationRepo: Repository<Organization>,
    @InjectRepository(Regional)
    private regionalRepo: Repository<Regional>,
  ) {
    super();
  }

  async create(createOrganizationDto: CreateOrganizationDto) {
    const data = await this.organizationRepo.save(createOrganizationDto);

    return this.responseAPI('success create organization', data);
  }

  async findAll(filter) {
    return this.generatePage(filter, this.organizationRepo, {
      relations: ['children'],
      order: {
        level: 'ASC',
      },
    });
  }

  async findOne(id: number) {
    const data = await this.organizationRepo.findOne(id);
    if (!data) throw new HttpException('organization not found', 409);

    return this.responseAPI('success', data);
  }

  async findRegional(id: number) {
    const data = await this.organizationRepo.findOne(id, {
      relations: ['parent'],
    });
    if (!data) throw new HttpException('organization not found', 409);
    switch (data.level) {
      case 4:
        // console.log('4');
        this.findRegional(data.parent.id);
        break;
      case 3:
        this.findRegional(data.parent.id);
        // console.log('3');
        break;
      case 2:
        // console.log('2');
        break;
    }

    const regData = [];
    if (data.level == 0) {
      const regional = await this.regionalRepo.find();
      regional.map((val) => regData.push(val.kode));
    } else {
      const regionalData = await this.organizationRepo.findOne(data.parent.id, {
        relations: ['parent'],
      });
      const nameReg =
        data.level == 3 ? regionalData.parent.name : data.parent.name;
      const regional = await this.regionalRepo.findOne({
        where: {
          nama_regional: ILike(nameReg.substring(7, 17)),
        },
      });
      regData.push(regional.kode);
    }

    return regData;
  }

  async findOptions() {
    const data = await this.organizationRepo.find({
      select: ['id', 'name'],
    });

    return this.responseAPI('success', data);
  }

  async update(id: number, updateOrganizationDto: UpdateOrganizationDto) {
    updateOrganizationDto.id = id;
    const data = await this.organizationRepo.save(updateOrganizationDto);

    return this.responseAPI('success', data);
  }

  async remove(id: number) {
    const organization = await this.organizationRepo.findOne(id);
    if (!organization) throw new HttpException('organization not found', 409);

    const data = await this.organizationRepo.remove(organization);

    return this.responseAPI('success delete organization', data);
  }
}

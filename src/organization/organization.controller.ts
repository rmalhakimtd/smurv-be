import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  HttpException,
  Req,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { JwtGuard } from 'src/auth/jwt.guard';
import {
  createNewOrganization,
  getAllOrganizations,
  findByIdOrganization,
  deleteOrganization,
  updateOrganization,
} from 'src/helper/constant';
import { ResponseService } from 'src/helper/service/response/api.service';
import { UserService } from 'src/user/user.service';
import {
  CreateOrganizationDto,
  FindOrganizationDto,
  OrganizationIdDto,
  ResponseOrganizationDto,
  UpdateOrganizationDto,
} from './dto/organization.dto';
import { OrganizationService } from './organization.service';

@ApiTags('Organization')
@ApiBearerAuth()
@UseGuards(JwtGuard)
@Controller('organization')
export class OrganizationController extends ResponseService {
  constructor(
    private readonly organizationService: OrganizationService,
    private readonly userService: UserService,
  ) {
    super();
  }

  @Post()
  create(
    @Req() req: any,
    @Body() createOrganizationDto: CreateOrganizationDto,
  ) {
    const perms = req.body.permission;
    if (!perms.includes(createNewOrganization))
      throw new HttpException('Access Forbidden', 403);

    return this.organizationService.create(createOrganizationDto);
  }

  @Get()
  @ApiOkResponse({ type: ResponseOrganizationDto })
  findAll(@Req() req: any, @Query() filter: FindOrganizationDto) {
    const perms = req.body.permission;
    if (!perms.includes(getAllOrganizations))
      throw new HttpException('Access Forbidden', 403);

    return this.organizationService.findAll(filter);
  }

  @Get('options')
  findOptions() {
    return this.organizationService.findOptions();
  }

  @Get('regional')
  async findRegional(@Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(findByIdOrganization))
      throw new HttpException('Access Forbidden', 403);

    const user = await this.userService.findById(req.user.id);
    if (!user.organization)
      throw new HttpException('Organization not found', 409);
    return this.organizationService.findRegional(user.organization.id);
  }

  @Get(':id')
  findOne(@Req() req: any, @Param() id: OrganizationIdDto) {
    const perms = req.body.permission;
    if (!perms.includes(findByIdOrganization))
      throw new HttpException('Access Forbidden', 403);

    return this.organizationService.findOne(+id.id);
  }

  @Patch(':id')
  update(
    @Req() req: any,
    @Param() id: OrganizationIdDto,
    @Body() updateOrganizationDto: UpdateOrganizationDto,
  ) {
    const perms = req.body.permission;
    if (!perms.includes(updateOrganization))
      throw new HttpException('Access Forbidden', 403);

    return this.organizationService.update(+id.id, updateOrganizationDto);
  }

  @Delete(':id')
  remove(@Req() req: any, @Param() id: OrganizationIdDto) {
    const perms = req.body.permission;
    if (!perms.includes(deleteOrganization))
      throw new HttpException('Access Forbidden', 403);

    return this.organizationService.remove(+id.id);
  }
}

import { ApiProperty, OmitType, PartialType, PickType } from '@nestjs/swagger';
import { IsArray, IsOptional, IsString } from 'class-validator';
import { PageRequestDto, PageResponseDto } from 'src/helper/dto/page.dto';
import { IsExist } from 'src/helper/validator/exist-validator';
import { User } from 'src/user/entities/user.entity';
import { Organization } from '../entity/organization.entity';

export class OrganizationDto {
  @ApiProperty()
  @IsOptional()
  @IsExist([Organization, 'id'])
  id: number;

  @ApiProperty()
  @IsOptional()
  name: string;

  @ApiProperty()
  @IsOptional()
  parent_id: number | null;

  @ApiProperty()
  @IsOptional()
  level: number;

  @ApiProperty()
  @IsOptional()
  user_id: number | null;
}

export class FindOrganizationDto extends PageRequestDto {
  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  name: string;
}

export class ResponseOrganizationDto extends PageResponseDto {
  @ApiProperty({ type: [OrganizationDto] })
  data: OrganizationDto[];
}

export class CreateOrganizationDto extends OmitType(OrganizationDto, ['id']) {}
export class OrganizationIdDto extends PickType(OrganizationDto, ['id']) {}
export class UpdateOrganizationDto extends PartialType(OrganizationDto) {}

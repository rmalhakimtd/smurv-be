import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  Req,
  HttpException,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { UserService } from './user.service';
import {
  AssignOrganization,
  CreateUserDto,
  FindUserDto,
  ResponseUserDto,
  UserIdDto,
} from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { JwtGuard } from 'src/auth/jwt.guard';
import { ResponseService } from 'src/helper/service/response/api.service';
import {
  createNewUser,
  getAllUsers,
  findByIdUser,
  updateUser,
  deleteUser,
} from '../helper/constant';
import { FileInterceptor } from '@nestjs/platform-express';
@ApiTags('User')
@ApiBearerAuth()
@UseGuards(JwtGuard)
@Controller('user')
export class UserController extends ResponseService {
  constructor(private readonly userService: UserService) {
    super();
  }

  @Post('import')
  @UseInterceptors(FileInterceptor('file'))
  async importDataExcel(@UploadedFile() file: Express.Multer.File) {
    return this.userService.importData(file);
  }

  @Post('firebase-update')
  updateFirebase(@Req() req: any, @Body() body: any) {
    // Semua user bisa, jadi tidak perlu access permission
    return this.userService.updateFirebase(req.user.id, body.firebase_id);
  }

  @Get('permission')
  getPermission(@Req() req: any) {
    return this.userService.getPermission(req.user.id);
  }

  @Post()
  create(@Req() req: any, @Body() createUserDto: CreateUserDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(createNewUser))
      throw new HttpException('Access Forbidden', 403);
    return this.userService.create(createUserDto);
  }

  @Get()
  @ApiOkResponse({ type: ResponseUserDto })
  findAll(@Req() req: any, @Query() filter: FindUserDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(getAllUsers))
      throw new HttpException('Access Forbidden', 403);

    return this.userService.findAll(filter);
  }

  @Get(':id')
  findOne(@Req() req: any, @Param() id: UserIdDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(findByIdUser))
      throw new HttpException('Access Forbidden', 403);

    return this.userService.findOne(+id.id);
  }

  @Post(':id/organization')
  assignOrganization(
    @Req() req: any,
    @Param() id: UserIdDto,
    @Body() body: AssignOrganization,
  ) {
    const perms = req.body.permission;
    if (!perms.includes(updateUser))
      throw new HttpException('Access Forbidden', 403);

    return this.userService.assignOrganization(+id.id, body.id);
  }

  @Post('activeflag/:id')
  async activeFlagUser(@Req() req: any, @Param() id: UserIdDto) {
    const perms = req.body.permission;
    if (!perms.includes(updateUser))
      throw new HttpException('Access Forbidden', 403);

    return this.userService.setActiveFlag(+id.id);
  }

  @Patch(':id')
  update(
    @Req() req: any,
    @Param() id: UserIdDto,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(updateUser))
      throw new HttpException('Access Forbidden', 403);

    return this.userService.update(+id.id, updateUserDto);
  }

  @Delete(':id')
  remove(@Req() req: any, @Param() id: UserIdDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(deleteUser))
      throw new HttpException('Access Forbidden', 403);

    return this.userService.remove(+id.id);
  }
}

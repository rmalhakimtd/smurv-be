import { OmitType, PickType } from '@nestjs/swagger';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import {
  IsArray,
  IsEmail,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { PageRequestDto, PageResponseDto } from 'src/helper/dto/page.dto';
import { IsExist } from 'src/helper/validator/exist-validator';
import { IsUnique } from 'src/helper/validator/unique-validator';
import { Organization } from 'src/organization/entity/organization.entity';
import { Role } from 'src/role/entities/role.entity';
import { User } from '../entities/user.entity';

export class UserDto {
  @ApiProperty()
  @IsOptional()
  @IsExist([User, 'id'])
  id: number;

  @ApiProperty({ required: true })
  @IsString()
  @MaxLength(200)
  @IsUnique([User, 'username'])
  username: string;

  @ApiProperty({ required: true })
  @IsString()
  @MaxLength(200)
  nama: string;

  @ApiProperty({ required: true })
  @IsString()
  password: string;

  @ApiProperty()
  @IsOptional()
  ldapflag: boolean;

  @ApiProperty()
  @IsOptional()
  activeflag: boolean;

  @ApiProperty()
  @IsOptional()
  registration_id: string;

  @ApiProperty()
  @IsOptional()
  jenis_user: string;

  @ApiProperty()
  @IsEmail()
  @IsUnique([User, 'email'])
  email: string;

  @ApiProperty()
  @IsString()
  no_hp: string;

  @ApiProperty()
  @IsString()
  ktp: string;

  @ApiProperty()
  @IsOptional()
  exp_date: Date;

  @ApiProperty()
  @IsArray()
  roles: Role[];

  @ApiProperty()
  @IsOptional()
  firebase_id: string;

  @ApiProperty()
  @IsOptional()
  organization: Organization;
}

export class FindUserDto extends PageRequestDto {
  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  username: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  nama: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  @IsEmail()
  email: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  no_hp: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  ktp: string;
}

export class ResponseUserDto extends PageResponseDto {
  @ApiProperty({ type: [UserDto] })
  data: UserDto[];
}

export class AssignOrganization {
  @ApiProperty({ required: true })
  @IsNumber()
  id: number;
}

export class CreateUserDto extends OmitType(UserDto, ['id']) {}
export class UserIdDto extends PickType(UserDto, ['id']) {}

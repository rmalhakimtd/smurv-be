import { HttpException, Injectable, UploadedFile } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import * as bcyrpt from 'bcrypt';
import { Role } from 'src/role/entities/role.entity';
import { PageService } from 'src/helper/service/page/page.service';
import { isEmpty } from 'src/helper/validator/empty-validator';
import { Organization } from 'src/organization/entity/organization.entity';
import * as xlsx from 'xlsx';
import { WorkBook, WorkSheet } from 'xlsx';
import { exit } from 'process';
@Injectable()
export class UserService extends PageService {
  constructor(
    @InjectRepository(User) private userRepo: Repository<User>,
    @InjectRepository(Role) private roleRepo: Repository<Role>,
    @InjectRepository(Organization)
    private organizationRepo: Repository<Organization>,
  ) {
    super();
  }

  async setActiveFlag(userId: number) {
    const user = await this.userRepo.findOne(userId);
    if (!user) throw new HttpException('User not found', 409);

    user.activeflag = !user.activeflag;
    return this.userRepo.save(user);
  }

  async getPermission(userId: number) {
    const user = await this.userRepo.findOne(userId, {
      relations: ['roles', 'roles.permissions'],
    });
    const permission = [];
    new Promise<void>((resolve, rejects) => {
      user.roles.map((val) => {
        val.permissions.map((val) => {
          if (!permission.includes(val.permission_name))
            permission.push(val.permission_name);
          return true;
        });
        return true;
      });
      resolve();
    });
    if (!(permission.length > 0))
      throw new HttpException('Please contact admin, to assign role', 403);

    return this.responseAPI('success', permission);
  }

  async create(createUserDto: CreateUserDto) {
    createUserDto.password = this.hashPassword(createUserDto.password);

    const roles = await this.roleRepo.findByIds(createUserDto.roles);
    if (isEmpty(roles)) throw new HttpException('Roles is not valid', 409);

    if (createUserDto.organization && createUserDto.organization != undefined) {
      const org = await this.organizationRepo.findOne(
        createUserDto.organization,
      );
      if (isEmpty(org))
        throw new HttpException('Organization is not valid', 409);
      createUserDto.organization = org;
    }

    createUserDto.roles = roles;


    const data = await this.userRepo.save(createUserDto);

    return this.responseAPI('success create user', data);
  }

  async importData(@UploadedFile() file: Express.Multer.File) {
    const workBook: WorkBook = await new Promise((resolve, reject) => {
      // const buffer = Buffer.concat(file.buffer);
      resolve(xlsx.read(file.buffer, { type: 'buffer' }));
    });

    const sheet: WorkSheet = workBook.Sheets[workBook.SheetNames[0]];
    const range = xlsx.utils.decode_range(sheet['!ref']);

    const created = [];
    const updated = [];
    for (let R = range.s.r; R <= range.e.r; ++R) {
      if (R === 0 || !sheet[xlsx.utils.encode_cell({ c: 0, r: R })]) {
        continue;
      }

      let col = 0;

      let entity = {
        id: null,
        username: sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v,
        nama: sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v,
        password: sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v,
        ldapflag:
          sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v == 1
            ? true
            : false,
        activeflag:
          sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v == 1
            ? true
            : false,
        registration_id: sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v,
        email: sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v,
        no_hp: sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v,
        ktp: sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v,
        exp_date: sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v,
        type: sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v,
        jenis_user: sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v,
        organization: sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v,
        firebase_id: sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v,
        roles: null,
      };

      let roles = sheet[xlsx.utils.encode_cell({ c: col++, r: R })]?.v;
      let role = null;
      if (typeof roles === 'string') {
        const strRoles = roles.split(';');
        roles = strRoles.map((val) => parseInt(val));
        role = await this.roleRepo.findByIds(roles);
      } else {
        role = [];
        role.push(await this.roleRepo.findOne(roles));
      }

      entity.roles = role;

      const exists = await this.userRepo.findOne({
        where: { username: entity.username },
      });

      if (exists) {
        entity = { ...entity, id: exists.id };
        updated.push(entity.username);
      } else {
        created.push(entity.username);
      }
      await this.userRepo.save(entity);
    }

    return this.responseAPI('success import data', {
      created: created,
      updated: updated,
    });
  }

  findAll(filter) {
    return this.generatePage(filter, this.userRepo, { relations: ['roles'] });
  }

  async findOne(id: number) {
    const data = await this.userRepo.findOne(id, {
      select: ['id'],
      relations: ['roles', 'roles.permissions'],
    });
    if (!data) throw new HttpException('User not found', 409);
    return this.responseAPI('user is found', data);
  }

  async assignOrganization(id: number, orgId: number) {
    const user = await this.userRepo.findOne(id);
    if (!user) throw new HttpException('User not found', 409);

    const organization = await this.organizationRepo.findOne(orgId);
    if (!organization) throw new HttpException('Organization not found', 409);

    user.organization = organization;

    return this.userRepo.save(user);
  }

  async updateFirebase(id: number, firebase: string) {
    const user = await this.userRepo.findOne(id);
    if (!user) throw new HttpException('user not found', 404);

    user.firebase_id = firebase;
    return this.userRepo.save(user);
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    updateUserDto.id = id;
    if (updateUserDto.password) {
      updateUserDto.password = this.hashPassword(updateUserDto.password);
    }

    const roles = await this.roleRepo.findByIds(updateUserDto.roles);
    if (isEmpty(roles)) throw new HttpException('Roles is not valid', 409);

    updateUserDto.roles = roles;

    if (updateUserDto.organization && updateUserDto.organization != undefined) {
      const org = await this.organizationRepo.findOne(
        updateUserDto.organization,
      );
      if (isEmpty(org))
        throw new HttpException('Organization is not valid', 409);
      updateUserDto.organization = org;
    }

    return this.userRepo.save(updateUserDto);
  }

  findById(id: number) {
    return this.userRepo
      .createQueryBuilder('m_users')
      .where(' m_users.id=:s', { s: id })
      .leftJoinAndSelect('m_users.organization', 'organization')
      .leftJoinAndSelect('m_users.roles', 'roles')
      .getOne();
  }

  findByUsername(username: string) {
    return this.userRepo
      .createQueryBuilder('m_users')
      .where('username=:s', { s: username })
      .leftJoinAndSelect('m_users.organization', 'organization')
      .getOne();
  }

  async getUserByRole(role: string) {
    const data = await this.roleRepo.findOne({
      where: {
        role_name: ILike(`%${role}%`),
      },
      relations: ['users'],
    });
    if (!data) throw new HttpException('Role not found', 409);

    return data.users;
  }

  async remove(id: number) {
    const user: User = await this.userRepo.findOne(id);
    if (!user) throw new HttpException('user not found', 409);

    return this.userRepo.remove(user);
  }

  hashPassword(password: string) {
    const hash = bcyrpt.hashSync(password, 10);
    return hash;
  }

  compare(plainPassword, hash) {
    const valid = bcyrpt.compareSync(plainPassword, hash);
    return valid;
  }
}

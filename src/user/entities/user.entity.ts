import { IsNotEmpty } from 'class-validator';
import { Organization } from 'src/organization/entity/organization.entity';
import { Role } from 'src/role/entities/role.entity';
import { AlarmSummary } from 'src/alarm/entities/alarm.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  Generated,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';

export enum JenisUser {
  ORGANIK = 'organik',
  NONORGANIK = 'nonorganik',
  API = 'api',
}

@Entity({ name: 'm_users' })
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  uuid: string;

  @Column()
  @Unique(['username'])
  username: string;

  @Column()
  @IsNotEmpty()
  nama: string;

  @Column()
  @IsNotEmpty()
  password: string;

  @Column({ default: false })
  ldapflag: boolean;

  @Column({ default: false })
  activeflag: boolean;

  @Column({ nullable: true })
  registration_id: string;

  @Column({
    type: 'enum',
    enum: JenisUser,
    default: JenisUser.NONORGANIK,
  })
  jenis_user: string;

  @Column()
  email: string;

  @Column()
  no_hp: string;

  @Column()
  ktp: string;

  @Column({ nullable: true })
  exp_date: Date;

  @Column({ nullable: true })
  type: string;

  @ManyToMany(() => Role)
  @JoinTable({
    name: 'users_roles',
    joinColumn: { name: 'user_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'role_id', referencedColumnName: 'id' },
  })
  roles: Role[];

  @ManyToOne(() => Organization, (organization) => organization.id)
  organization: Organization;

  @Column({ nullable: true })
  telegram_id: string;

  @Column({ nullable: true })
  telegram_username: string;

  @Column({ nullable: true })
  telegram_verification: string;

  @Column({ nullable: true })
  @DeleteDateColumn()
  deleted_at: Date;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn({ onUpdate: 'CURRENT_TIMESTAMP(6)' })
  updatedAt: Date;

  @OneToMany(() => AlarmSummary, (alarm) => alarm.acknowledge_user)
  acknowledges: AlarmSummary[];

  @Column()
  firebase_id: string;
}

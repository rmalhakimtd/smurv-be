import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  Param,
  Patch,
  Post,
  Query,
  Req,
} from '@nestjs/common';
import { ApiOkResponse } from '@nestjs/swagger';
import {
  createNewDevice,
  getAllDevices,
  findByIdDevice,
  deleteDevice,
} from 'src/helper/constant';
import { ResponseService } from 'src/helper/service/response/api.service';
import { DeviceService } from './device.service';
import {
  CreateDeviceDto,
  ResponseDeviceDto,
  FindDeviceDto,
  DeviceIdDto,
  UpdateDeviceDto,
} from './dto/device.dto';

@Controller('device')
export class DeviceController extends ResponseService {
  constructor(private readonly deviceService: DeviceService) {
    super();
  }

  @Post()
  create(@Req() req: any, @Body() createDeviceDto: CreateDeviceDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(createNewDevice))
      throw new HttpException('Access Forbidden', 403);

    return this.deviceService.create(createDeviceDto);
  }

  @Get()
  @ApiOkResponse({ type: ResponseDeviceDto })
  findAll(@Req() req: any, @Query() filter: FindDeviceDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(getAllDevices))
      throw new HttpException('Access Forbidden', 403);

    return this.deviceService.findAll(filter);
  }

  @Get('options')
  findOptions() {
    return this.deviceService.findOptions();
  }

  @Get(':id')
  findOne(@Req() req: any, @Param() id: DeviceIdDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(findByIdDevice))
      throw new HttpException('Access Forbidden', 403);

    return this.deviceService.findOne(+id.id);
  }

  @Patch(':id')
  update(
    @Req() req: any,
    @Param() id: DeviceIdDto,
    @Body() updateDeviceDto: UpdateDeviceDto,
  ) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes('updateDevice'))
      throw new HttpException('Access Forbidden', 403);

    return this.deviceService.update(+id.id, updateDeviceDto);
  }

  @Delete(':id')
  remove(@Req() req: any, @Param() id: DeviceIdDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes(deleteDevice))
      throw new HttpException('Access Forbidden', 403);

    return this.deviceService.remove(+id.id);
  }
}

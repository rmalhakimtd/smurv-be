import { ApiProperty, OmitType, PartialType, PickType } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';
import { PageRequestDto, PageResponseDto } from 'src/helper/dto/page.dto';
import { IsExist } from 'src/helper/validator/exist-validator';
import { Device } from '../entity/device.entity';

export class DeviceDto {
  @ApiProperty()
  @IsOptional()
  @IsExist([Device, 'id'])
  id: number;
  @ApiProperty()
  @IsString()
  device_name: string;

  @ApiProperty()
  @IsString()
  device_code: string;
}

export class FindDeviceDto extends PageRequestDto {
  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  device_name: string;
}

export class ResponseDeviceDto extends PageResponseDto {
  @ApiProperty({ type: [DeviceDto] })
  data: DeviceDto[];
}

export class CreateDeviceDto extends OmitType(DeviceDto, ['id']) {}
export class DeviceIdDto extends PickType(DeviceDto, ['id']) {}
export class UpdateDeviceDto extends PartialType(DeviceDto) {}

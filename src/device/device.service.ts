import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PageService } from 'src/helper/service/page/page.service';
import { Repository } from 'typeorm';
import { CreateDeviceDto, UpdateDeviceDto } from './dto/device.dto';
import { Device } from './entity/device.entity';

@Injectable()
export class DeviceService extends PageService {
  constructor(
    @InjectRepository(Device)
    private deviceRepo: Repository<Device>,
  ) {
    super();
  }

  async create(createDeviceDto: CreateDeviceDto) {
    const data = await this.deviceRepo.save(createDeviceDto);

    return this.responseAPI('success create Device', data);
  }

  async findAll(filter) {
    const data = await this.generatePage(filter, this.deviceRepo);

    return this.responseAPI('success', data);
  }

  async findOne(id: number) {
    const data = await this.deviceRepo.findOne(id);
    if (!data) throw new HttpException('Device not found', 409);

    return this.responseAPI('success', data);
  }

  async findOptions() {
    const data = await this.deviceRepo.find({
      select: ['id', 'device_name'],
    });

    return this.responseAPI('success', data);
  }

  async update(id: number, updateDeviceDto: UpdateDeviceDto) {
    updateDeviceDto.id = id;
    const data = await this.deviceRepo.save(updateDeviceDto);

    return this.responseAPI('success', data);
  }

  async remove(id: number) {
    const organization = await this.deviceRepo.findOne(id);
    if (!organization) throw new HttpException('Device not found', 409);

    const data = await this.deviceRepo.remove(organization);

    return this.responseAPI('success delete Device', data);
  }
}

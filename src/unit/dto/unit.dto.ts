import { ApiProperty, OmitType, PartialType, PickType } from '@nestjs/swagger';
import { IsArray, IsOptional, IsString } from 'class-validator';
import { PageRequestDto, PageResponseDto } from 'src/helper/dto/page.dto';
import { IsExist } from 'src/helper/validator/exist-validator';
import { User } from 'src/user/entities/user.entity';
import { Unique } from 'typeorm';
import { Unit } from '../entity/unit.entity';

export class UnitDto {
  @ApiProperty()
  @IsOptional()
  @IsExist([Unit, 'id'])
  id: number;

  @ApiProperty()
  @IsOptional()
  kode_divisi: string;

  @ApiProperty()
  @IsOptional()
  objectid_posisi: string;

  @ApiProperty()
  @IsOptional()
  nama_posisi: string;

  @ApiProperty()
  @IsOptional()
  band: string;

  @ApiProperty()
  @IsOptional()
  @Unique(['objectid_subunit'])
  objectid_subunit: string;

  @ApiProperty()
  @IsOptional()
  sub_unit: string;

  @ApiProperty()
  @IsOptional()
  unit: string;

  @ApiProperty()
  @IsOptional()
  egm_deputy: string;

  @ApiProperty()
  @IsOptional()
  objectid_parent: string;

  @ApiProperty()
  @IsOptional()
  flag_chief: string;

  @ApiProperty()
  @IsOptional()
  c_kode_posisi: string;

  @ApiProperty()
  @IsOptional()
  user_id: number | null;
}

export class FindUnitDto extends PageRequestDto {
  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  unit: string;

  @ApiProperty({ required: false })
  @IsString()
  @IsOptional()
  sub_unit: string;
}

export class ResponseUnitDto extends PageResponseDto {
  @ApiProperty({ type: [UnitDto] })
  data: UnitDto[];
}

export class CreateUnitDto extends OmitType(UnitDto, ['id']) {}
export class UnitIdDto extends PickType(UnitDto, ['id']) {}
export class UpdateUnitDto extends PartialType(UnitDto) {}

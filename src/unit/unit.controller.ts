import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Req,
  HttpException,
} from '@nestjs/common';
import { Request } from 'express';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { JwtGuard } from 'src/auth/jwt.guard';
import { ResponseService } from 'src/helper/service/response/api.service';
import {
  CreateUnitDto,
  ResponseUnitDto,
  UnitIdDto,
  UpdateUnitDto,
} from './dto/unit.dto';
import { UnitService } from './unit.service';

@ApiTags('Unit')
@ApiBearerAuth()
@UseGuards(JwtGuard)
@Controller('unit')
export class UnitController extends ResponseService {
  constructor(private readonly unitService: UnitService) {
    super();
  }

  @Post()
  create(@Req() req: any, @Body() createUnitRepo: CreateUnitDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes('createNewUnit'))
      throw new HttpException('Access Forbidden', 403);

    return this.unitService.create(createUnitRepo);
  }

  @Get()
  @ApiOkResponse({ type: ResponseUnitDto })
  async findAll(@Req() req: Request) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes('getAllUnit'))
      throw new HttpException('Access Forbidden', 403);

    const models = await this.unitService.queryBuilder('m_unit');

    const sort: any = req.query.sortBy;
    const sortDirection: any = req.query.sortDirection;

    if (sort && sortDirection) {
      models.orderBy(`m_unit.${sort}`, sortDirection.toUpperCase());
    }

    const page: number = parseInt(req.query.page as any) || 1;
    const perpage: number = parseInt(req.query.perpage as any) || 20;
    const total = await models.getCount();

    models.offset((page - 1) * perpage).limit(perpage);

    models.distinctOn([
      'm_unit.objectid_subunit',
      'm_unit.sub_unit',
      'm_unit.unit',
    ]);

    return {
      total,
      page,
      pages: Math.ceil(total / perpage),
      data: await models.getMany(),
    };
  }

  @Get('options')
  findOptions() {
    return this.unitService.findOptions();
  }

  @Get(':id')
  findOne(@Req() req: any, @Param() id: UnitIdDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes('findByIdUnit'))
      throw new HttpException('Access Forbidden', 403);

    return this.unitService.findOne(+id.id);
  }

  @Patch(':id')
  update(
    @Req() req: any,
    @Param() id: UnitIdDto,
    @Body() updateUnitDto: UpdateUnitDto,
  ) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes('updateUnit'))
      throw new HttpException('Access Forbidden', 403);

    return this.unitService.update(+id.id, updateUnitDto);
  }

  @Delete(':id')
  remove(@Req() req: any, @Param() id: UnitIdDto) {
    // eslint-disable-next-line prefer-const
    let perms = req.body.permission;
    if (!perms.includes('deleteUnit'))
      throw new HttpException('Access Forbidden', 403);

    return this.unitService.remove(+id.id);
  }
}

import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PageService } from 'src/helper/service/page/page.service';
import { Repository } from 'typeorm';
import { CreateUnitDto, UpdateUnitDto } from './dto/unit.dto';
import { Unit } from './entity/unit.entity';

@Injectable()
export class UnitService extends PageService {
  constructor(
    @InjectRepository(Unit)
    private unitRepo: Repository<Unit>,
  ) {
    super();
  }

  async queryBuilder(alias: string) {
    return this.unitRepo.createQueryBuilder(alias);
  }

  async create(createUnitDto: CreateUnitDto) {
    const data = await this.unitRepo.save(createUnitDto);

    return this.responseAPI('success create unit', data);
  }

  async findAll(filter) {
    const data = await this.generatePage(filter, this.unitRepo, {
      relations: ['user'],
    });

    return this.responseAPI('success', data);
  }

  async findOne(id: number) {
    const data = await this.unitRepo.findOne(id);
    if (!data) throw new HttpException('menu not found', 409);

    return this.responseAPI('success', data);
  }

  async findOptions() {
    const data = await this.unitRepo.find({
      select: ['id', 'unit', 'sub_unit'],
    });

    return this.responseAPI('success', data);
  }

  async update(id: number, updateUnitDto: UpdateUnitDto) {
    updateUnitDto.id = id;
    const data = await this.unitRepo.save(updateUnitDto);

    return this.responseAPI('success', data);
  }

  async remove(id: number) {
    const unit = await this.unitRepo.findOne(id);
    if (!unit) throw new HttpException('unit not found', 409);

    const data = await this.unitRepo.remove(unit);

    return this.responseAPI('success delete unit', data);
  }
}

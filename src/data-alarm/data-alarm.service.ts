import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IS_UUID, UUIDVersion } from 'class-validator';
import * as moment from 'moment';
import { PageService } from 'src/helper/service/page/page.service';
import { In, Repository } from 'typeorm';
import { GPONAlert } from '../alarm/entities/gpon.entity';
import { TrunkAlert } from '../alarm/entities/trunk.entity';
import { ViewAlarmSummary } from '../alarm/entities/view-alarm.entity';

@Injectable()
export class DataAlarmService extends PageService {
  constructor(
    @InjectRepository(TrunkAlert)
    private trunkAlertRepo: Repository<TrunkAlert>,
    @InjectRepository(GPONAlert) private gponAlertRepo: Repository<GPONAlert>,
    @InjectRepository(ViewAlarmSummary)
    private alarmRepo: Repository<ViewAlarmSummary>,
  ) {
    super();
  }

  async setStatusAck(
    uuid: UUIDVersion[] | UUIDVersion,
    userId: any,
    type: string,
  ) {
    let models = null;
    switch (type.toLocaleUpperCase()) {
      case 'GPON':
        models = this.gponAlertRepo;
        break;
      case 'METRO':
        models = this.trunkAlertRepo;
        break;
      case 'BBONE':
        models = this.trunkAlertRepo;
        break;
      default:
        models = this.trunkAlertRepo;
        break;
    }

    let data = null;
    if (typeof uuid === 'number') {

      data = await models.findOne({
        where: {
          uuid: uuid,
        },
      });
    } else {
      data = await models.find({
        where: {
          uuid: uuid,
        },
      });
    }

    if (!data) throw new HttpException('Alarm not found', 409);

    if (typeof uuid === 'number') {
      data.acknowledge_by = userId;
      data.acknowledge_status = 1;
      data.acknowledge_time = moment().toDate();
    } else {

      data.map((val) => {
        val.acknowledge_by = userId;
        val.acknowledge_status = 1;
        val.acknowledge_time = moment().toDate();
      });
    }

    const update = await models.save(data);

    return this.responseAPI('Alarm is set Acknowledge', update);
  }

  async openAcknowledge(uuid: number | number[], type: string) {
    let models = null;
    switch (type) {
      case 'GPON':
        models = this.gponAlertRepo;
        break;
      case 'METRO':
        models = this.trunkAlertRepo;
        break;
      case 'BBONE':
        models = this.trunkAlertRepo;
        break;
      default:
        models = this.trunkAlertRepo;
        break;
    }

    let data = null;
    if (typeof uuid === 'number') {
      data = await models.findOne({
        where: {
          uuid: uuid,
        },
      });
    } else if (Array.isArray(uuid)) {
      data = await models.findByIds({
        where: {
          uuid: uuid,
        },
      });
    }
    if (!data) throw new HttpException('Alarm not found', 409);

    // eslint-disable-next-line prefer-const
    if (typeof uuid === 'number') {
      data.acknowledge_status = 2;
    } else {
      data.map((val) => {
        val.acknowledge_status = 2;
      });
    }

    return models.save(data);
  }

  async getDetailAlarm(uuid: number, type: string) {
    let models = null;
    switch (type) {
      case 'GPON':
        models = this.gponAlertRepo;
        break;
      case 'METRO':
        models = this.trunkAlertRepo;
        break;
      case 'BBONE':
        models = this.trunkAlertRepo;
        break;
      default:
        models = this.trunkAlertRepo;
        break;
    }

    const data = await this.gponAlertRepo.findOne({
      where: {
        uuid: uuid,
      },
    });
    if (!data) throw new HttpException('data not found', 404);

    return this.responseAPI('success', data);
  }

  getAllAlarm(filter) {
    return this.generatePage(filter, this.alarmRepo);
  }
}

import { InjectQueue } from '@nestjs/bull';
import {
  Body,
  Controller,
  Get,
  HttpException,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Queue } from 'bull';
import { JwtGuard } from 'src/auth/jwt.guard';
import {
  findByIdAlarmSummaries,
  getAllAlarmSummaries,
  setAcknowledgeAlarm,
} from 'src/helper/constant';
import { DataAlarmService } from './data-alarm.service';
import { FindViewAlarm, SetAcknowledge } from '../alarm/dto/datasource.dto';

@ApiTags('Data Alarm')
@ApiBearerAuth()
@UseGuards(JwtGuard)
@Controller('data-alarm')
export class DataAlarmController {
  constructor(
    @InjectQueue('alarm') private readonly alarmQueue: Queue,
    private readonly dataAlarmService: DataAlarmService,
  ) {}

  @Post('acknowledge')
  async setAcknowledge(@Body() body: any, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(setAcknowledgeAlarm))
      throw new HttpException('Access Forbidden', 403);

    const uuidAlarm = body.uuid;
    const type = body.type as string;
    const idUser = req.user.id;
    try {
      // const job = await this.alarmQueue.add('acknowledge', {
      //   uuidAlarm,
      //   type,
      // });

      return this.dataAlarmService.setStatusAck(uuidAlarm, idUser, type);
    } catch (e) {
      console.log(e);
      return {
        status: 400,
        message: 'Terjadi kesalahan',
        error: e.message,
      };
    }
  }

  @Get('detail')
  getDetailAlarm(@Req() req: any, @Query() query: FindViewAlarm) {
    const perms = req.body.permission;
    if (!perms.includes(findByIdAlarmSummaries))
      throw new HttpException('Access Forbidden', 403);

    const uuid = query.uuid;
    console.log(query);
    const type = query.kategori as string;
    // return this.dataAlarmService.getDetailAlarm(uuid, type);
  }

  @Get()
  getAllDataAlarm(@Req() req: any, @Query() query: FindViewAlarm) {
    const perms = req.body.permission;
    if (!perms.includes(getAllAlarmSummaries))
      throw new HttpException('Access Forbidden', 403);

    const filter = query;
    return this.dataAlarmService.getAllAlarm(filter);
  }
}

import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataAlarmController } from './data-alarm.controller';
import { DataAlarmService } from './data-alarm.service';
import { GPONAlert } from '../alarm/entities/gpon.entity';
import { TrunkAlert } from '../alarm/entities/trunk.entity';
import { ViewAlarmSummary } from '../alarm/entities/view-alarm.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([GPONAlert, TrunkAlert, ViewAlarmSummary]),
    BullModule.registerQueue({
      name: 'alarm',
    }),
  ],
  controllers: [DataAlarmController],
  providers: [DataAlarmService],
})
export class DataAlarmModule {}

import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { CreateMailDto } from './dto/mail.dto';
import { PageService } from 'src/helper/service/page/page.service';
import { google, Auth } from 'googleapis';

@Injectable()
export class MailService extends PageService {
  oauthClient: Auth.OAuth2Client;
  constructor(private mailerService: MailerService) {
    super();
    // const clientID = process.env.MAIL_CLIENT_ID;
    // const clientSecret = process.env.MAIL_CLIENT_SECRET;

    // this.oauthClient = new google.auth.OAuth2(clientID, clientSecret);
    // this.oauthClient.setCredentials({
    //   refresh_token: process.env.MAIL_REFRESH_TOKEN,
    // });

    // const accessToken = this.oauthClient.getAccessToken();
  }

  async sendNotifEmail(mail: any) {
    const mailData = [];
    for (const item of mail.data) {
      const mailOptions = {
        from: item.from,
        to: item.to,
        subject: item.subject,
        generateTextFromHTML: true,
        html: item.html,
      };

      const mailSend = await this.mailerService.sendMail(mailOptions);
      mailData.push(mailSend);
    }
    return this.responseAPI('success', mailData);
  }
}

import { Test, TestingModule } from '@nestjs/testing';
import { DismantleController } from './dismantle.controller';

describe('DismantleController', () => {
  let controller: DismantleController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DismantleController],
    }).compile();

    controller = module.get<DismantleController>(DismantleController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

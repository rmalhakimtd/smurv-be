import {
  Body,
  Controller,
  Get,
  HttpException,
  Inject,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtGuard } from 'src/auth/jwt.guard';
import {
  dismantleResponseOM,
  dismantleResponseSPM,
  getAllDismantleRequest,
  includeDismantleList,
  sendDismantleRequest,
  sendResponseOM,
  sendResponseSPM,
  setExcludeAlert,
  setNormalAlert,
} from 'src/helper/constant';
import { DismantleService } from './dismantle.service';
import {
  CreateDismantleDto,
  CreateDismantleListDto,
  CreateDismantleResponseDto,
  NormalAlertDto,
} from './dto/dismantle.dto';
import { DismantleResponseService } from 'src/dismantle-response/dismantle-response.service';
import { UserService } from 'src/user/user.service';
import { OrganizationService } from 'src/organization/organization.service';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
// import { ClientProxy } from '@nestjs/microservices';

@Controller('dismantle')
@ApiTags('Dismantle')
@ApiBearerAuth()
@UseGuards(JwtGuard)
export class DismantleController {
  constructor(
    @InjectQueue('notification') private readonly dismantleQueue: Queue,
    private readonly dismantleService: DismantleService,
    private readonly dismantleResponseService: DismantleResponseService,
    private readonly userService: UserService,
    // @Inject('NOTIFICATION_SERVICE') private client: ClientProxy,
    private readonly organizationService: OrganizationService,
  ) {}

  @Post('exclude')
  setExcludeAlert(@Body() body: NormalAlertDto, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(setExcludeAlert))
      throw new HttpException('Access Forbidden', 403);

    try {
      return this.dismantleResponseService.postExcludeAlert(body, req.user.id);
    } catch (e) {
      return {
        status: 400,
        message: 'Terjadi kesalahan',
        error: e.message,
      };
    }
  }

  @Post('normal')
  setNormalAlert(@Body() body: CreateDismantleListDto, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(setNormalAlert))
      throw new HttpException('Access Forbidden', 403);

    try {
      return this.dismantleService.postNormalAlert(body, req.user.id);
    } catch (e) {
      return {
        status: 400,
        message: 'Terjadi kesalahan',
        error: e.message,
      };
    }
  }

  @Post('include')
  includeDismantle(@Body() body: NormalAlertDto, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(includeDismantleList))
      throw new HttpException('Access Forbidden', 403);

    try {
      return this.dismantleService.postIncludeDismantle(body, req.user.id);
    } catch (e) {
      return {
        status: 400,
        message: 'Terjadi kesalahan',
        error: e.message,
      };
    }
  }

  @Post('response/om')
  async responseOm(@Body() body: CreateDismantleResponseDto, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(sendResponseOM))
      throw new HttpException('Access Forbidden', 403);

    try {
      const resp = await this.dismantleResponseService.postResponseOMRequest(
        body,
        req.user.id,
      );
      if (!resp) throw new HttpException('An Error occured', 409);
      const sender = await this.userService.findById(req.user.id);

      const mailData = {
        data: [],
      };

      for (const item of body.data) {
        const dis = await this.dismantleService.findOne(item.request_id);
        const dismantle = await this.dismantleService.findByUuid(
          dis.data.uuid,
          dis.data.kategori,
        );
        let users = await this.userService.getUserByRole('SPM');
        users = users.filter((val) => val.id != req.user.id);
        const payload = {
          users: users,
          title: 'Dismantle Request',
          message: `Telah dilakukan request dismantle oleh : ${dismantle.request_user.nama}`,
          data: dismantle,
        };

        await this.dismantleQueue.add('redis', {
          payload: payload,
        });

        for (const user of users) {
          mailData.data.push({
            to: user.email,
            from: process.env.MAIL_USER,
            subject: 'Notif Dismantle ' + dismantle.alert_code,
            html:
              'Kepada Yth <br/>' +
              'User ' +
              dismantle.request_user.nama +
              ' telah melakukan dismantle request dengan detail sebagai berikut : <br/><br/>' +
              '<table>' +
              '<tr><td>Alert Code</td>' +
              dismantle.alert_code +
              '<td></tr>' +
              '<tr><td>Alert Time</td>' +
              dismantle.alert_time +
              '<td></tr>' +
              '<tr><td>Request User</td>' +
              dismantle.request_user.nama +
              '<td></tr>' +
              '<tr><td>Catatan</td>' +
              dismantle.catatan +
              '<td></tr>' +
              '<tr><td>Response</td><td>' +
              dismantleResponseOM[body.response_int + 1] +
              '</td></tr>' +
              '<tr><td>Catatan Response Dismantle</td>' +
              body.catatan +
              '<td></tr>' +
              '</table>',
          });
          await this.dismantleQueue.add('firebase', {
            sender: req.user.id,
            receiver: user.id,
            token: user.firebase_id,
            title: 'Dismantle Response OM',
            body: `Telah dilakukan response dismantle oleh : ${sender.nama}`,
          });
        }
      }
      await this.dismantleQueue.add('email', {
        mail: mailData,
      });
      return resp;
    } catch (e) {
      return {
        status: 400,
        message: 'Terjadi kesalahan',
        error: e.message,
      };
    }
  }

  @Post('response/spm')
  async responseSPM(@Body() body: CreateDismantleResponseDto, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(sendResponseSPM))
      throw new HttpException('Access Forbidden', 403);

    try {
      const resp = await this.dismantleResponseService.postResponseSPMRequest(
        body,
        req.user.id,
      );
      if (!resp) throw new HttpException('An Error occured', 409);

      const sender = await this.userService.findById(req.user.id);

      const mailData = {
        data: [],
      };

      for (const item of body.data) {
        const dis = await this.dismantleService.findOne(item.request_id);
        const dismantle = await this.dismantleService.findByUuid(
          dis.data.uuid,
          dis.data.kategori,
        );
        let users = await this.userService.getUserByRole('OM');
        users = users.filter((val) => val.id != req.user.id);
        const payload = {
          users: users,
          title: 'Dismantle Request',
          message: `Telah dilakukan request dismantle oleh : ${dismantle.request_user.nama}`,
          data: dismantle,
        };

        await this.dismantleQueue.add('redis', {
          payload: payload,
        });
        for (const user of users) {
          mailData.data.push({
            to: user.email,
            from: process.env.MAIL_USER,
            subject: 'Notif Dismantle ' + dismantle.alert_code,
            html:
              'Kepada Yth <br/>' +
              'User SPM' +
              dismantle.request_user.nama +
              ' telah melakukan Response dismantle request dengan detail sebagai berikut : <br/><br/>' +
              '<table>' +
              '<tr><td>Alert Code</td>' +
              dismantle.alert_code +
              '<td></tr>' +
              '<tr><td>Alert Time</td>' +
              dismantle.alert_time +
              '<td></tr>' +
              '<tr><td>Request User</td>' +
              dismantle.request_user.nama +
              '<td></tr>' +
              '<tr><td>Catatan Dismantle</td>' +
              dismantle.catatan +
              '<td></tr>' +
              '<tr><td>Response</td><td>' +
              dismantleResponseSPM[body.response_int + 1] +
              '</td></tr>' +
              '<tr><td>Catatan Response Dismantle</td>' +
              body.catatan +
              '<td></tr>' +
              '</table>',
          });
          await this.dismantleQueue.add('firebase', {
            sender: req.user.id,
            receiver: user.id,
            token: user.firebase_id,
            title: 'Dismantle Response SPM',
            body: `Telah dilakukan response dismantle oleh : ${sender.nama}`,
          });
        }
      }
      await this.dismantleQueue.add('email', {
        mail: mailData,
      });
      return resp;
    } catch (e) {
      return {
        status: 400,
        message: 'Terjadi kesalahan',
        error: e.message,
      };
    }
  }

  @Post('request')
  async create(@Body() body: CreateDismantleDto, @Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(sendDismantleRequest))
      throw new HttpException('Access Forbidden', 403);

    try {
      const resp = await this.dismantleService.postDismantleRequest(
        body,
        req.user.id,
      );
      if (!resp) throw new HttpException('An Error occured', 409);
      const mailData = {
        data: [],
      };

      for (const item of body.data) {
        const dismantle = await this.dismantleService.findByUuid(
          item.uuid,
          item.kategori,
        );
        let users = await this.userService.getUserByRole('OM');
        users = users.filter((val) => val.id != req.user.id);

        const payload = {
          users: users,
          title: 'Dismantle Request',
          message: `Telah dilakukan request dismantle oleh : ${dismantle.request_user.nama}`,
          data: dismantle,
        };

        await this.dismantleQueue.add('redis', {
          payload: payload,
        });

        for (const user of users) {
          mailData.data.push({
            to: user.email,
            from: process.env.MAIL_USER,
            subject: 'Notif Dismantle ' + dismantle.alert_code,
            html:
              'Kepada Yth <br/>' +
              'User ' +
              dismantle.request_user.nama +
              ' telah melakukan dismantle request dengan detail sebagai berikut : <br/><br/>' +
              '<table>' +
              '<tr><td>Alert Code</td>' +
              dismantle.alert_code +
              '<td></tr>' +
              '<tr><td>Alert Time</td>' +
              dismantle.alert_time +
              '<td></tr>' +
              '<tr><td>Request User</td>' +
              dismantle.request_user.nama +
              '<td></tr>' +
              '<tr><td>Catatan</td>' +
              dismantle.catatan +
              '<td></tr>' +
              '</table>',
          });
          this.dismantleQueue.add('firebase', {
            sender: req.user.id,
            receiver: user.id,
            token: user.firebase_id,
            title: 'Dismantle Request',
            body: `Telah dilakukan request dismantle oleh : ${dismantle.request_user.nama}`,
          });
        }
      }
      this.dismantleQueue.add('email', {
        mail: mailData,
      });
      return resp;
    } catch (e) {
      return {
        status: 400,
        message: 'Terjadi kesalahan',
        error: e.message,
      };
    }
  }

  @Get(':id')
  findById(@Req() req: any, @Param() id: number) {
    const perms = req.body.permission;
    if (!perms.includes(getAllDismantleRequest))
      throw new HttpException('Access Forbidden', 403);

    return this.dismantleService.findOne(id);
  }

  @Get('response/:id')
  findResponseById(@Req() req: any, @Param() id: number) {
    const perms = req.body.permission;
    if (!perms.includes(getAllDismantleRequest))
      throw new HttpException('Access Forbidden', 403);

    return this.dismantleResponseService.getDataMessage(id);
  }

  @Get()
  async getAll(@Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(getAllDismantleRequest))
      throw new HttpException('Access Forbidden', 403);
    const user = await this.userService.findById(req.user.id);
    if (!user.organization)
      throw new HttpException('User organisasi tidak ditemukan', 409);
    const regional = await this.organizationService.findRegional(
      user.organization.id,
    );

    req.user.regional = regional;

    return this.dismantleService.findAll(req);
  }

  @Get('summary')
  async getSummary(@Req() req: any) {
    const perms = req.body.permission;
    if (!perms.includes(getAllDismantleRequest))
      throw new HttpException('Access Forbidden', 403);
    const user = await this.userService.findById(req.user.id);
    if (!user.organization)
      throw new HttpException('User organisasi tidak ditemukan', 409);
    const regional = await this.organizationService.findRegional(
      user.organization.id,
    );

    req.user.regional = regional;

    let resp = await this.dismantleService.findAll(req);
    return {
      total: resp.data.length,
      backbone: resp.data.filter((x) => x.kategori.toLowerCase() == 'backbone')
        .length,
      metro: resp.data.filter((x) => x.kategori.toLowerCase() == 'metro'),
      gpon: resp.data.filter((x) => x.kategori.toLowerCase() == 'gpon'),
      service_node: resp.data.filter(
        (x) => x.kategori.toLowerCase() == 'service_node',
      ),
      blackout: resp.data.filter((x) => x.kategori.toLowerCase() == 'blackout'),
    };
  }
}

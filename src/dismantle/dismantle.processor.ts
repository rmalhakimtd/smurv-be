import { Process, Processor } from '@nestjs/bull';
import { Inject } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Job } from 'bull';
import { MailService } from 'src/mail/mail.service';
import { PushNotificationService } from 'src/push-notification/push-notification.service';

@Processor('notification')
export class DismantleProcessor {
  constructor(
    private readonly mailService: MailService,
    private readonly pushNotifService: PushNotificationService,
    @Inject('NOTIFICATION_SERVICE') private client: ClientProxy,
  ) {}

  @Process('email')
  async sendingEmail(job: Job) {
    const mailData = job.data.mail;
    return this.mailService.sendNotifEmail(mailData);
  }

  @Process('redis')
  sendToRedis(job: Job) {
    const payload = job.data.payload;
    // Send data into Redis
    this.client.emit('dismantle-request', payload);
  }

  @Process('firebase')
  async sendingNotifFirebase(job: Job) {
    const token = job.data.token;
    const title = job.data.title;
    const body = job.data.body;
    const sender = job.data.sender;
    const receiver = job.data.receiver;

    return this.pushNotifService.send({
      sender: sender,
      receiver: receiver,
      token: token,
      title: title,
      body: body,
    });
  }

  @Process('message')
  async sendingMessage(job: Job) {
    const dismantle = job.data.dismantle;
    const type = job.data.type as string;
  }
}

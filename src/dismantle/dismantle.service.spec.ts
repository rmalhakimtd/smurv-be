import { Test, TestingModule } from '@nestjs/testing';
import { DismantleService } from './dismantle.service';

describe('DismantleService', () => {
  let service: DismantleService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DismantleService],
    }).compile();

    service = module.get<DismantleService>(DismantleService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

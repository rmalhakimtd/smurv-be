import { User } from 'src/user/entities/user.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { DismantleResponse } from './dismantle-response.entity';

@Entity('v_dismantle_data')
export class DismantleData {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  uuid: string;

  @Column({ nullable: true })
  alert_code: string;

  @Column({ nullable: true })
  regional: string;

  @Column({ nullable: true })
  kategori: string;

  @Column({ nullable: true })
  tipe: string;

  @Column()
  alert_time: string;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'sender_id' })
  request_user: User;

  @Column()
  catatan: string;

  @Column()
  parent_id: number;

  @Column()
  status: number;

  @Column()
  status_str: string;

  @Column()
  is_exclude: number;

  @Column()
  is_dismantle: number;

  @OneToMany(() => DismantleResponse, (dismantle) => dismantle.data)
  @JoinColumn({ name: 'id' })
  response: DismantleResponse[];

  @Column()
  createdAt: string;
}

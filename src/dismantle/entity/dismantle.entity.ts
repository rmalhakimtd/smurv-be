import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('t_dismantle_request')
export class DismantleRequest {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  uuid: string;

  @Column()
  kategori: string;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'sender_id', referencedColumnName: 'id' })
  sender: User;

  sender_id: number;

  @Column({ default: 0 })
  status: number;

  @Column()
  catatan: string;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn({ onUpdate: 'CURRENT_TIMESTAMP(6)' })
  updatedAt: Date;

  @ManyToOne(() => DismantleRequest, (dismantle) => dismantle.id)
  @JoinColumn({ name: 'parent_id' })
  parent: DismantleRequest;

  @Column()
  is_exclude: number;

  @Column()
  included_at: Date;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'included_by' })
  included_user: User;

  @Column()
  is_dismantle: number;
}

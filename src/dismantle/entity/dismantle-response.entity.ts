import { User } from 'src/user/entities/user.entity';
import {
  Column,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { DismantleData } from './dimantle-data.entity';
import { DismantleRequest } from './dismantle.entity';

@Entity('t_dismantle_response')
export class DismantleResponse {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => DismantleRequest, (dismantle) => dismantle.id)
  @JoinColumn({ name: 'request_id', referencedColumnName: 'id' })
  request: DismantleRequest;

  @ManyToOne(() => DismantleData, (dismantle) => dismantle.response)
  @JoinColumn({ name: 'request_id' })
  data: DismantleData;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;

  @Column()
  response: string;

  @Column()
  response_int: number;

  @Column()
  catatan: string;

  @Column()
  type: number;
}

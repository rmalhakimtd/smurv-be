import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsArray, IsNumber, IsOptional, IsString } from 'class-validator';

export class DismantleRequestDto {
  uuid: string;
  kategori: string;
}

export class CreateDismantleDto {
  @ApiProperty({ required: true })
  @IsArray()
  data: DismantleRequestDto[];

  @ApiProperty({ required: true })
  @IsString()
  catatan: string;
}

export class DismanteIdDto {
  @ApiProperty({ required: true })
  @IsNumber()
  id: number;
}

export class NormalAlertDto {
  @ApiProperty({ required: true })
  @IsArray()
  data: DismanteIdDto[];
}

export class DismantleResponseDto {
  @ApiProperty({ required: true })
  @IsOptional()
  request_id: any;
}

export class CreateDismantleResponseDto {
  @ApiProperty({ required: true })
  @IsArray()
  data: DismantleResponseDto[];

  @ApiProperty({ required: true })
  @IsString()
  response: string;

  @ApiProperty({ required: true })
  @IsNumber()
  response_int: number;

  @ApiProperty({ required: true })
  @IsString()
  catatan: string;
}

export class CreateDismantleListDto {
  @ApiProperty({ required: true })
  @IsArray()
  data: DismantleRequestDto[];
}

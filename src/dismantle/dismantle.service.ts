import { HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GPONAlert } from 'src/alarm/entities/gpon.entity';
import { TrunkAlert } from 'src/alarm/entities/trunk.entity';
import { PageService } from 'src/helper/service/page/page.service';
import { User } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';
import {
  CreateDismantleDto,
  CreateDismantleListDto,
  NormalAlertDto,
} from './dto/dismantle.dto';
import { DismantleRequest } from './entity/dismantle.entity';
import * as moment from 'moment';
import { BboneAlert } from 'src/alarm/entities/bbone.entity';
import { MetroAlert } from 'src/alarm/entities/metro.entity';
import { DatinData } from 'src/alarm/entities/datin.entity';
import { DismantleData } from './entity/dimantle-data.entity';
import { VoiceData } from 'src/alarm/entities/voice.entity';
import { BlackoutAlert } from 'src/dashboard/entities/blackout.entity';

@Injectable()
export class DismantleService extends PageService {
  constructor(
    @InjectRepository(TrunkAlert) private trunkRepo: Repository<TrunkAlert>,
    @InjectRepository(User) private userRepo: Repository<User>,
    @InjectRepository(DismantleRequest)
    private dismantleRepo: Repository<DismantleRequest>,
    @InjectRepository(GPONAlert) private gponAlertRepo: Repository<GPONAlert>,
    @InjectRepository(BboneAlert) private bboneRepo: Repository<BboneAlert>,
    @InjectRepository(MetroAlert) private metroRepo: Repository<MetroAlert>,
    @InjectRepository(DatinData) private datinRepo: Repository<DatinData>,
    @InjectRepository(VoiceData) private voiceRepo: Repository<VoiceData>,
    @InjectRepository(BlackoutAlert) private blackOutRepo: Repository<BlackoutAlert>,
    @InjectRepository(DismantleData)
    private dismantleData: Repository<DismantleData>,
  ) {
    super();
  }

  async postIncludeDismantle(request: NormalAlertDto, userId: number) {
    try {
      for (const item of request.data) {
        const data = await this.dismantleRepo.findOne({
          where: {
            id: item.id,
            is_exclude: 1,
            is_dismantle: 1,
          },
        });
        if (!data) throw new HttpException('Message not found', 404);

        const userModel = await this.userRepo.findOne(userId);
        if (!userModel) throw new HttpException('access forbiden', 403);

        data.included_at = moment().toDate();
        data.included_user = userModel;
        data.is_exclude = 0;

        const dismantle = await this.dismantleRepo.save(data);
      }
      return this.responseAPI('success', []);
    } catch (e) {
      throw new HttpException(e.message, 400);
    }
  }

  async postNormalAlert(request: CreateDismantleListDto, user: number) {
    try {
      for (const item of request.data) {
        // Save into dismantle request
        const userModel = await this.userRepo.findOne(user);
        // Update masing-masing model
        let models = null;
        switch (item.kategori.toLocaleUpperCase()) {
          case 'GPON':
            models = this.gponAlertRepo;
            break;
          case 'METRO':
            models = this.metroRepo;
            break;
          case 'BBONE':
            models = this.bboneRepo;
            break;
          case 'DATIN':
            models = this.datinRepo;
            break;
          case 'VOICE':
            models = this.voiceRepo;
            break;
          case 'BLACKOUT':
            models = this.blackOutRepo;
          default:
            new HttpException('Kategori tidak valid', 400);
            break;
        }
        // eslint-disable-next-line prefer-const
        let data = await models.findOne({
          where: {
            uuid: item.uuid,
          },
        });
        if (!data) {
          throw new HttpException('Alarm not found', 409);
        }

        data.validated_by = null;
        data.is_valid = null;
        data.validated_time = null;
        data.status = 'Acknowledge';

        // console.log(item);

        const dismantle = await this.dismantleRepo.findOne({
          where: {
            uuid: item.uuid,
            kategori: item.kategori,
          },
        });
        if (!dismantle)
          throw new HttpException('Alarm belum di dismantle', 409);

        dismantle.status = 4;
        await this.dismantleRepo.save(dismantle);

        await models.save(data);
      }
      return {
        status: 200,
        message: 'Berhasil Menambahkan Data Dismantle',
        result: [],
      };
    } catch (e) {
      throw new HttpException('Terjadi kesalahan server : ' + e.message, 400);
    }
  }

  async postDismantleRequest(request: CreateDismantleDto, user: number) {
    try {
      // const dataDismantleRequest = [];
      for (const item of request.data) {
        const cek = await this.dismantleRepo.findOne({
          where: {
            uuid: item.uuid,
            kategori: item.kategori,
            is_dismantle: 1,
          },
        });
        if (cek) {
          continue;
        }
        // Save into dismantle request
        const userModel = await this.userRepo.findOne(user);
        // Update masing-masing model
        let models = null;
        switch (item.kategori.toLocaleUpperCase()) {
          case 'GPON':
            models = this.gponAlertRepo;
            break;
          case 'METRO':
            models = this.metroRepo;
            break;
          case 'BBONE':
            models = this.bboneRepo;
            break;
          case 'DATIN':
            models = this.datinRepo;
            break;
          case 'VOICE':
            models = this.voiceRepo;
            break;
          case 'BLACKOUT':
            models = this.blackOutRepo;
          default:
            new HttpException('Kategori tidak valid', 400);
            break;
        }
        // eslint-disable-next-line prefer-const
        let data = await models.findOne({
          where: {
            uuid: item.uuid,
            status: 'Not Valid',
          },
        });
        if (!data) {
          throw new HttpException('Alarm not found', 409);
        }

        const dismantle = await this.dismantleRepo.save({
          uuid: item.uuid,
          kategori: item.kategori,
          sender_id: user,
          sender: userModel,
          catatan: request.catatan,
        });
        data.status = 'Dismantle Request';

        await models.save(data);
        // dataDismantleRequest.push(JSON.stringify(dismantle));
      }
      return {
        status: 200,
        message: 'Berhasil Menambahkan Data Dismantle',
        result: [],
      };
    } catch (e) {
      throw new HttpException('Terjadi kesalahan server : ' + e.message, 400);
    }
  }

  async findAll(req: any) {
    const user = await this.userRepo.findOne(req.user.id, {
      relations: ['roles'],
    });
    const roles = [];
    user.roles.map((val) => roles.push(val.role_name));

    // eslint-disable-next-line prefer-const
    let models = this.dismantleData.createQueryBuilder('v_dismantle_data');

    models.leftJoinAndSelect('v_dismantle_data.response', 'response');
    models.leftJoinAndSelect('response.user', 'user');
    models.leftJoinAndSelect('v_dismantle_data.request_user', 'request_user');

    models.andWhere('regional IN (:...regional)', {
      regional: req.user.regional,
    });

    const search = req.query.search as string;
    if (search && search != '') {
      models.andWhere(
        'v_dismantle_data.alert_code ilike :s or kategori ilike :s or status ilike :s ',
        { s: `%${search}%` },
      );
    }

    const sort: any = req.query.sortBy;
    const sortDirection: any = req.query.sortDirection;

    if (sort && sortDirection) {
      models.orderBy(`v_dismantle_data.${sort}`, sortDirection.toUpperCase());
    } else {
      // models.orderBy('v_dismantle_data.alert_time', 'ASC');
    }

    const uuid: string = req.query.uuid as string;
    if (uuid && uuid != '') {
      models.andWhere('v_dismantle_data.uuid = :uuid', { uuid: uuid });
    }

    let start_date = req.query.start_date as string;
    if (start_date) {
      start_date = moment(start_date.split(' ')[0]).format(
        'YYYY-MM-DD HH:mm:ss',
      );

      models.andWhere('v_dismantle_data."createdAt" >= :start_date', {
        start_date: `${start_date}`,
      });
    }

    let end_date = req.query.end_date as string;
    if (end_date) {
      end_date = moment(end_date.split(' ')[0]).format('YYYY-MM-DD HH:mm:ss');
      models.andWhere('v_dismantle_data."createdAt" <= :end_date', {
        end_date: `${end_date}`,
      });
    }

    const kategori: string = req.query.kategori as string;
    if (kategori && kategori != '') {
      models.andWhere('kategori ilike :q2', { q2: kategori });
    }

    const status: string = req.query.status as string;
    if (status && status != '') {
      models.andWhere('status = :q3', { q3: `${status}` });
    } else {
      const roleCheck = roles.find((element) => {
        if (element.includes('OM') || element.includes('Superadmin')) {
          // console.log('here OM');
          models.andWhere('v_dismantle_data.status IN (0,1,2)');
          return 'OM';
        } else if (element.includes('SPM')) {
          // console.log('here SPM');
          models.andWhere('v_dismantle_data.status IN (1,2)');
          return 'SPM';
        } else {
          // console.log('other ' + element);
          return 'Other';
        }
      });
    }

    const area: string = req.query.area as string;
    if (area && area != '') {
      models.andWhere('regional ilike :q4', { q4: `'${area}'` });
    }

    const tipe_alert: string = req.query.tipe_alert as string;
    if (tipe_alert && tipe_alert != '') {
      models.andWhere('tipe ilike :q5', { q5: tipe_alert });
    }

    // const page: number = parseInt(req.query.page as any) || 1;
    // const perpage: number = parseInt(req.query.perpage as any) || 20;
    // const total = await models.getCount();

    // models.offset((page - 1) * perpage).limit(perpage);

    // console.log(models.getQuery());

    return {
      data: await models.getMany(),
    };
  }

  async findOne(id: number) {
    const data = await this.dismantleRepo.findOne(id);
    if (!data) throw new HttpException('Data Dismantle not found', 404);
    return this.responseAPI('show detail dismantle', data);
  }

  async findByUuid(uuid: string, kategori: string) {
    const data = await this.dismantleData.findOne({
      relations: ['request_user'],
      where: {
        uuid: uuid,
        kategori: kategori.toLocaleUpperCase(),
      },
    });
    if (!data) throw new HttpException('Data Dismantle not found', 404);
    return data;
  }

  async queryBuilder(alias: string) {
    return this.dismantleRepo.createQueryBuilder(alias);
  }
}

import { Module } from '@nestjs/common';
import { DismantleService } from './dismantle.service';
import { DismantleController } from './dismantle.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GPONAlert } from 'src/alarm/entities/gpon.entity';
import { TrunkAlert } from 'src/alarm/entities/trunk.entity';
import { User } from 'src/user/entities/user.entity';
import { DismantleRequest } from './entity/dismantle.entity';
import { DismantleResponseService } from 'src/dismantle-response/dismantle-response.service';
import { DismantleResponseController } from 'src/dismantle-response/dismantle-response.controller';
import { DismantleResponse } from './entity/dismantle-response.entity';
import { AlarmSummaryService } from 'src/alarm/alarm.service';
import { AlarmSummary } from 'src/alarm/entities/alarm.entity';
import { DismantleData } from './entity/dimantle-data.entity';
import { ExcludeEntity } from 'src/excludes/entities/exclude.entity';
import { DatinData } from 'src/alarm/entities/datin.entity';
import { MetroAlert } from 'src/alarm/entities/metro.entity';
import { BboneAlert } from 'src/alarm/entities/bbone.entity';
import { MailService } from 'src/mail/mail.service';
import { UserService } from 'src/user/user.service';
import { Role } from 'src/role/entities/role.entity';
import { Organization } from 'src/organization/entity/organization.entity';
import { OrganizationService } from 'src/organization/organization.service';
import { Regional } from 'src/alarm/entities/regional.entity';
import { VoiceData } from 'src/alarm/entities/voice.entity';
import { BullModule } from '@nestjs/bull';
import { DismantleProcessor } from './dismantle.processor';
import { PushNotificationService } from 'src/push-notification/push-notification.service';
import { Notification } from 'src/push-notification/entity/notification.entity';
import { ConfigModule } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { BlackoutAlert } from 'src/dashboard/entities/blackout.entity';
import { ValidatedResponse } from 'src/alarm/entities/validated-response.entity';

@Module({
  imports: [
    ConfigModule.forRoot(),
    ClientsModule.register([
      {
        name: 'NOTIFICATION_SERVICE',
        transport: Transport.REDIS,
        options: {
          url: process.env.REDIS_CONNECTION,
        },
      },
    ]),

    TypeOrmModule.forFeature([
      GPONAlert,
      TrunkAlert,
      User,
      Role,
      Organization,
      DismantleRequest,
      DismantleResponse,
      AlarmSummary,
      DismantleData,
      ExcludeEntity,
      DatinData,
      MetroAlert,
      BboneAlert,
      VoiceData,
      BlackoutAlert,
      ValidatedResponse,
      Regional,
      Notification,
    ]),
    BullModule.registerQueue({
      name: 'notification',
    }),
  ],
  providers: [
    DismantleService,
    DismantleResponseService,
    AlarmSummaryService,
    MailService,
    UserService,
    OrganizationService,
    DismantleProcessor,
    PushNotificationService,
  ],
  controllers: [DismantleController, DismantleResponseController],
  exports: [DismantleProcessor],
})
export class DismantleModule {}

import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'o_cpu_nat_hsi' })
export class CpuNatHSI {
  @PrimaryColumn()
  id: number;

  @Column()
  hostname: string;

  @Column()
  interface: string;

  @Column()
  session_count: string;

  @Column()
  memory: number;

  @Column()
  cpu: number;

  @Column()
  status: string;

  @Column()
  kategori: string;

  @Column()
  last_update: Date;
}

@Entity({ name: 'v_cpu_nat_summary' })
export class CpuNatSummary {
  @PrimaryColumn()
  hostname: string;

  @Column()
  ipadd: number;

  @Column()
  outlier: number;

  @Column()
  normal: number;

  total: number;
}

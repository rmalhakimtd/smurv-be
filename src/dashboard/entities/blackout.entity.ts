import { User } from 'src/user/entities/user.entity';
import { Column, Entity, Generated, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';

@Entity({ name: 'o_blackout_data' })
export class BlackoutAlert {
  @PrimaryColumn()
  id: number;

  @Column()
  @Generated('uuid')
  uuid: string;

  @Column()
  ipadd: string;

  @Column()
  device: string;

  @Column()
  tm_ehealth: Date;

  @Column()
  reachability: number;

  @Column()
  status_ping: string;

  @Column()
  cek_ping: string;

  @Column()
  tanggal: string;

  @Column()
  updated_at: Date;

  @Column({ nullable: true })
  is_valid: boolean;

  @Column({ nullable: true })
  acknowledge_by: number;

  @Column({ type: 'timestamp', nullable: true })
  acknowledge_time: Date;

  @Column({ nullable: true })
  acknowledge_status: number;

  @Column({ nullable: true })
  status: string;

  @Column({ nullable: true })
  is_closed: boolean;

  @Column()
  tiket_nossa: string;

  @Column()
  validated_time: Date;

  @ManyToOne(() => User, (user) => user.acknowledges)
  @JoinColumn({ name: 'validated_by' })
  validated_user: User;

  @Column({ nullable: true })
  validated_by: number;

  @Column()
  ping_detail: string;
}

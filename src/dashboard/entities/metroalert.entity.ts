import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'v_metro_summary' })
export class MetroAlarm {
  @PrimaryColumn()
  regional: string;

  @Column()
  down_new: number;

  @Column()
  down_ack: number;

  @Column()
  utilize_new: number;

  @Column()
  utilize_ack: number;

  @Column()
  degrade_new: number;

  @Column()
  degrade_ack: number;

  @Column()
  healthy_new: number;

  @Column()
  healthy_ack: number;
}

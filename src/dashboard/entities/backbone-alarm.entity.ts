import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'v_backbone_summary' })
export class BackboneAlarm {
  @PrimaryColumn()
  regional: string;

  @Column()
  down_new: number;

  @Column()
  down_ack: number;

  @Column()
  utilize_new: number;

  @Column()
  utilize_ack: number;

  @Column()
  degrade_new: number;

  @Column()
  degrade_ack: number;

  @Column()
  healthy_new: number;

  @Column()
  healthy_ack: number;
}

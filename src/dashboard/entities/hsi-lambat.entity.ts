import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'v_hsi_lambat' })
export class HsiLambatEntity {
  @PrimaryColumn()
  regional: string;

  @Column()
  h1: number;

  @Column()
  h2: number;

  @Column()
  h7: number;
}

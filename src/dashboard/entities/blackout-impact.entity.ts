import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'v_impact_summary' })
export class BlackoutImpactSummary {
  @PrimaryColumn()
  segmen: string;

  @Column()
  jumlah: number;
}

@Entity({ name: 'blackout_impact' })
export class BlackoutImpactEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  uuid: string;

  @Column()
  segmen: string;

  @Column()
  reachability: string;

  @Column()
  jumlah: number;

  @Column()
  tm_dt_ehealth: string;

  @Column()
  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @Column()
  @UpdateDateColumn({ name: 'updated_at', onUpdate: 'CURRENT_TIMESTAMP(6)' })
  updatedAt: Date;
}

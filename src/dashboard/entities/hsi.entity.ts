import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'hsi_tiket' })
export class HsiEntity {
  @PrimaryColumn()
  id: number;

  @Column()
  uuid: string;

  @Column()
  Incident: string;

  @Column()
  Summary: string;

  @Column()
  Source: string;

  @Column()
  Segment: string;

  @Column()
  Channel: string;

  @Column()
  Customer_Segment: string;

  @Column()
  Service_ID: string;

  @Column()
  Service_No: string;

  @Column()
  Service_Type: string;

  @Column()
  Reported_Date: Date;

  @Column()
  Status: string;

  @Column()
  Workzone: string;

  @Column()
  Witel: string;

  @Column()
  Regional: string;
}

@Entity({ name: 'v_hsi_witel' })
export class HsiWitelEntity {
  @PrimaryColumn()
  Witel: string;

  @Column()
  jumlah: number;

  @Column()
  h1: number;

  @Column()
  h7: number;
}

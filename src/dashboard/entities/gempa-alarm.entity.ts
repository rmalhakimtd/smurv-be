import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 't_gempa_bumi' })
export class GempaBumi {
  @PrimaryColumn()
  id: number;

  @Column()
  tanggal: string;

  @Column()
  jam: string;

  @Column()
  date_time: Date;

  @Column()
  coordinates: string;

  @Column()
  lintang: string;

  @Column()
  bujur: string;

  @Column()
  magnitude: string;

  @Column()
  kedalaman: string;

  @Column()
  wilayah: string;

  @Column()
  potensi: string;
}

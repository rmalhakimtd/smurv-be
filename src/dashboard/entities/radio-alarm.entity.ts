import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'v_radio_summary' })
export class RadioSummaryEntity {
  @PrimaryColumn()
  regional: string;

  @Column()
  down_new: number;

  @Column()
  down_ack: number;
}

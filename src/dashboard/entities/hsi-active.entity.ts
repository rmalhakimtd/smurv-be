import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'v_hsi_high_active' })
export class HsiHighActive {
  @PrimaryColumn()
  Witel: string;

  @Column()
  jumlah: number;
}

@Entity({ name: 'v_hsi_jumlah_tiket' })
export class HsiJumlahTiketView {
  @PrimaryColumn("varchar")
  Regional: string;

  @PrimaryColumn("varchar")
  Witel: string;

  @PrimaryColumn("varchar")
  Workzone: string;

  @PrimaryColumn("timestamp")
  Reported_Date: string;

  @Column()
  jumlah: number;
}

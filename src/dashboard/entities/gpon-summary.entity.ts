import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'v_gpon_summary' })
export class GponSummaryEntity {
  @PrimaryColumn()
  regional: string;

  @Column()
  down_new: number;

  @Column()
  down_ack: number;
}

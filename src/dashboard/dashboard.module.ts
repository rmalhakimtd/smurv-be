import { Module } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { DashboardController } from './dashboard.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AlarmSummary } from 'src/alarm/entities/alarm.entity';
import { TrunkAlert } from 'src/alarm/entities/trunk.entity';
import { GPONAlert } from 'src/alarm/entities/gpon.entity';
import { BackboneAlarm } from './entities/backbone-alarm.entity';
import { HsiHighActive, HsiJumlahTiketView } from './entities/hsi-active.entity';
import { HsiLambatEntity } from './entities/hsi-lambat.entity';
import { HsiEntity, HsiWitelEntity } from './entities/hsi.entity';
import { MetroAlarm } from './entities/metroalert.entity';
import { ServiceNodeAlarm } from './entities/servidenode-alarm.entity';
import { GponSummaryEntity } from './entities/gpon-summary.entity';
import { CpuNatHSI, CpuNatSummary } from './entities/cpunat-hsi.entity';
import { GempaBumi } from './entities/gempa-alarm.entity';
import { BlackoutAlert } from './entities/blackout.entity';
import { HttpModule } from '@nestjs/axios';
import { RadioSummaryEntity } from './entities/radio-alarm.entity';
import {
  BlackoutImpactEntity,
  BlackoutImpactSummary,
} from './entities/blackout-impact.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      AlarmSummary,
      TrunkAlert,
      GPONAlert,
      BackboneAlarm,
      HsiHighActive,
      HsiLambatEntity,
      HsiEntity,
      HsiWitelEntity,
      MetroAlarm,
      ServiceNodeAlarm,
      GponSummaryEntity,
      CpuNatHSI,
      GempaBumi,
      BlackoutAlert,
      RadioSummaryEntity,
      BlackoutImpactSummary,
      BlackoutImpactEntity,
      CpuNatSummary,
      HsiJumlahTiketView,
    ]),
    HttpModule,
  ],
  controllers: [DashboardController],
  providers: [DashboardService],
})
export class DashboardModule {}

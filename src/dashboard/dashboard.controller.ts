import { Controller, Get, Param, Req } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import * as convert from 'xml-js';
import { HttpService } from '@nestjs/axios';

@Controller('dashboard')
export class DashboardController {
  constructor(
    private http: HttpService,
    private readonly dashboardService: DashboardService,
  ) {}

  @Get('alarm-summary/:kategori')
  getChartAlarmSummary(@Req() req: any, @Param() params: any) {
    const { kategori } = params;
    return this.dashboardService.getAlarmSummary(kategori);
  }

  @Get('backbone-alert')
  getBackboneAlert(@Req() req: any) {
    return this.dashboardService.getBackboneAlert();
  }

  @Get('metro-alert')
  getMetroAlert(@Req() req: any) {
    return this.dashboardService.getMetroAlert();
  }

  @Get('service-alert')
  getServiceNodeAlert(@Req() req: any) {
    return this.dashboardService.getServiceNodeAlert();
  }

  @Get('gpon-alert')
  getGponSummary(@Req() req: any) {
    return this.dashboardService.getGponSummary();
  }

  @Get('cpu-nat-hsi')
  getCpuNatHSI(@Req() req: any) {
    return this.dashboardService.getCpuNatHSI();
  }

  @Get('cpu-nat-detail')
  getCpuNatHSIDetail(@Req() req: any) {
    return this.dashboardService.getCpuNatHSIDetail(req);
  }

  @Get('active-hsi')
  getActiveHsi(@Req() req: any) {
    return this.dashboardService.getActiveHsi();
  }

  @Get('active-hsi-all')
  getActiveHsiAll(@Req() req: any) {
    return this.dashboardService.getActiveHsiAll();
  }

  @Get('active-hsi-detail')
  getActiveHsiDetail(@Req() req: any) {
    return this.dashboardService.getActiveHsiDetail(req);
  }

  @Get('hsi-lambat')
  getHsiLambat(@Req() req: any) {
    return this.dashboardService.getHsiLambat();
  }

  @Get('hsi-witel')
  getHSIWitel(@Req() req: any) {
    return this.dashboardService.getHSILambatWitel();
  }

  @Get('hsi-detail')
  getHSIDetail(@Req() req: any) {
    // return req.query.regional;
    return this.dashboardService.getHSIDetail(req);
  }

  @Get('gempa-alert')
  getGempaAlert(@Req() req: any) {
    return this.dashboardService.getGempaBumi();
  }

  @Get('blackout-alert')
  getAlertBlackout(@Req() req: any) {
    return this.dashboardService.getBlackoutAlert();
  }

  @Get('blackout-detail')
  getBlackoutDetail(@Req() req: any) {
    console.log(req.query.hostname);
    const hostname = req.query.hostname as string;
    return this.dashboardService.getBlackoutDetail(hostname);
  }

  @Get('banjir-alert')
  async getBanjirAlert(@Req() req: any) {
    const response = await this.http
      .get('http://poskobanjirdsda.jakarta.go.id/xmldata.xml')
      .toPromise();
    // const options = { compact: true, ignoreComment: true, spaces: 4 };
    const options = {
      compact: true,
      ignoreComment: true,
      alwaysChildren: true,
    };
    const result = convert.xml2json(response.data, options);
    const json = JSON.parse(result);

    const compare = (a, b) => {
      switch (b.STATUS_SIAGA._text) {
        case 'Status : Siaga 4':
          return 1;
        case 'Status : Siaga 3':
          return 1;
        case 'Status : Siaga 2':
          return 1;
        case 'Status : Siaga 1':
          return 1;
        case 'Status : Normal':
          return -1;
        default:
          return 0;
      }
    };

    json.DocumentElement.SP_GET_LAST_STATUS_PINTU_AIR.sort(compare);

    return JSON.stringify(json);
  }

  @Get('radio-alert')
  getRadioSummary(@Req() req: any) {
    return this.dashboardService.getRadioSummary();
  }
}

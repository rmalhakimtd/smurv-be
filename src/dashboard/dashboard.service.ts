import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AlarmSummary } from 'src/alarm/entities/alarm.entity';
import { ItemData } from 'src/excludes/dto/exclude-response.dto';
import { MoreThan, Not, Repository } from 'typeorm';
import { BackboneAlarm } from './entities/backbone-alarm.entity';
import {
  BlackoutImpactEntity,
  BlackoutImpactSummary,
} from './entities/blackout-impact.entity';
import { BlackoutAlert } from './entities/blackout.entity';
import { CpuNatHSI, CpuNatSummary } from './entities/cpunat-hsi.entity';
import { GempaBumi } from './entities/gempa-alarm.entity';
import { GponSummaryEntity } from './entities/gpon-summary.entity';
import { HsiHighActive, HsiJumlahTiketView } from './entities/hsi-active.entity';
import { HsiLambatEntity } from './entities/hsi-lambat.entity';
import { HsiEntity, HsiWitelEntity } from './entities/hsi.entity';
import { MetroAlarm } from './entities/metroalert.entity';
import { RadioSummaryEntity } from './entities/radio-alarm.entity';
import { ServiceNodeAlarm } from './entities/servidenode-alarm.entity';

@Injectable()
export class DashboardService {
  constructor(
    @InjectRepository(AlarmSummary) private alarmRepo: Repository<AlarmSummary>,
    @InjectRepository(BackboneAlarm)
    private backboneAlarmRepo: Repository<BackboneAlarm>,
    @InjectRepository(HsiHighActive)
    private hsiHighRepo: Repository<HsiHighActive>,

    @InjectRepository(HsiLambatEntity)
    private hsiLambat: Repository<HsiLambatEntity>,

    @InjectRepository(HsiEntity)
    private hsiRepo: Repository<HsiEntity>,

    @InjectRepository(HsiWitelEntity)
    private hsiWitel: Repository<HsiWitelEntity>,

    @InjectRepository(MetroAlarm)
    private metroAlertRepo: Repository<MetroAlarm>,

    @InjectRepository(ServiceNodeAlarm)
    private serviceAlertRepo: Repository<ServiceNodeAlarm>,

    @InjectRepository(GponSummaryEntity)
    private gponSummaryRepo: Repository<GponSummaryEntity>,

    @InjectRepository(CpuNatHSI)
    private cpuNatRepo: Repository<CpuNatHSI>,

    @InjectRepository(GempaBumi) private gempaRepo: Repository<GempaBumi>,
    @InjectRepository(BlackoutAlert)
    private blackoutRepo: Repository<BlackoutAlert>,

    @InjectRepository(RadioSummaryEntity)
    private radioRepo: Repository<RadioSummaryEntity>,

    @InjectRepository(BlackoutImpactSummary)
    private impactRepo: Repository<BlackoutImpactSummary>,

    @InjectRepository(BlackoutImpactEntity)
    private blackoutImpactRepo: Repository<BlackoutImpactEntity>,

    @InjectRepository(CpuNatSummary)
    private natSummaryRepo: Repository<CpuNatSummary>,

    @InjectRepository(HsiJumlahTiketView)
    private jumlahTiketHsi: Repository<HsiJumlahTiketView>,
  ) {}

  async getAlarmSummary(kategori: string) {
    let newAlert = await this.alarmRepo.count({
      where: {
        kategori: kategori.toUpperCase(),
        status: 'NEW',
      },
    });
    let acknowledge = await this.alarmRepo.count({
      where: {
        kategori: kategori.toUpperCase(),
        status: 'Acknowledge',
      },
    });
    return {
      series: [
        {
          name: 'Jumlah',
          data: [newAlert, acknowledge],
        },
      ],
    };
  }

  async getBackboneAlert() {
    let data = await this.backboneAlarmRepo.find();
    let new_total = 0;
    for (let item of data) {
      new_total +=
        Number(item.down_new) +
        Number(item.utilize_new) +
        Number(item.degrade_new) +
        Number(item.healthy_new);
    }
    return {
      total_new: new_total,
      data: data,
    };
  }

  async getMetroAlert() {
    let data = await this.metroAlertRepo.find();
    let new_total = 0;
    for (let item of data) {
      new_total +=
        Number(item.down_new) +
        Number(item.utilize_new) +
        Number(item.degrade_new) +
        Number(item.healthy_new);
    }
    return {
      total_new: new_total,
      data: data,
    };
  }

  async getServiceNodeAlert() {
    let data = await this.serviceAlertRepo.find();
    let new_total = 0;
    for (let item of data) {
      new_total +=
        Number(item.down_new) +
        Number(item.utilize_new) +
        Number(item.degrade_new) +
        Number(item.healthy_new);
    }
    return {
      total_new: new_total,
      data: data,
    };
  }

  async getGponSummary() {
    let data = await this.gponSummaryRepo.find();
    let new_total = 0;
    for (let item of data) {
      new_total += Number(item.down_new);
    }
    return {
      total_new: new_total,
      data: data,
    };
  }

  async getCpuNatHSI() {
    const models = await this.natSummaryRepo.find({
      where: {
        outlier: MoreThan(0),
      },
      order: {
        outlier: 'DESC',
      },
    });

    for (const item of models) {
      item.total = Number(item.outlier) + Number(item.normal);
    }

    return {
      data: models,
      total: models.length,
    };
  }

  async getCpuNatHSIDetail(req: any) {
    const hostname = req.query.hostname;
    const models = await this.cpuNatRepo.find({
      where: {
        hostname: hostname,
      },
      order: {
        cpu: 'ASC',
      },
    });

    return {
      data: models,
    };
  }

  async getActiveHsi() {
    const models = await this.hsiHighRepo
      .createQueryBuilder()
      .limit(5)
      .getMany();
    return models;
  }

  async getActiveHsiAll() {
    return this.hsiHighRepo.find();
  }

  async getHsiLambat() {
    return this.hsiLambat.find();
  }

  async getHSILambatWitel() {
    return this.hsiWitel.find({ order: { jumlah: 'DESC' } });
  }

  async getHSIDetail(req: any) {
    let models = await this.hsiRepo.createQueryBuilder('hsi_tiket');

    const regional = req.query.regional as string;
    const witel = req.query.witel as string;
    const status = req.query.status as string;
    const tanggal = req.query.tanggal as string;

    if (tanggal && tanggal != '') {
      models.andWhere('date("Reported_Date")=:tanggal', { tanggal: tanggal });
    }

    if (regional && regional != '') {
      models.andWhere('"Regional"=:q1', { q1: regional.toUpperCase() });
    }

    if (witel && witel != '') {
      models.andWhere('"Witel"=:q2', { q2: witel.toUpperCase() });
    }

    if (status && status != '') {
      models.andWhere('"Status"=:q3', { q3: status.toUpperCase() });
    }

    return await models.getMany();
  }

  async getGempaBumi() {
    return await this.gempaRepo.find({
      take: 5,
      order: {
        date_time: 'DESC',
      },
    });
  }

  async getBlackoutAlert() {
    let impact = await this.impactRepo.find();
    let data = await this.blackoutRepo.find({
      order: {
        updated_at: 'DESC',
      },
      where: {
        cek_ping: 'failed',
      },
    });

    let total = await this.blackoutRepo.count({
      where: {
        cek_ping: 'failed',
      },
    });
    return {
      data: data,
      total: total,
      impact: impact,
    };
  }

  async getBlackoutDetail(hostname: string) {
    const query = this.blackoutImpactRepo.createQueryBuilder('blackout_impact');
    query.select('MAX(blackout_impact.tm_dt_ehealth)', 'max');
    const result = await query.getRawOne();

    let impact = await this.blackoutImpactRepo
      .createQueryBuilder('blackout_impact')
      .where('blackout_impact.tm_dt_ehealth = :max_date', {
        max_date: result.max,
      })
      .andWhere('blackout_impact.node = :hostname', { hostname: hostname })
      .andWhere("blackout_impact.reachability='0'")
      .orderBy('updated_at', 'DESC')
      .getMany();

    return {
      hostname: hostname,
      impact: impact,
      total: impact.length,
    };
  }

  async getRadioSummary() {
    const data = await this.radioRepo.find();
    let new_total = 0;
    for (const item of data) {
      new_total += Number(item.down_new);
    }
    return {
      total_new: new_total,
      data: data,
    };
  }

  async getActiveHsiDetail(req: any) {
    let models = await this.jumlahTiketHsi.createQueryBuilder()

    const witel = req.query.witel as string;
    if (req.query.witel && witel != '') {
      models.andWhere('"Witel" = :witel',{witel:witel})
    }

    // const startDate = req.query.startdate as string;
    // if (req.query.startDate && req.query.startDate != '') {
    //   models.andWhere('"Reported_Date">=:start_date', {
    //     start_date: startDate,
    //   });
    // }

    // const endDate = req.query.enddate as string;
    // if (req.query.enddate && req.query.enddate != '') {
    //   models.andWhere('"Reported_Date"<=:end_date', {
    //     end_date: endDate,
    //   });
    // }

    let data = await models.select('"Witel"',"witel")
      .addSelect('"Regional"',"regional")
      .addSelect('"Workzone"',"workzone")
      .addSelect("SUM(jumlah)","jumlah")
      .groupBy('"Witel"')
      .addGroupBy('"Regional"')
      .addGroupBy('"Workzone"')
      .orderBy('jumlah','DESC')
    .getRawMany()

    return {
      data: data,
      total: data.length,
    }
  }
}

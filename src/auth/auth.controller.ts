import {
  Body,
  Controller,
  Get,
  HttpException,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UserService } from 'src/user/user.service';
import { AuthDto } from './auth.dto';
import { AuthService } from './auth.service';
import { JwtGuard } from './jwt.guard';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private userService: UserService,
  ) {}

  @Get()
  @ApiBearerAuth()
  @UseGuards(JwtGuard)
  async cekUser(@Request() req) {
    let user = await this.userService.findById(req.user.id);
    return {
      ...user,
      roles: req.user.roles || [],
    };
  }

  @Post()
  async login(@Body() authDto: AuthDto) {
    const user = await this.authService.cekUser(
      authDto.username,
      authDto.password,
    );
    if (!user) throw new HttpException('Login failed', 409);

    return this.authService.generateToken({ id: user.id });
  }
}

import { BadRequestException, HttpException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/user.service';
import * as ldap from 'ldapjs';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}
  async cekUser(username, password) {
    const user = await this.userService.findByUsername(username);
    if (user) {
      try {
        if (user.ldapflag) {
          const client = ldap.createClient({
            url: 'ldap://10.62.128.210',
          });

          const promise = new Promise((resolve, reject) => {
            client.bind(username, password, (err) => {
              if (err) {
                // console.log(err.lde_message);
                reject(err.lde_message);
              } else {
                // console.log('true');
                resolve(1);
              }
            });
          });

          const auth = await promise;
          if (auth == 1) {
            return user;
          }
        }

        const valid = this.userService.compare(password, user.password);
        if (valid) {
          return user;
        } else {
          throw new BadRequestException({ message: 'Password salah' });
        }
      } catch (err) {
        throw new HttpException(err, 409);
      }
    } else {
      throw new BadRequestException({ message: 'Username tidak ditemukan' });
    }
  }

  generateToken(user: any) {
    const dataToken = { id: user.id };
    const token = this.jwtService.sign(dataToken);
    return { token: token };
  }
}

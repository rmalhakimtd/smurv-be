import { ApiProduces, ApiProperty, PickType } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { UserDto } from 'src/user/dto/create-user.dto';

export class AuthDto {
  @ApiProperty({ required: true })
  @IsString()
  username: string;

  @ApiProperty({ required: true })
  @IsString()
  password: string;
}

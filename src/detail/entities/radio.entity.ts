import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  Generated,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'o_radio_data' })
export class RadioData {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @Generated('uuid')
  uuid: string;

  @Column()
  tglfile: string;

  @Column()
  ruas: string;

  @Column()
  node: string;

  @Column()
  nbr: string;

  @Column()
  tm_dt_splk: string;

  @Column()
  eheath_stat: number;

  @Column()
  ip_p2p: string;

  @Column()
  alert_time: string;

  @Column()
  regional: string;

  @Column()
  is_valid: boolean;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @Column()
  @UpdateDateColumn({ name: 'updated_at', onUpdate: 'CURRENT_TIMESTAMP(6)' })
  updatedAt: Date;

  @Column()
  validated_by: number;

  @Column()
  acknowledge_by: number;

  @Column()
  acknowledge_time: Date;

  @Column()
  acknowledge_status: string;

  @Column()
  status: string;

  @Column()
  is_closed: boolean;

  @Column()
  tiket_nossa: string;

  @Column()
  validated_time: Date;

  @ManyToOne(() => User, (user) => user.acknowledges)
  @JoinColumn({ name: 'validated_by' })
  validated_user: User;

  @Column()
  cek_ping: string;

  @Column()
  packets_sent: string;

  @Column()
  packet_loss: string;

  @Column()
  packets_recv: string;

  @Column()
  ping_detail: string;
}

import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import moment from 'moment';
import { AlarmSummary } from 'src/alarm/entities/alarm.entity';
import { BboneAlert } from 'src/alarm/entities/bbone.entity';
import { GPONAlert } from 'src/alarm/entities/gpon.entity';
import { MetroAlert } from 'src/alarm/entities/metro.entity';
import { BlackoutAlert } from 'src/dashboard/entities/blackout.entity';

import { CreateDetailDto } from './dto/create-detail.dto';
import { UpdateDetailDto } from './dto/update-detail.dto';
import { RadioData } from './entities/radio.entity';
import { ServiceNodeData } from './entities/service-node.entity';

@Injectable()
export class DetailService {
  constructor(
    @InjectRepository(BboneAlert) private backboneRepo,
    @InjectRepository(MetroAlert) private metroRepo,
    @InjectRepository(ServiceNodeData) private serviceNodeRepo,
    @InjectRepository(AlarmSummary) private alarmRepo,
    @InjectRepository(RadioData) private radioRepo,
    @InjectRepository(BlackoutAlert) private blackoutRepo,
  ) {}

  async getBackboneDetail(req: any) {
    let models = await this.backboneRepo.createQueryBuilder('o_bbone_data');

    const regional = req.query.regional as string;
    const status = req.query.status as string;
    let start_date = req.query.start_date as string;
    let end_date = req.query.end_date as string;
    let search = req.query.search as string;

    if (search) {
      models.andWhere(
        'o_bbone_data.node ilike :s or ruas ilike :s or ip_p2p ilike :s ',
        { s: `%${search}%` },
      );
    }

    if (start_date) {
      start_date = moment(start_date.split(' ')[0]).format(
        'YYYY-MM-DD HH:mm:ss',
      );

      models.andWhere('o_bbone_data."createdAt" >= :start_date', {
        start_date: `${start_date}`,
      });
    }

    if (end_date) {
      end_date = moment(end_date.split(' ')[0]).format('YYYY-MM-DD HH:mm:ss');

      models.andWhere('o_bbone_data."createdAt" <= :end_date', {
        end_date: `${end_date}`,
      });
    }

    if (regional) {
      models.andWhere('o_bbone_data.regional=:regional', {
        regional: regional.toUpperCase(),
      });
    }

    if (status) {
      models.andWhere('status ilike :status', {
        status: `%${status}%`,
      });
    } else {
      models.andWhere("is_closed!=true and status not ilike '%CLOSED%'");
    }

    const tipe_alert: string = req.query.tipe_alert as string;
    if (tipe_alert && tipe_alert != '') {
      switch (tipe_alert.toLowerCase()) {
        case 'down':
          models.andWhere("alert ilike '%down%'");
          break;
        case 'utilize':
          models.andWhere("alert ilike '%utilize%'");
          break;
        case 'healty check':
          models.andWhere(
            "(alert ilike '%temp_ovl%' or alert ilike '%cpu_ovl%' or alert ilike '%mem_ovl%')",
          );
          break;
        case 'degrade':
          models.andWhere(
            "(alert ilike '%LVLPWR%' or alert ilike '%CrcErr%' or alert ilike '%flap%')",
          );
          break;
        default:
          models.andWhere('alert ilike :tipe', { tipe: tipe_alert });
          break;
      }
    }

    const sort: any = req.query.sortBy;
    const sortDirection: any = req.query.sortDirection;
    if (sort && sortDirection) {
      models.orderBy(`o_bbone_data.${sort}`, sortDirection.toUpperCase());
    }

    const page: number = parseInt(req.query.page as any) || 1;
    const perpage: number = parseInt(req.query.perpage as any) || 20;
    const total = await models.getCount();

    models.offset((page - 1) * perpage).limit(perpage);

    return {
      data: await models.getMany(),
      total: total,
      page: page,
      perpage: perpage,
    };
  }

  async getMetroDetail(req: any) {
    let models = await this.metroRepo.createQueryBuilder('o_metro_data');

    const regional = req.query.regional as string;
    const status = req.query.status as string;
    let start_date = req.query.start_date as string;
    let end_date = req.query.end_date as string;
    let search = req.query.search as string;

    console.log('Regional : ', regional);
    if (search) {
      models.andWhere(
        'o_metro_data.node ilike :s or ruas ilike :s or ip_p2p ilike :s ',
        { s: `%${search}%` },
      );
    }

    if (start_date) {
      start_date = moment(start_date.split(' ')[0]).format(
        'YYYY-MM-DD HH:mm:ss',
      );

      models.andWhere('o_metro_data."createdAt" >= :start_date', {
        start_date: `${start_date}`,
      });
    }

    if (end_date) {
      end_date = moment(end_date.split(' ')[0]).format('YYYY-MM-DD HH:mm:ss');

      models.andWhere('o_metro_data."createdAt" <= :end_date', {
        end_date: `${end_date}`,
      });
    }

    if (regional) {
      models.andWhere('o_metro_data.regional=:regional', {
        regional: regional.toUpperCase(),
      });
    }

    if (status) {
      models.andWhere('status ilike :status', {
        status: `%${status}%`,
      });
    } else {
      models.andWhere("is_closed!=true and status not ilike '%CLOSED%'");
    }

    const tipe_alert: string = req.query.tipe_alert as string;
    if (tipe_alert && tipe_alert != '') {
      switch (tipe_alert.toLowerCase()) {
        case 'down':
          models.andWhere("alert ilike '%down%'");
          break;
        case 'utilize':
          models.andWhere("alert ilike '%utilize%'");
          break;
        case 'healty check':
          models.andWhere(
            "(alert ilike '%temp_ovl%' or alert ilike '%cpu_ovl%' or alert ilike '%mem_ovl%')",
          );
          break;
        case 'degrade':
          models.andWhere(
            "(alert ilike '%LVLPWR%' or alert ilike '%CrcErr%' or alert ilike '%flap%')",
          );
          break;
        default:
          models.andWhere('alert ilike :tipe', { tipe: tipe_alert });
          break;
      }
    }

    const sort: any = req.query.sortBy;
    const sortDirection: any = req.query.sortDirection;
    if (sort && sortDirection) {
      models.orderBy(`o_metro_data.${sort}`, sortDirection.toUpperCase());
    }

    const page: number = parseInt(req.query.page as any) || 1;
    const perpage: number = parseInt(req.query.perpage as any) || 20;
    const total = await models.getCount();

    models.offset((page - 1) * perpage).limit(perpage);

    return {
      data: await models.getMany(),
      total: total,
      page: page,
      perpage: perpage,
    };
  }

  async getServiceNode(req: any) {
    let models = await this.serviceNodeRepo.createQueryBuilder(
      'v_service_node_data',
    );

    const regional = req.query.regional as string;
    const status = req.query.status as string;
    let start_date = req.query.start_date as string;
    let end_date = req.query.end_date as string;
    let search = req.query.search as string;

    if (search) {
      models.andWhere(
        'v_service_node_data.node ilike :s or ruas ilike :s or ip_p2p ilike :s ',
        { s: `%${search}%` },
      );
    }

    if (start_date) {
      start_date = moment(start_date.split(' ')[0]).format(
        'YYYY-MM-DD HH:mm:ss',
      );

      models.andWhere('v_service_node_data."createdAt" >= :start_date', {
        start_date: `${start_date}`,
      });
    }

    if (end_date) {
      end_date = moment(end_date.split(' ')[0]).format('YYYY-MM-DD HH:mm:ss');

      models.andWhere('v_service_node_data."createdAt" <= :end_date', {
        end_date: `${end_date}`,
      });
    }

    if (regional) {
      models.andWhere('v_service_node_data.regional=:regional', {
        regional: regional.toUpperCase(),
      });
    }

    if (status) {
      models.andWhere('status ilike :status', {
        status: `%${status}%`,
      });
    } else {
      models.andWhere("is_closed!=true and status not ilike '%CLOSED%'");
    }

    const tipe_alert: string = req.query.tipe_alert as string;
    if (tipe_alert && tipe_alert != '') {
      switch (tipe_alert.toLowerCase()) {
        case 'down':
          models.andWhere("alert ilike '%down%'");
          break;
        case 'utilize':
          models.andWhere("alert ilike '%utilize%'");
          break;
        case 'healty check':
          models.andWhere(
            "(alert ilike '%temp_ovl%' or alert ilike '%cpu_ovl%' or alert ilike '%mem_ovl%')",
          );
          break;
        case 'degrade':
          models.andWhere(
            "(alert ilike '%LVLPWR%' or alert ilike '%CrcErr%' or alert ilike '%flap%')",
          );
          break;
        default:
          models.andWhere('alert ilike :tipe', { tipe: tipe_alert });
          break;
      }
    }

    const sort: any = req.query.sortBy;
    const sortDirection: any = req.query.sortDirection;
    if (sort && sortDirection) {
      models.orderBy(
        `v_service_node_data.${sort}`,
        sortDirection.toUpperCase(),
      );
    }

    const page: number = parseInt(req.query.page as any) || 1;
    const perpage: number = parseInt(req.query.perpage as any) || 20;
    const total = await models.getCount();

    models.offset((page - 1) * perpage).limit(perpage);

    return {
      data: await models.getMany(),
      total: total,
      page: page,
      perpage: perpage,
    };
  }

  async getGponDetail(req: any) {
    let models = await this.alarmRepo
      .createQueryBuilder('v_alarm_summary')
      .where("kategori='GPON'");

    const regional = req.query.regional as string;
    const status = req.query.status as string;
    let start_date = req.query.start_date as string;
    let end_date = req.query.end_date as string;
    let search = req.query.search as string;

    if (search) {
      models.andWhere(
        'v_alarm_summary.node ilike :s or ruas ilike :s or ip_p2p ilike :s ',
        { s: `%${search}%` },
      );
    }

    if (start_date) {
      start_date = moment(start_date.split(' ')[0]).format(
        'YYYY-MM-DD HH:mm:ss',
      );

      models.andWhere('v_alarm_summary."createdAt" >= :start_date', {
        start_date: `${start_date}`,
      });
    }

    if (end_date) {
      end_date = moment(end_date.split(' ')[0]).format('YYYY-MM-DD HH:mm:ss');

      models.andWhere('v_alarm_summary."createdAt" <= :end_date', {
        end_date: `${end_date}`,
      });
    }

    if (regional) {
      models.andWhere('v_alarm_summary.regional=:regional', {
        regional: regional.toUpperCase(),
      });
    }
    if (status) {
      models.andWhere('status ilike :status', {
        status: `%${status}%`,
      });
    } else {
      models.andWhere("is_closed!=true and status not ilike '%CLOSED%'");
    }

    const tipe_alert: string = req.query.tipe_alert as string;
    if (tipe_alert && tipe_alert != '') {
      models.andWhere('v_alarm_summary.tipe =:tipe', tipe_alert.toUpperCase());
    }

    const sort: any = req.query.sortBy;
    const sortDirection: any = req.query.sortDirection;
    if (sort && sortDirection) {
      models.orderBy(`v_alarm_summary.${sort}`, sortDirection.toUpperCase());
    }

    const page: number = parseInt(req.query.page as any) || 1;
    const perpage: number = parseInt(req.query.perpage as any) || 20;
    const total = await models.getCount();

    models.offset((page - 1) * perpage).limit(perpage);

    return {
      data: await models.getMany(),
      total: total,
      page: page,
      perpage: perpage,
    };
  }

  async getRadioData(req: any) {
    let models = await this.radioRepo.createQueryBuilder('o_radio_data');

    const regional = req.query.regional as string;
    const status = req.query.status as string;
    let start_date = req.query.start_date as string;
    let end_date = req.query.end_date as string;
    let search = req.query.search as string;

    if (search) {
      models.andWhere(
        'o_radio_data.node ilike :s or ruas ilike :s or ip_p2p ilike :s ',
        { s: `%${search}%` },
      );
    }

    if (start_date) {
      start_date = moment(start_date.split(' ')[0]).format(
        'YYYY-MM-DD HH:mm:ss',
      );

      models.andWhere('o_radio_data."createdAt" >= :start_date', {
        start_date: `${start_date}`,
      });
    }

    if (end_date) {
      end_date = moment(end_date.split(' ')[0]).format('YYYY-MM-DD HH:mm:ss');

      models.andWhere('o_radio_data."createdAt" <= :end_date', {
        end_date: `${end_date}`,
      });
    }

    if (regional) {
      models.andWhere('o_radio_data.regional=:regional', {
        regional: regional.toUpperCase(),
      });
    }

    if (status) {
      models.andWhere('o_radio_data.status ilike :status', {
        status: `%${status}%`,
      });
    } else {
      models.andWhere("is_closed!=true and status not ilike '%CLOSED%'");
    }

    const tipe_alert: string = req.query.tipe_alert as string;
    if (tipe_alert && tipe_alert != '') {
      switch (tipe_alert.toLowerCase()) {
        case 'down':
          models.andWhere("alert ilike '%down%'");
          break;
        case 'utilize':
          models.andWhere("alert ilike '%utilize%'");
          break;
        case 'healty check':
          models.andWhere(
            "(alert ilike '%temp_ovl%' or alert ilike '%cpu_ovl%' or alert ilike '%mem_ovl%')",
          );
          break;
        case 'degrade':
          models.andWhere(
            "(alert ilike '%LVLPWR%' or alert ilike '%CrcErr%' or alert ilike '%flap%')",
          );
          break;
        default:
          models.andWhere('alert ilike :tipe', { tipe: tipe_alert });
          break;
      }
    }

    const sort: any = req.query.sortBy;
    const sortDirection: any = req.query.sortDirection;
    if (sort && sortDirection) {
      models.orderBy(`o_radio_data.${sort}`, sortDirection.toUpperCase());
    }

    const page: number = parseInt(req.query.page as any) || 1;
    const perpage: number = parseInt(req.query.perpage as any) || 20;
    const total = await models.getCount();

    models.offset((page - 1) * perpage).limit(perpage);

    return {
      data: await models.getMany(),
      total: total,
      page: page,
      perpage: perpage,
    };
  }

  async getBlackoutData(req: any) {
    let models = await this.blackoutRepo.createQueryBuilder('o_blackout_data');

    const status = req.query.status as string;
    let start_date = req.query.start_date as string;
    let end_date = req.query.end_date as string;
    let search = req.query.search as string;
    let ping_status = req.query.ping_status as string;

    if (ping_status) {
      models.andWhere('o_blackout_data.cek_ping=:ping_status', {
        ping_status: ping_status,
      });
    } else {
      models.andWhere("o_blackout_data.cek_ping='failed'");
    }

    if (search) {
      models.andWhere('(o_blackout_data.device ilike :s or ipadd ilike :s) ', {
        s: `%${search}%`,
      });
    }

    if (start_date) {
      start_date = moment(start_date.split(' ')[0]).format(
        'YYYY-MM-DD HH:mm:ss',
      );

      models.andWhere('o_blackout_data.created_at >= :start_date', {
        start_date: `${start_date}`,
      });
    }

    if (end_date) {
      end_date = moment(end_date.split(' ')[0]).format('YYYY-MM-DD HH:mm:ss');

      models.andWhere('o_blackout_data.created_at <= :end_date', {
        end_date: `${end_date}`,
      });
    }

    if (status) {
      models.andWhere('status ilike :status', {
        status: `%${status}%`,
      });
    } else {
      models.andWhere("is_closed!=true and status not ilike '%CLOSED%'");
    }

    const sort: any = req.query.sortBy;
    const sortDirection: any = req.query.sortDirection;
    if (sort && sortDirection) {
      models.orderBy(`o_blackout_data.${sort}`, sortDirection.toUpperCase());
    }

    const page: number = parseInt(req.query.page as any) || 1;
    const perpage: number = parseInt(req.query.perpage as any) || 20;
    const total = await models.getCount();

    models.offset((page - 1) * perpage).limit(perpage);

    return {
      data: await models.getMany(),
      total: total,
      page: page,
      perpage: perpage,
    };
  }
}

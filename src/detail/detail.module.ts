import { Module } from '@nestjs/common';
import { DetailService } from './detail.service';
import { DetailController } from './detail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BboneAlert } from 'src/alarm/entities/bbone.entity';
import { MetroAlert } from 'src/alarm/entities/metro.entity';
import { ServiceNodeAlarm } from 'src/dashboard/entities/servidenode-alarm.entity';
import { ServiceNodeData } from './entities/service-node.entity';
import { GPONAlert } from 'src/alarm/entities/gpon.entity';
import { AlarmSummary } from 'src/alarm/entities/alarm.entity';
import { RadioData } from './entities/radio.entity';
import { BlackoutAlert } from 'src/dashboard/entities/blackout.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BboneAlert,
      MetroAlert,
      ServiceNodeData,
      GPONAlert,
      AlarmSummary,
      RadioData,
      BlackoutAlert,
    ]),
  ],
  controllers: [DetailController],
  providers: [DetailService],
})
export class DetailModule {}

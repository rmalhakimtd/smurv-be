import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
} from '@nestjs/common';
import { DetailService } from './detail.service';
import { CreateDetailDto } from './dto/create-detail.dto';
import { UpdateDetailDto } from './dto/update-detail.dto';

@Controller('detail')
export class DetailController {
  constructor(private readonly detailService: DetailService) {}

  @Get('backbone')
  getBackboneDetail(@Req() req: any) {
    return this.detailService.getBackboneDetail(req);
  }

  @Get('metro')
  getMetroDetail(@Req() req: any) {
    return this.detailService.getMetroDetail(req);
  }

  @Get('service-node')
  getServiceNode(@Req() req: any) {
    return this.detailService.getServiceNode(req);
  }

  @Get('gpon')
  getGponData(@Req() req: any) {
    return this.detailService.getGponDetail(req);
  }

  @Get('radio')
  getRadioData(@Req() req: any) {
    return this.detailService.getRadioData(req);
  }

  @Get('blackout')
  getBlackoutData(@Req() req: any) {
    return this.detailService.getBlackoutData(req);
  }
}

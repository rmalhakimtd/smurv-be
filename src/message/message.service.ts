/* eslint-disable prefer-const */
import { InjectQueue } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Queue } from 'bull';
import { User } from 'src/user/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MessageService {
  constructor(
    @InjectQueue('notification') private readonly queue: Queue,
    @InjectRepository(User) private userRepo: Repository<User>,
  ) {}

  async sendNotifToAll(title: string, message: string) {
    let user = await this.userRepo
      .createQueryBuilder('m_users')
      .where('firebase_id is not null')
      .getMany();
    for (let item of user) {
      await this.queue.add('firebase', {
        sender_id: item.id,
        token: item.firebase_id,
        title: title,
        body: message,
      });
    }
    return true;
  }
}

/* eslint-disable prefer-const */
import { Controller } from '@nestjs/common';
import {
  Ctx,
  MessagePattern,
  Payload,
  RedisContext,
} from '@nestjs/microservices';
import { MessageService } from './message.service';

@Controller()
export class MessageController {
  constructor(private readonly messageService: MessageService) {}

  @MessagePattern('gpon-blackout')
  async getGponBlackout(@Payload() data: any, @Ctx() context: RedisContext) {
    let title = 'GPON Blackout';
    let message = `GPON Blackout Alert Node ${data.NodeId} IP :${data.NodeIp}`;
    await this.messageService.sendNotifToAll(title, message);
    console.log('GET GPON Blackout : ', data);
  }
}

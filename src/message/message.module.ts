import { Module } from '@nestjs/common';
import { MessageService } from './message.service';
import { MessageController } from './message.controller';
import { BullModule } from '@nestjs/bull';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/user/entities/user.entity';

@Module({
  imports: [
    BullModule.registerQueue({
      name: 'notification',
    }),
    TypeOrmModule.forFeature([User]),
  ],
  controllers: [MessageController],
  providers: [MessageService],
})
export class MessageModule {}

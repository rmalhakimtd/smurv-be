import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ExistValidator } from './helper/validator/exist-validator';
import { UniqueValidator } from './helper/validator/unique-validator';
import { AuthModule } from './auth/auth.module';
import { RoleModule } from './role/role.module';
import { PermissionModule } from './permission/permission.module';
import { PageService } from './helper/service/page/page.service';
import { ResponseService } from './helper/service/response/api.service';
import { RoleMiddleware } from './middleware/role.middleware';
import { BullModule } from '@nestjs/bull';
import { OrganizationModule } from './organization/organization.module';
import { PushNotificationModule } from './push-notification/push-notification.module';
import { DeviceModule } from './device/device.module';
import { TelegramModule } from './telegram/telegram.module';
import { OptionModule } from './option/option.module';
import { AlarmSummaryModule } from './alarm/alarm.module';
import { MailModule } from './mail/mail.module';
import { ExcludesModule } from './excludes/excludes.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { DismantleModule } from './dismantle/dismantle.module';
import { MessageModule } from './message/message.module';
import { PerangkatModule } from './perangkat/perangkat.module';
import { DetailModule } from './detail/detail.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      port: parseInt(process.env.DB_PORT),
      database: process.env.DB_NAME,
      synchronize: false,
      entities: ['dist/**/*.entity.js'],
    }),
    BullModule.forRoot({
      redis: {
        host: process.env.REDIS_HOST,
        port: parseInt(process.env.REDIS_PORT),
        // port: 6380,
      },
    }),
    UserModule,
    AuthModule,
    RoleModule,
    PermissionModule,
    OrganizationModule,
    DeviceModule,
    AlarmSummaryModule,
    TelegramModule,
    OptionModule,
    MailModule,
    ExcludesModule,
    DashboardModule,
    DismantleModule,
    MessageModule,
    PushNotificationModule,
    PerangkatModule,
    DetailModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    ExistValidator,
    UniqueValidator,
    PageService,
    ResponseService,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(RoleMiddleware).forRoutes('/');
  }
}
